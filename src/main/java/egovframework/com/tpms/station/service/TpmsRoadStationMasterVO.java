package egovframework.com.tpms.station.service;

public class TpmsRoadStationMasterVO {
	private long id;
	private long tri_id;
	private int lane;
	private double extension;
	private int sector;
	private int section;
	private int history_n;
	private int seq_n;
	private boolean is_use;
	private int version;
	private int real_len;
	private String f_node_id;
	private String t_node_id;
	private int is_multi;
	private int main_num;
	private int sub_num;
	private int dir;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTri_id() {
		return tri_id;
	}
	public void setTri_id(long tri_id) {
		this.tri_id = tri_id;
	}
	public int getLane() {
		return lane;
	}
	public void setLane(int lane) {
		this.lane = lane;
	}
	public double getExtension() {
		return extension;
	}
	public void setExtension(double extension) {
		this.extension = extension;
	}
	public int getSector() {
		return sector;
	}
	public void setSector(int sector) {
		this.sector = sector;
	}
	public int getSection() {
		return section;
	}
	public void setSection(int section) {
		this.section = section;
	}
	public int getHistory_n() {
		return history_n;
	}
	public void setHistory_n(int history_n) {
		this.history_n = history_n;
	}
	public int getSeq_n() {
		return seq_n;
	}
	public void setSeq_n(int seq_n) {
		this.seq_n = seq_n;
	}
	public boolean isIs_use() {
		return is_use;
	}
	public void setIs_use(boolean is_use) {
		this.is_use = is_use;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public int getReal_len() {
		return real_len;
	}
	public void setReal_len(int real_len) {
		this.real_len = real_len;
	}
	public String getF_node_id() {
		return f_node_id;
	}
	public void setF_node_id(String f_node_id) {
		this.f_node_id = f_node_id;
	}
	public String getT_node_id() {
		return t_node_id;
	}
	public void setT_node_id(String t_node_id) {
		this.t_node_id = t_node_id;
	}
	public int getIs_multi() {
		return is_multi;
	}
	public void setIs_multi(int is_multi) {
		this.is_multi = is_multi;
	}
	public int getMain_num() {
		return main_num;
	}
	public void setMain_num(int main_num) {
		this.main_num = main_num;
	}
	public int getSub_num() {
		return sub_num;
	}
	public void setSub_num(int sub_num) {
		this.sub_num = sub_num;
	}
	public int getDir() {
		return dir;
	}
	public void setDir(int dir) {
		this.dir = dir;
	}
}
