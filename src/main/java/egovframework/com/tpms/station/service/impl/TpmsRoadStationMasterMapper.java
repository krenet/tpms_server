package egovframework.com.tpms.station.service.impl;

import egovframework.com.tpms.station.service.TpmsRoadStationMasterVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsRoadStationMasterMapper")
public interface TpmsRoadStationMasterMapper {
	//<select id="selectStationMasterById" resultType="tpmsRoadStationMasterVO" parameterType="long">
	public TpmsRoadStationMasterVO selectStationMasterById(long id);
}
