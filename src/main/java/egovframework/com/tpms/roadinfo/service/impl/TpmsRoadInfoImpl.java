package egovframework.com.tpms.roadinfo.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import egovframework.com.tpms.cmm.service.FileUploadService;
import egovframework.com.tpms.roadinfo.service.TpmsRoadInfoService;
import egovframework.com.tpms.roadinfo.service.TpmsRoadInfoVO;

@Service("tpmsRoadInfoService")
@Transactional
public class TpmsRoadInfoImpl implements TpmsRoadInfoService{
	
	@Resource(name="tpmsRoadInfoMapper")
	private TpmsRoadInfoMapper tpmsRoadInfoMapper;

	@Autowired
	FileUploadService fileUploadService;
	
	@Override
	public TpmsRoadInfoVO insertRoadInfo(Map<String, Object> map) {
		// TODO Auto-generated method stub
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadInfoVO vo = mapper.convertValue(map, TpmsRoadInfoVO.class);		
		try {
			vo.setEtc(java.net.URLDecoder.decode(vo.getEtc(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return insertRoadInfoData(vo, "new");
	}
	
	public TpmsRoadInfoVO insertRoadInfoData(TpmsRoadInfoVO vo, String insertType) {
		// 이미 있는 도로인지 체크
		int cnt = tpmsRoadInfoMapper.selectExistRoadCheck(vo);
		if(cnt>0) {
			vo.setOK(false);
			return vo;
		}
		
		int version = 0;
		
		if(insertType.equals("new")) {
			// version check
			version = tpmsRoadInfoMapper.selectNewRoadInfoVersion();
			
			if(version==0) {
				tpmsRoadInfoMapper.insertNewRoadInfoVersion("");
				version = tpmsRoadInfoMapper.selectNewRoadInfoVersion();
			}
		}else if(insertType.equals("update")) {
			version = tpmsRoadInfoMapper.selectUpdateRoadInfoVersion();
			if(version==0) {
				version = tpmsRoadInfoMapper.insertUpdateRoadInfoVersion("");
			}
		}
		
		
		vo.setVersion(version);
		vo.setUn_use(0);
		int res = tpmsRoadInfoMapper.insertRoadInfo(vo);
		
		if(res>0) vo.setOK(true);
		
		return vo;
	}

	@Override
	public ArrayList<TpmsRoadInfoVO> insertRoadInfoList(MultipartFile file, String columnsList, String typeList) {
		
		if(file.getSize()>0) {
			String path = fileUploadService.restore(file);
			System.out.println("file path : "+path);
			
			if(!(path.toLowerCase().endsWith(".xlsx")||path.toLowerCase().endsWith(".csv"))) {
				return null;
			}
			
			FileInputStream fs;
			String[] fieldNames = columnsList.split(",");
			String[] fieldTypes = typeList.split(",");
			
			ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
			XSSFWorkbook workbook = null;
			try {
				fs = new FileInputStream(new File(path));
				OPCPackage opcPackage = OPCPackage.open(fs);
				workbook = new XSSFWorkbook(opcPackage);
				opcPackage.close();
			} catch (InvalidFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			
			XSSFSheet sheet = workbook.getSheetAt(0);
			// sheet row
			Iterator<Row> rowIterator = sheet.iterator();
			// 한 행 저장
			HashMap<String, Object> oneRow = null;
			Row row;
			int rowCnt = 0;
			JSONObject json = null;
			
			while(rowIterator.hasNext()) {
				row = rowIterator.next();
				if(++rowCnt < 3) continue;
				
				Iterator<Cell> cellIterator = row.cellIterator();
				oneRow = new HashMap<>();
				json = new JSONObject();
				while(cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					if((cell.getColumnIndex()==0||cell.getColumnIndex()==1)&&cell.getCellType()==Cell.CELL_TYPE_BLANK) break;
					System.out.println("fieldNames : "+fieldNames[cell.getColumnIndex()]);
					
					System.out.println(cell.getColumnIndex());
					
					
					try {

						switch (cell.getCellType()) {
						case Cell.CELL_TYPE_STRING:
	//						System.out.println(cell.getStringCellValue());
							if(fieldTypes[cell.getColumnIndex()].equals("json")) {
									json.put(fieldNames[cell.getColumnIndex()], cell.getStringCellValue());
								break;
							}
							oneRow.put(fieldNames[cell.getColumnIndex()], cell.getStringCellValue());
							break;
						case Cell.CELL_TYPE_NUMERIC:
							String value = "";
							if(fieldTypes[cell.getColumnIndex()].equals("string")) {
								value = String.valueOf(Math.floor(cell.getNumericCellValue()));
								if(value.contains(".")) {
									value = value.split("\\.")[0];
									
									if(fieldTypes[cell.getColumnIndex()].equals("json")) {
										json.put(fieldNames[cell.getColumnIndex()], value);
										break;
									}
									
									oneRow.put(fieldNames[cell.getColumnIndex()], value);
								}
							}else {
								if(fieldTypes[cell.getColumnIndex()].equals("json")) {
									json.put(fieldNames[cell.getColumnIndex()], cell.getNumericCellValue());
									break;
								}
								
								oneRow.put(fieldNames[cell.getColumnIndex()], cell.getNumericCellValue());
							}
							
							break;
						case Cell.CELL_TYPE_BOOLEAN:
							if(fieldTypes[cell.getColumnIndex()].equals("json")) {
								json.put(fieldNames[cell.getColumnIndex()], cell.getBooleanCellValue());
								break;
							}
							
							oneRow.put(fieldNames[cell.getColumnIndex()], cell.getBooleanCellValue());
							break;
						case Cell.CELL_TYPE_BLANK:
							if(fieldTypes[cell.getColumnIndex()].equals("json")) {
								json.put(fieldNames[cell.getColumnIndex()], "");
								break;
							}
							
							if(fieldTypes[cell.getColumnIndex()].equals("int")) {
								oneRow.put(fieldNames[cell.getColumnIndex()], 0);
							}else {
								oneRow.put(fieldNames[cell.getColumnIndex()], null);
							}
							break;
						default:
							break;
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(fieldTypes.length == cell.getColumnIndex()) break;
				}
				if(json.length()>0) oneRow.put("etc", json.toString());
				if(oneRow.size()>0) rowList.add(oneRow);
				else break;
			}
			
			try {
				fs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;
			TpmsRoadInfoVO[] voList = null;
			try {
				jsonStr = objectMapper.writeValueAsString(rowList);
				voList = objectMapper.readValue(jsonStr, new TypeReference<TpmsRoadInfoVO[]>() {});
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ArrayList<TpmsRoadInfoVO> res = new ArrayList<TpmsRoadInfoVO>();
			for (TpmsRoadInfoVO tpmsRoadInfoVO : voList) {
				res.add(insertRoadInfoData(tpmsRoadInfoVO, "new"));
			}
			
			return res;
		}else {
			return null;
		}
		
		
	}

	@Override
	public ArrayList<String> selectNoList() {
		// TODO Auto-generated method stub
		ArrayList<String> list = tpmsRoadInfoMapper.selectNoList();
		return ObjectArraySort(list);
	}

	public ArrayList<String> ObjectArraySort(ArrayList<String> list){

		ArrayList<String> res = new ArrayList<>();
		ArrayList<Integer> tempList = new ArrayList<>();
		int no = 0;
		for (String string : list) {
			try {
				no = Integer.parseInt(string);
				tempList.add(no);
			}catch(Exception e) {
				res.add(string);
			}
		}
		
		Collections.sort(tempList);
		
		for (Integer integer : tempList) {
			res.add(integer.toString());
		}
		
		return res;
	}
	
	@Override
	public ArrayList<String> selectNameList() {
		// TODO Auto-generated method stub
		ArrayList<String> list = tpmsRoadInfoMapper.selectNameList();
		
		Collections.sort(list);
		
		return list;
	}

	@Override
	public ArrayList<String> selectNoListForRank(String rank) {
		// TODO Auto-generated method stub
		ArrayList<String> list = tpmsRoadInfoMapper.selectNoListForRank(rank);
		
		return ObjectArraySort(list);
	}

	@Override
	public ArrayList<String> selectNameListForRank(String rank) {
		// TODO Auto-generated method stub
		ArrayList<String> list = tpmsRoadInfoMapper.selectNameListForRank(rank);
		
		Collections.sort(list);
		
		return list;
	}

	@Override
	public ArrayList<String> selectNameListForRankNo(Map<String, Object> map) {
		// TODO Auto-generated method stub
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadInfoVO vo = mapper.convertValue(map, TpmsRoadInfoVO.class);		
		ArrayList<String> list = tpmsRoadInfoMapper.selectNameListForRankNo(vo);
		
		Collections.sort(list);
		
		return list;
	}

	@Override
	public TpmsRoadInfoVO selectOneRoadInfo(Map<String, Object> map) {
		// TODO Auto-generated method stub
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadInfoVO vo = mapper.convertValue(map, TpmsRoadInfoVO.class);
		
		return tpmsRoadInfoMapper.selectOneRoadInfo(vo);
	}

	@Override
	public String updateEachRoadInfo(Map<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper();
		String updateObj = null;
		String originalObj = null;
//		TpmsRoadInfoVO updateVO = mapper.convertValue(updateObj, TpmsRoadInfoVO.class);
//		TpmsRoadInfoVO originalVO = mapper.convertValue(originalObj, TpmsRoadInfoVO.class);
		TpmsRoadInfoVO updateVO = new TpmsRoadInfoVO();
		TpmsRoadInfoVO originalVO = new TpmsRoadInfoVO();
		try {
			updateObj = java.net.URLDecoder.decode((String) map.get("updateObj"), "UTF-8");
			originalObj = java.net.URLDecoder.decode((String) map.get("originalObj"), "UTF-8");
			
//			updateVO = mapper.readValue(updateObj, new TypeReference<TpmsRoadInfoVO>() {});
//			originalVO = mapper.readValue(originalObj, new TypeReference<TpmsRoadInfoVO>() {});
			updateVO = mapper.readValue(updateObj, TpmsRoadInfoVO.class);
			originalVO = mapper.readValue(originalObj, TpmsRoadInfoVO.class);
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// TODO Auto-generated method stub
		
		try {
			updateVO.setEtc(java.net.URLDecoder.decode(updateVO.getEtc(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int res= tpmsRoadInfoMapper.selectExistRoadCheck(originalVO);
		if(res<1) return "존재하지 않는 노선입니다.";
		
		res = tpmsRoadInfoMapper.updateRoadInfo(originalVO);
		if(res<1) return "[수정 실패] 이력 데이터를 만드는 과정에 오류가 생겼습니다.";
		
		TpmsRoadInfoVO resVO = insertRoadInfoData(updateVO, "update");
		if(resVO.isOK()) return "수정 성공";
		else return "[수정 실패] 수정 데이터 입력에 실패하였습니다.";
		
	}

	
}
