package egovframework.com.tpms.roadinfo.service.impl;

import java.util.ArrayList;

import egovframework.com.tpms.cmm.service.TpmsCodeVO;
import egovframework.com.tpms.roadinfo.service.TpmsRoadInfoVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsRoadInfoMapper")
public interface TpmsRoadInfoMapper {
	//<insert id="insertRoadInfo" parameterType="tpmsRoadInfoVO">
	public int insertRoadInfo(TpmsRoadInfoVO vo);
	//<select id="selectNewRoadInfoVersion" resultType="int">
	public int selectNewRoadInfoVersion();
	//<insert id="insertNewRoadInfoVersion" parameterType="String">
	public int insertNewRoadInfoVersion(String etc);
	//<select id="selectExistRoadCheck" parameterType="tpmsRoadInfoVO" resultType="int">
	public int selectExistRoadCheck(TpmsRoadInfoVO vo);
	//<select id="selectNoList" resultType="int">
	public ArrayList<String> selectNoList();
	//<select id="selectNameList" resultType="String">
	public ArrayList<String> selectNameList();
	//<select id="selectNoListForRank" resultType="String" parameterType="String">
	public ArrayList<String> selectNoListForRank(String value);
	//<select id="selectNameListForRank" resultType="String" parameterType="String">
	public ArrayList<String> selectNameListForRank(String value);
	//<select id="selectNameListForRankNo" resultType="String" parameterType="tpmsRoadInfoVO">
	public ArrayList<String> selectNameListForRankNo(TpmsRoadInfoVO vo); 
	//<select id="selectOneRoadInfo" resultType="tpmsRoadInfoVO" parameterType="tpmsRoadInfoVO">
	public TpmsRoadInfoVO selectOneRoadInfo(TpmsRoadInfoVO vo);
	//<update id="updateRoadInfo" parameterType="tpmsRoadInfoVO">
	public int updateRoadInfo(TpmsRoadInfoVO vo);
	//<select id="selectUpdateRoadInfoVersion" resultType="int">
	public int selectUpdateRoadInfoVersion();
	//insertUpdateRoadInfoVersion
	public int insertUpdateRoadInfoVersion(String etc); 
	//<select id="selectRoadInfoDataById" resultType="tpmsRoadInfoVO" parameterType="long">
	public TpmsRoadInfoVO selectRoadInfoDataById(long id);
}
