package egovframework.com.tpms.roadinfo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

public interface TpmsRoadInfoService {
	//<insert id="insertRoadInfo" parameterType="tpmsRoadInfoVO">
	public TpmsRoadInfoVO insertRoadInfo(Map<String, Object> map);
	//<insert id="insertNewRoadInfoVersion" parameterType="String">
	//public int insertNewRoadInfoVersion(String etc);
	public ArrayList<TpmsRoadInfoVO> insertRoadInfoList(MultipartFile file,  String columnsList, String typeList);
	//<select id="selectNoList" resultType="int">
	public ArrayList<String> selectNoList();
	//<select id="selectNameList" resultType="String">
	public ArrayList<String> selectNameList();
	//<select id="selectNoListForRank" resultType="String" parameterType="String">
	public ArrayList<String> selectNoListForRank(String rank);
	//<select id="selectNameListForRank" resultType="String" parameterType="String">
	public ArrayList<String> selectNameListForRank(String rank);
	//<select id="selectNameListForRankNo" resultType="String" parameterType="tpmsRoadInfoVO">
	public ArrayList<String> selectNameListForRankNo(Map<String, Object> map);
	//<select id="selectOneRoadInfo" resultType="tpmsRoadInfoVO" parameterType="tpmsRoadInfoVO">
	public TpmsRoadInfoVO selectOneRoadInfo(Map<String, Object> map);
	//update Road Info
	public String updateEachRoadInfo(Map<String, Object> map);
	
}
