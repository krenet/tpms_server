package egovframework.com.tpms.roadinfo.service;

public class TpmsRoadInfoVO {

	private long id;
	private String name;
	private String rank;
	private String type;
	private String no;
	private int lanes;
	
	private String s_node_id;
	private String e_node_id;
	private String etc;
	private int version;
	private int extension;
	private int un_use;
	
	public int getUn_use() {
		return un_use;
	}
	public void setUn_use(int un_use) {
		this.un_use = un_use;
	}
	/** 입력 또는 수정 등 데이터베이스 관련 수행 작업 성공여부 */
	private boolean isOK;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public int getLanes() {
		return lanes;
	}
	public void setLanes(int lanes) {
		this.lanes = lanes;
	}
	public String getS_node_id() {
		return s_node_id;
	}
	public void setS_node_id(String s_node_id) {
		this.s_node_id = s_node_id;
	}
	public String getE_node_id() {
		return e_node_id;
	}
	public void setE_node_id(String e_node_id) {
		this.e_node_id = e_node_id;
	}
	public String getEtc() {
		return etc;
	}
	public void setEtc(String etc) {
		this.etc = etc;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public int getExtension() {
		return extension;
	}
	public void setExtension(int extension) {
		this.extension = extension;
	}
	public boolean isOK() {
		return isOK;
	}
	public void setOK(boolean isOK) {
		this.isOK = isOK;
	}
	
	
}
