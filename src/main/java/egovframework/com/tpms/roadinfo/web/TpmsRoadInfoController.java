package egovframework.com.tpms.roadinfo.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.cmm.service.TpmsCmmService;
import egovframework.com.tpms.roadinfo.service.TpmsRoadInfoService;
import egovframework.com.tpms.roadinfo.service.TpmsRoadInfoVO;

@CrossOrigin("*")
@RestController
public class TpmsRoadInfoController {

	@Resource(name="tpmsRoadInfoService")
	private TpmsRoadInfoService tpmsRoadInfoService;
	
	// POST방식 RequestParam방식 된다.
	@RequestMapping(value="/tpms/insertRoadInfo.do", method=RequestMethod.POST)
	public ModelAndView insertRoadInfo(@RequestParam HashMap<String, Object> map){

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		TpmsRoadInfoVO vo = tpmsRoadInfoService.insertRoadInfo(map);
		if(vo.isOK()) mav.addObject("res", 1);
		else mav.addObject("res", 0);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/insertRoadInfoList.do", method=RequestMethod.POST)
	public ModelAndView insertRoadInfoList(@RequestParam("file") MultipartFile file, @RequestParam HashMap<String, Object> map) {
		ArrayList<TpmsRoadInfoVO> resList = tpmsRoadInfoService.insertRoadInfoList(file, (String) map.get("columns"), (String) map.get("types"));
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		if(resList.size()>0) {
			ArrayList<TpmsRoadInfoVO> failList = new ArrayList<>();
			for (TpmsRoadInfoVO tpmsRoadInfoVO : resList) {
				if(!tpmsRoadInfoVO.isOK()) failList.add(tpmsRoadInfoVO);
			}
			
			if(failList.size()>0) mav.addObject("res", failList);
			else mav.addObject("res", 1);
		}else {
			mav.addObject("res", 0);
		}
		
		return mav;
	}

	
	@RequestMapping(value="/tpms/selectNoList.do", method=RequestMethod.POST)
	public ModelAndView selectNoList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadInfoService.selectNoList());
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/selectNameList.do", method=RequestMethod.POST)
	public ModelAndView selectNameList() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadInfoService.selectNameList());
		
		return mav;
	}
	
	
	@RequestMapping(value="/tpms/selectNoListForRank.do", method=RequestMethod.POST)
	public ModelAndView selectNoListForRank(@RequestParam String rank) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadInfoService.selectNoListForRank(rank));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/selectNameListForRank.do", method=RequestMethod.POST)
	public ModelAndView selectNameListForRank(@RequestParam String rank) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadInfoService.selectNameListForRank(rank));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/selectNameListForRankNo.do", method=RequestMethod.POST)
	public ModelAndView selectNameListForRankNo(@RequestParam HashMap<String, Object> map) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadInfoService.selectNameListForRankNo(map));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/selectOneRoadInfo.do", method=RequestMethod.POST)
	public ModelAndView selectOneRoadInfo(@RequestParam HashMap<String, Object> map) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadInfoService.selectOneRoadInfo(map));
		
		return mav;
	}
	
	//updateEachRoadInfo
	@RequestMapping(value="/tpms/updateEachRoadInfo.do", method=RequestMethod.POST)
	public ModelAndView updateEachRoadInfo(@RequestParam HashMap<String, Object> map) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadInfoService.updateEachRoadInfo(map));
		
		return mav;
	}
	

	
	
}
