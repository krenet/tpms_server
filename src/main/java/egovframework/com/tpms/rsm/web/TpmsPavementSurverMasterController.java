package egovframework.com.tpms.rsm.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.cmm.service.TpmsCmmService;
import egovframework.com.tpms.rsm.service.TpmsPavementSurveyMasterService;

@Controller
public class TpmsPavementSurverMasterController {
	@Resource(name="tpmsPavementSurveyMasterService")
	private TpmsPavementSurveyMasterService tpmsPavementSurveyMasterService;
	@Resource(name="tpmsCmmService")
	private TpmsCmmService tpmsCmmService;
	
	@RequestMapping(value="/tpms/getSelectEquip1.do", method=RequestMethod.POST)
    public ModelAndView getSelectEquip1(@RequestParam HashMap<String, Object> map){

        ModelAndView mav = new ModelAndView();
        mav.setViewName("jsonView");

        mav.addObject("res", tpmsPavementSurveyMasterService.getSelectEquip1(map));

        return mav;
    }
	
	@RequestMapping(value="/tpms/searchSurveyMaster.do", method=RequestMethod.POST)
	public ModelAndView searchSurveyMaster(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		List<Map<String, Object>> searchSurveyMasterList = tpmsPavementSurveyMasterService.searchSurveyMaster(map);
		if(searchSurveyMasterList.size()>0) searchSurveyMasterList = tpmsCmmService.dataReplace(searchSurveyMasterList);
		
		mav.addObject("res", searchSurveyMasterList);	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getEquip2Crack.do", method=RequestMethod.POST)
    public ModelAndView getEquip2Crack(@RequestParam HashMap<String, Object> map){

        ModelAndView mav = new ModelAndView();
        mav.setViewName("jsonView");

        Map<String, Object> param = new HashMap<>();
		param.put("section", "setting");
        
        mav.addObject("res", tpmsPavementSurveyMasterService.getEquip2Crack(map, tpmsCmmService.readIniFile(param)));

        return mav;
    }
	
	@RequestMapping(value="/tpms/getEquip2Hwd.do", method=RequestMethod.POST)
    public ModelAndView getEquip2Hwd(@RequestParam HashMap<String, Object> map){

        ModelAndView mav = new ModelAndView();
        mav.setViewName("jsonView");

        mav.addObject("res", tpmsPavementSurveyMasterService.getEquip2Hwd(map));

        return mav;
    }
	
	@RequestMapping(value="/tpms/getEquip2Gpr.do", method=RequestMethod.POST)
    public ModelAndView getEquip2Gpr(@RequestParam HashMap<String, Object> map){

        ModelAndView mav = new ModelAndView();
        mav.setViewName("jsonView");

        mav.addObject("res", tpmsPavementSurveyMasterService.getEquip2Gpr(map));

        return mav;
    }
	
	@RequestMapping(value="/tpms/getEquip1RuttIri.do", method=RequestMethod.POST)
	public ModelAndView getEquip1RuttIri(){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsPavementSurveyMasterService.getEquip1RuttIri());	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getEquip2ViewCrack.do", method=RequestMethod.POST)
	public ModelAndView getEquip2ViewCrack(){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsPavementSurveyMasterService.getEquip2ViewCrack());	
		
		return mav;
	}
}
