package egovframework.com.tpms.rsm.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.cmm.service.TpmsCmmService;
import egovframework.com.tpms.rsm.service.TpmsRoadSurveyMasterService;

@RestController
public class TpmsRoadSurveyMasterController {
	
	@Resource(name="tpmsRoadSurveyMasterService")
	private TpmsRoadSurveyMasterService tpmsRoadSurveyMasterService;
	
	@Resource(name="tpmsCmmService")
	private TpmsCmmService tpmsCmmService;
	
	@RequestMapping(value="/tpms/insertEquipment1DataList.do", method=RequestMethod.POST)
	public ModelAndView insertEquipment1DataList(@RequestParam("excelFolder") List<MultipartFile> excelFolder, @RequestParam HashMap<String, Object> map) {
		
		tpmsRoadSurveyMasterService.insertPesDataList(excelFolder, (String)map.get("table_columns"), (String)map.get("table_types"),
											(String)map.get("excel_columns"), (String)map.get("excel_types"));

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", 1);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/insertEquipment1Data.do", method=RequestMethod.POST)
	public ModelAndView insertEquipment1Data(@RequestParam("excelFile") MultipartFile excelFile, @RequestParam HashMap<String, Object> map) {
		
		HashMap<String, Object> res = tpmsRoadSurveyMasterService.insertPesData(excelFile, (String)map.get("table_columns"), (String)map.get("table_types"),
																				(String)map.get("excel_columns"), (String)map.get("excel_types"));
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/buildFileData.do", method=RequestMethod.POST)
	public ModelAndView buildFileData(@RequestParam("file") MultipartFile file) {
		Map<String, Object> param = new HashMap<>();
		param.put("section", "directorys");
		
		int res = tpmsRoadSurveyMasterService.buildFileData(file, tpmsCmmService.readIniFile(param));
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}
	
	
	@RequestMapping(value="/tpms/insertEquipment2CrackData.do", method=RequestMethod.POST)
	public ModelAndView insertEquipment2CrackData(@RequestParam("file") MultipartFile file, @RequestParam HashMap<String, Object> map) {
		
		HashMap<String, Object> res = tpmsRoadSurveyMasterService.insertCrackData(file, (String)map.get("columns"), (String)map.get("types"), tpmsCmmService.getIndexFormula());
		System.out.println(res);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/insertEquipment2GprData.do", method=RequestMethod.POST)
	public ModelAndView insertEquipment2GprData(@RequestParam("file") MultipartFile file, @RequestParam HashMap<String, Object> map) {
		
		HashMap<String, Object> res = tpmsRoadSurveyMasterService.insertGprData(file, (String)map.get("columns"), (String)map.get("types"));

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/insertEquipment2HwdData.do", method=RequestMethod.POST)
	public ModelAndView insertEquipment2HwdData(@RequestParam("file") MultipartFile file, @RequestParam HashMap<String, Object> map) {
		
		HashMap<String, Object> res = tpmsRoadSurveyMasterService.insertHwdData(file, (String)map.get("columns"), (String)map.get("types"));

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/selectRoadSectionInfo.do", method=RequestMethod.POST)
	public ModelAndView selectRoadSectionInfo(@RequestParam Long id) {
		Map<String, Object> initparam = new HashMap<>();
		initparam.put("section", "resource");
		initparam.put("options", "roadSurvey1Img");
		
		Map<String, Object> ini = (Map<String, Object>) tpmsCmmService.readIniFile(initparam);
		String resourceUrl = (String)ini.get("value");
		String formula = tpmsCmmService.getIndexFormula();
		HashMap<String, Object> res = tpmsRoadSurveyMasterService.selectRoadSectionInfo(id, resourceUrl, formula);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}	
	
	@RequestMapping(value="/tpms/selectRoadSectionList.do", method=RequestMethod.POST)
	public ModelAndView selectRoadSectionList(@RequestParam HashMap<String, Object> param) {
		Map<String, Object> initparam = new HashMap<>();
		initparam.put("section", "resource");
		initparam.put("options", "roadSurvey1Img");
		
		Map<String, Object> ini = (Map<String, Object>) tpmsCmmService.readIniFile(initparam);
		String resourceUrl = (String)ini.get("value");
		String formula = tpmsCmmService.getIndexFormula();
		
		ArrayList<HashMap<String, Object>> res = tpmsRoadSurveyMasterService.selectRoadSectionList(param, resourceUrl, formula);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}	
	
	@RequestMapping(value="/tpms/selectRoadSectionLineExt.do", method=RequestMethod.POST)
	public ModelAndView selectRoadSectionLineExt(@RequestParam HashMap<String, Object> param) {
		
		HashMap<String, Object> res = tpmsRoadSurveyMasterService.selectRoadSectionLineExt(param);

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}
	
}
