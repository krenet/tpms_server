package egovframework.com.tpms.rsm.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.cmm.service.TpmsCmmService;
import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterService;
import egovframework.com.tpms.rsm.service.TpmsDetailTargetService;

@Controller
public class TpmsDetailTargetController {
	
	@Resource(name="tpmsDetailTargetService")
	private TpmsDetailTargetService tpmsDetailTargetService;
	
	@Resource(name="tpmsCmmService")
	private TpmsCmmService tpmsCmmService;
	
	@Resource(name="tpmsRoadMasterService")
	private TpmsRoadMasterService tpmsRoadMasterService;

//	@RequestMapping(value="/tpms/getAllDetail.do", method=RequestMethod.POST)
//	public ModelAndView getAllDetail(){
//		
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("jsonView");
//		mav.addObject("res", tpmsDetailTargetService.getAllDetail());	
//		
//		return mav;
//	}
	
//	@RequestMapping(value="/tpms/getCurrYearDetail.do", method=RequestMethod.POST)
//	public ModelAndView getCurrYearDetail(@RequestParam int year){
//		
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("jsonView");
//		mav.addObject("res", tpmsDetailTargetService.getCurrYearDetail(year));	
//		
//		return mav;
//	}
	
//	@RequestMapping(value="/tpms/getAllMaster.do", method=RequestMethod.POST)
//	public ModelAndView getAllMaster(){
//		
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("jsonView");
//		
//		List<Map<String, Object>> masterList = tpmsDetailTargetService.getAllMaster();
//		if(masterList.size()>0) masterList = tpmsCmmService.dataReplace(masterList);
//		
//		mav.addObject("res", masterList);	
//		
//		return mav;
//	}
	@RequestMapping(value="/tpms/getAllDetail.do", method=RequestMethod.POST)
	public ModelAndView getAllDetail(){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsDetailTargetService.getAllDetail());	
		mav.addObject("yearList", tpmsDetailTargetService.getYearList());
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getCurrYearDetail.do", method=RequestMethod.POST)
	public ModelAndView getCurrYearDetail(@RequestParam int year){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsDetailTargetService.getCurrYearDetail(year));	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getAllMaster.do", method=RequestMethod.POST)
	public ModelAndView getAllMaster(){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		List<Map<String, Object>> masterList = tpmsDetailTargetService.getAllMaster();
		if(masterList.size()>0) masterList = tpmsCmmService.dataReplace(masterList);
		
		mav.addObject("res", masterList);	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getCurrYearEquip.do", method=RequestMethod.POST)
	public ModelAndView getCurrYearEquip(){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsDetailTargetService.getCurrYearEquip());	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/searchMaster.do", method=RequestMethod.POST)
	public ModelAndView searchMaster(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		List<Map<String, Object>> searchMasterList = tpmsDetailTargetService.searchMaster(map);
		if(searchMasterList.size()>0) searchMasterList = tpmsCmmService.dataReplace(searchMasterList);
		
		mav.addObject("res", searchMasterList);	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/searchEquip.do", method=RequestMethod.POST)
	public ModelAndView searchEquip(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("res", tpmsDetailTargetService.searchEquip(map));	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/addToDetail.do", method=RequestMethod.POST)
	public ModelAndView addToDetail(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
//		System.out.println("파람파람: "+map);
		
		List<Map<String, Object>> detailList = tpmsDetailTargetService.addToDetail(map);
		detailList = tpmsCmmService.getTargetId("D", detailList);
//		System.out.println("상세조사구간ID: "+tpmsCmmService.getTargetId("D", detailList));
		mav.addObject("res", detailList);
//		System.out.println("결과는요~? "+ detailList);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/saveDetail.do", method=RequestMethod.POST)
	public ModelAndView saveDetail(@RequestParam HashMap<String, Object> map){
	
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsDetailTargetService.saveDetail(map));
		int result = tpmsDetailTargetService.saveDetail(map);
//		if(result==0) {
//			mav.addObject("res", "삭제 성공, 저장 실패");
//		}else if(result==1) {
//			mav.addObject("res", "저장 성공");
//		}else if(result==2) {
//			mav.addObject("res", "모두 실패");
//		}
		mav.addObject("res", result);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/searchDetail.do", method=RequestMethod.POST)
	public ModelAndView searchDetail(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
//		ArrayList<String> noList = tpmsRoadMasterService.getNoList(map);
//		System.out.println("noList: "+noList);
		
		mav.addObject("res", tpmsDetailTargetService.searchDetail(map));	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getSelectedTS_ID.do", method=RequestMethod.POST)
	public ModelAndView getSelectedTS_ID(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("res", tpmsDetailTargetService.getSelectedTS_ID(map));	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getTargetTsList.do", method=RequestMethod.POST)
	public ModelAndView getTargetTsList(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("res", tpmsDetailTargetService.getTargetTsList(map));	
		
		return mav;
	}
	
}