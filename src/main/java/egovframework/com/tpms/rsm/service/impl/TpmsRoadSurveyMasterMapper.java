package egovframework.com.tpms.rsm.service.impl;

import java.util.ArrayList;
import java.util.HashMap;

import egovframework.com.tpms.rsm.service.Tpms10mVO;
import egovframework.com.tpms.rsm.service.TpmsCrackVO;
import egovframework.com.tpms.rsm.service.TpmsFileLogVO;
import egovframework.com.tpms.rsm.service.TpmsGprVO;
import egovframework.com.tpms.rsm.service.TpmsHwdVO;
import egovframework.com.tpms.rsm.service.TpmsPesVO;
import egovframework.com.tpms.rsm.service.TpmsRoadSurveyMasterVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsRoadSurveyMasterMapper")
public interface TpmsRoadSurveyMasterMapper {
	//<select id="getRoadSurveyMasterStartTsId" parameterType="String" resultType="long">
	public long getRoadSurveyMasterStartTsId(String target_id);
	//<select id="getRoadSurveyMasterEndTsId" parameterType="String" resultType="long">
	public long getRoadSurveyMasterEndTsId(String target_id);
	//<select id="selectRoadSurveyMasterNewVersionCheck" resultType="int">
	public int selectRoadSurveyMasterNewVersionCheck();
	//insertRoadSurveyMasterNewVersion
	public void insertRoadSurveyMasterNewVersion();
	//<select id="selectRoadSurveyMasterMaxVersion" resultType="int">
	public int selectRoadSurveyMasterMaxVersion();
	//<insert id="insertRoadSurvey1DataToMaster" parameterType="tpmsRoadSurveyMasterVO">
	public int insertRoadSurvey1DataToMaster(TpmsRoadSurveyMasterVO vo);
	//<select id="select10mIdListForPesData" parameterType="String" resultType="long">
	public long[] select10mIdListForPesData(String target_id);
	//<select id="selectPesDataNewVersionCheck" resultType="int">
	public int selectPesDataNewVersionCheck();
	//insertPesNewVersion
	public void insertPesNewVersion();
	//<insert id="insertPesData" parameterType="tpmsPesVO">
	public int insertPesData(TpmsPesVO vo);
	//<select id="selectExistsTargetIdCnt" parameterType="String" resultType="int">
	public int selectExistsTargetIdCnt(String target_id);
	//<insert id="insertRoadSurveyMasterUpdateVersion" parameterType="int">
	public void insertRoadSurveyMasterUpdateVersion(int version);
	//<select id="selectExistsTargetIdCntPes" parameterType="String" resultType="int">
	public int selectExistsTargetIdCntPes(String target_id);
	//<select id="selectPesDataMaxVersionCheck" resultType="int">
	public int selectPesDataMaxVersionCheck();
	//<insert id="insertPesDataUpdateVersion" parameterType="int">
	public void insertPesDataUpdateVersion(int version);
	//<insert id="insertFileLogData" parameterType="tpmsFileLogVO">
	public void insertFileLogData(TpmsFileLogVO vo);
	//<select id="selectExistsFileLogCheck" resultType="int" parameterType="tpmsFileLogVO">
	public int selectExistsFileLogCheck(TpmsFileLogVO vo);
	//<update id="updateFileLog" parameterType="tpmsFileLogVO">
	public int updateFileLog(TpmsFileLogVO vo);
	//<select id="selectWaitLogCntCheck" parameterType="tpmsFileLogVO" resultType="int">
	public int selectWaitLogCntCheck(TpmsFileLogVO vo);
	//<insert id="insertCrackData" parameterType="tpmsCrackVO">
	public int insertCrackData(TpmsCrackVO vo);
	//<select id="select10mListForCrack" parameterType="map" resultType="tpms10mVO">
	public ArrayList<Tpms10mVO> select10mListForCrack(TpmsCrackVO vo); 
	//<select id="selectDetailTargetRow" parameterType="String" resultType="map">
	public HashMap<String, Object> selectDetailTargetRow(String target);
	//<insert id="insertGprData" parameterType="tpmsGprVO">
	public int insertGprData(TpmsGprVO vo);
	//<insert id="insertHwdData" parameterType="tpmsHwdVO">
	public int insertHwdData(TpmsHwdVO vo);
	//<delete id="deleteCrackData" parameterType="String">
	public int deleteCrackData(String detail_target_id);
	//<update id="updateIsCrack" parameterType="String">
	public int updateIsCrack(String detail_target_id);
	//<delete id="deleteGprData" parameterType="String">
	public int deleteGprData(String detail_target_id);
	//<delete id="deleteHwdData" parameterType="String">
	public int deleteHwdData(String detail_target_id);
	//<update id="updateIsGpr" parameterType="String">
	public int updateIsGpr(String detail_target_id);
	//<update id="updateIsHwd" parameterType="String">
	public int updateIsHwd(String detail_target_id);
	//<select id="selectRdIri" parameterType="tpmsCrackVO" resultType="tpmsPesVO">
	public TpmsPesVO selectRdIri(TpmsCrackVO vo);
	//<update id="updateUnUsePesData" parameterType="String">
//	public void updateUnUsePesData(String target_id);
	//<update id="updateImgUploadState" parameterType="String">
	public void updateImgUploadState(String target_id);
	public void updateTcrack(TpmsCrackVO vo);
	//<select id="select10MCrackData" parameterType="Long" resultType="map">
	public HashMap<String, Object> select10MCrackData(Long id);  
	//<select id="select10MEquip1Data" parameterType="Long" resultType="map">
	public HashMap<String, Object> select10MEquip1Data(Long id);
	//<select id="select10MRepairData" parameterType="Long" resultType="map">
	public HashMap<String, Object> select10MRepairData(Long id);
	//<select id="selectSectionTsExtList" parameterType="map" resultType="map">
	public ArrayList<HashMap<String, Object>> selectSectionTsExtList(HashMap<String, Object> map);
	//<select id="selectSectionTs10mExtList" parameterType="Long" resultType="map">
	public ArrayList<HashMap<String, Object>> selectSectionTs10mExtList(Long ts_id);
	//<select id="selectPrevLinkData" parameterType="map" resultType="map">
	public HashMap<String, Object> selectPrevLinkData(HashMap<String, Object> param);
	public HashMap<String, Object> selectNextLinkData(HashMap<String, Object> param);
	//insertConvertStatus
	public int insertConvertStatus(String target_id);
	//<delete id="deleteEquip1Data" parameterType="String">
	public int deleteEquip1Data(String target_id);
	//<delete id="deleteSurveyMaster" parameterType="String">
	public int deleteSurveyMaster(String target_id);
	//<select id="select10MEquip1List" parameterType="map" resultType="map">
	public ArrayList<HashMap<String, Object>> select10MEquip1List(HashMap<String, Object> map); 
	//<select id="select10MRepairList" parameterType="map" resultType="map">
	public ArrayList<HashMap<String, Object>> select10MRepairList(HashMap<String, Object> map);
	//<select id="select10MCrackList" parameterType="map" resultType="map">
	public ArrayList<HashMap<String, Object>> select10MCrackList(HashMap<String, Object> map);
	
}
