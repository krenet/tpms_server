package egovframework.com.tpms.rsm.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TpmsDetailTargetService {

	public List<Map<String, Object>> getYearList();
	public List<Map<String, Object>> getAllDetail();
	public List<Map<String, Object>> getCurrYearDetail(int year);
	public List<Map<String, Object>> getAllMaster();
	public List<Map<String, Object>> getCurrYearEquip();
	public List<Map<String, Object>> searchMaster(Map<String, Object> map);
	public List<Map<String, Object>> searchEquip(Map<String, Object> map);
	public List<Map<String, Object>> addToDetail(Map<String, Object> map);
	public Map<String, Object> getNode_OrderNEQ1(Map<String, Integer> map);
	public Map<String, Object> getNode_OrderEQ1(Map<String, Integer> map);
	public String getStartExt(Map<String, Long> map);
	public String getEndExt(Map<String, Long> map);
	public int getS_T10m(int ts_id);
	public int getE_T10m(int ts_id);
	public int saveDetail(Map<String, Object> map);
	public List<Map<String, Object>> searchDetail(Map<String, Object> map);
	public List<Map<String, Object>> getSelectedTS_ID(Map<String, Object> map);
	// 2021-02-24 박혜원 추가 
	public List<Map<String, Object>> getTargetTsList(Map<String, Object> map);
	// 2021-02-24 박혜원 추가 
}