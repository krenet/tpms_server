package egovframework.com.tpms.rsm.service;

public class TpmsCrackViewVO {

	private long t10m_id;
	private long tsm_id;
	private long tri_id;
	private int dir;
	private int main_num;
	private int sub_num;
	private String geo;
	private int order;
	private long ts_id;
	private int t10m_order;
	private long crack_id;
	private String target_id;
	private String detail_target_id;
	private int extension;
	private String filename;
	private double line_cr;
	private double fetching;
	private double turtle_lamp_cr;
	private double pothole;
	private double etc;
	private double cr;
	private double spi;
	private double mpci;
	private double nhpci;
	private String date;
	
	
	public double getSpi() {
		return spi;
	}
	public void setSpi(double spi) {
		this.spi = spi;
	}
	public double getMpci() {
		return mpci;
	}
	public void setMpci(double mpci) {
		this.mpci = mpci;
	}
	public double getNhpci() {
		return nhpci;
	}
	public void setNhpci(double nhpci) {
		this.nhpci = nhpci;
	}
	public long getT10m_id() {
		return t10m_id;
	}
	public void setT10m_id(long t10m_id) {
		this.t10m_id = t10m_id;
	}
	public long getTsm_id() {
		return tsm_id;
	}
	public void setTsm_id(long tsm_id) {
		this.tsm_id = tsm_id;
	}
	public long getTri_id() {
		return tri_id;
	}
	public void setTri_id(long tri_id) {
		this.tri_id = tri_id;
	}
	public int getDir() {
		return dir;
	}
	public void setDir(int dir) {
		this.dir = dir;
	}
	public int getMain_num() {
		return main_num;
	}
	public void setMain_num(int main_num) {
		this.main_num = main_num;
	}
	public int getSub_num() {
		return sub_num;
	}
	public void setSub_num(int sub_num) {
		this.sub_num = sub_num;
	}
	public String getGeo() {
		return geo;
	}
	public void setGeo(String geo) {
		this.geo = geo;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public long getTs_id() {
		return ts_id;
	}
	public void setTs_id(long ts_id) {
		this.ts_id = ts_id;
	}
	public int getT10m_order() {
		return t10m_order;
	}
	public void setT10m_order(int t10m_order) {
		this.t10m_order = t10m_order;
	}
	public long getCrack_id() {
		return crack_id;
	}
	public void setCrack_id(long crack_id) {
		this.crack_id = crack_id;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getDetail_target_id() {
		return detail_target_id;
	}
	public void setDetail_target_id(String detail_target_id) {
		this.detail_target_id = detail_target_id;
	}
	public int getExtension() {
		return extension;
	}
	public void setExtension(int extension) {
		this.extension = extension;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public double getLine_cr() {
		return line_cr;
	}
	public void setLine_cr(double line_cr) {
		this.line_cr = line_cr;
	}
	public double getFetching() {
		return fetching;
	}
	public void setFetching(double fetching) {
		this.fetching = fetching;
	}
	public double getTurtle_lamp_cr() {
		return turtle_lamp_cr;
	}
	public void setTurtle_lamp_cr(double turtle_lamp_cr) {
		this.turtle_lamp_cr = turtle_lamp_cr;
	}
	public double getPothole() {
		return pothole;
	}
	public void setPothole(double pothole) {
		this.pothole = pothole;
	}
	public double getEtc() {
		return etc;
	}
	public void setEtc(double etc) {
		this.etc = etc;
	}
	public double getCr() {
		return cr;
	}
	public void setCr(double cr) {
		this.cr = cr;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
