package egovframework.com.tpms.rsm.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterVO;
import egovframework.com.tpms.rsm.service.TpmsPavementSurveyMasterService;

@Service("tpmsPavementSurveyMasterService")
@Transactional
public class TpmsPavementSurveyMasterImpl implements TpmsPavementSurveyMasterService{
	@Resource(name="tpmsPavementSurveyMasterMapper")
	private TpmsPavementSurveyMasterMapper tpmsPavementSurveyMasterMapper;

	@Override
	public List<Map<String, Object>> getSelectEquip1(HashMap<String,Object> map) {
		// TODO Auto-generated method stub
		String targetListStr = (String) map.get("target_id");
		List<String> targetList = new ArrayList<String>(Arrays.asList(targetListStr.split(",")));
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("target_list", targetList);
		
		List<Map<String, Object>> List = tpmsPavementSurveyMasterMapper.getSelectEquip1(param);
		return List;
	}

	@Override
	public List<Map<String, Object>> searchSurveyMaster(Map<String, Object> map) {
		
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadMasterVO vo = mapper.convertValue(map, TpmsRoadMasterVO.class);
		
		return tpmsPavementSurveyMasterMapper.searchSurveyMaster(vo);
	}

	@Override
	public List<Map<String, Object>> getEquip2Crack(HashMap<String, Object> map, Object formula) {
		String tsIdListStr = (String) map.get("ts_id");
		List<String> tsIdList = new ArrayList<String>(Arrays.asList(tsIdListStr.split(",")));
		List<Long> tsList = new ArrayList<>();
		for (String str : tsIdList) {
			tsList.add(Long.parseLong(str));
		}
		int year = Integer.parseInt((String)map.get("year"));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tsId_list", tsList);
		param.put("year", year);
		
		List<Map<String, Object>> List = tpmsPavementSurveyMasterMapper.getEquip2Crack(param);
		Map<String, Object> formulahMap = (Map<String, Object>) formula;
		String pav_idx = (String)formulahMap.get("formula");
		
		for(int i=0; i<List.size(); ++i) {
			List.get(i).put("crack", String.format("%.2f", List.get(i).get("cr")));
			List.get(i).put("pav_idx", String.format("%.2f", List.get(i).get(pav_idx)));
			List.get(i).put("pav_idx_origin", List.get(i).get(pav_idx));
		}
		
		return List;
	}

	@Override
	public List<Map<String, Object>> getEquip2Hwd(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		String tsIdListStr = (String) map.get("ts_id");
		List<String> tsIdList = new ArrayList<String>(Arrays.asList(tsIdListStr.split(",")));
		List<Long> tsList = new ArrayList<>();
		for (String str : tsIdList) {
			tsList.add(Long.parseLong(str));
		}
		int year = Integer.parseInt((String)map.get("year"));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tsId_list", tsList);
		param.put("year", year);
		
		List<Map<String, Object>> List = tpmsPavementSurveyMasterMapper.getEquip2Hwd(param);
		return List;
	}

	@Override
	public List<Map<String, Object>> getEquip2Gpr(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		String tsIdListStr = (String) map.get("ts_id");
		List<String> tsIdList = new ArrayList<String>(Arrays.asList(tsIdListStr.split(",")));
		List<Long> tsList = new ArrayList<>();
		for (String str : tsIdList) {
			tsList.add(Long.parseLong(str));
		}
		int year = Integer.parseInt((String)map.get("year"));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tsId_list", tsList);
		param.put("year", year);
		
		List<Map<String, Object>> List = tpmsPavementSurveyMasterMapper.getEquip2Gpr(param);
		return List;
	}

	@Override
	public List<Map<String, Object>> getEquip1RuttIri() {
		// TODO Auto-generated method stub
		
		List<Map<String, Object>> res = tpmsPavementSurveyMasterMapper.getEquip1RuttIri();
		return res;
	}

	@Override
	public List<Map<String, Object>> getEquip2ViewCrack() {
		// TODO Auto-generated method stub
		List<Map<String, Object>> res = tpmsPavementSurveyMasterMapper.getEquip2ViewCrack();
		return res;
	}
}
