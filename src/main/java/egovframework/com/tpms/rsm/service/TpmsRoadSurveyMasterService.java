package egovframework.com.tpms.rsm.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface TpmsRoadSurveyMasterService {
	// insert PES Data List
	//, Object readIniFilePath
	//, List<MultipartFile> imgFolder,
	public void insertPesDataList(List<MultipartFile> excelFolder, String table_columns, String table_types, String excel_columns, String excel_types);
	// insert PES Data
	public HashMap<String, Object> insertPesData(MultipartFile excelFile, String table_columns, String table_types, String excel_columns, String excel_types);
	// pes image copy
	public int buildFileData(MultipartFile file, Object iniFilePath);
	//insertCrData
	public HashMap<String, Object> insertCrackData(MultipartFile file, String columns, String types, String indexFormula);
	//insertGprData
	public HashMap<String, Object> insertGprData(MultipartFile file, String columns, String types);
	//inserHwdData
	public HashMap<String, Object> insertHwdData(MultipartFile file, String columns, String types);
	// select road section info
//	public HashMap<String, Object> selectRoadSectionInfo(Long id, String resourceUrl, String formula);
	public HashMap<String, Object> selectRoadSectionInfo(Long id, String resourceUrl, String formula);
	public ArrayList<HashMap<String, Object>> selectRoadSectionList(HashMap<String, Object> param, String resourceUrl, String formula);
	// select road section geo line extension
	public HashMap<String, Object> selectRoadSectionLineExt(HashMap<String, Object> param);
}
