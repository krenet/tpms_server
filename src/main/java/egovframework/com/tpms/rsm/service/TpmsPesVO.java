package egovframework.com.tpms.rsm.service;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TpmsPesVO {

	private long id;
	private long t10m_id;
	private String target_id;
	private int extension;
	private double rutting;
	private double left_iri;
	private double right_iri;
	private double cri;
	private double max_iri;
	private double avg_iri;
	private double cant;
	private double profile_slope;
	private double curve_radius;
	private double speed;
	private double latitude;
	private double longitude;
	private String front_img;
	private String pave_img;
	private int version;
	private boolean un_use;
	
	private int year;
	private int month;
	private int day;
	private String regdate;
	private String makedate;
	
	private String upload_state;
	private String convert_state;
	
	
	
	
	public String getUpload_state() {
		return upload_state;
	}

	public void setUpload_state(String upload_state) {
		this.upload_state = upload_state;
	}

	public String getConvert_state() {
		return convert_state;
	}

	public void setConvert_state(String convert_state) {
		this.convert_state = convert_state;
	}

	public boolean getUn_use() {
		return un_use;
	}

	public void setUn_use(boolean un_use) {
		this.un_use = un_use;
	}

	public String getMakedate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String date = format.format(new Date(this.year, this.month, this.day));
		this.makedate = date;
		
		return makedate;
	}
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getT10m_id() {
		return t10m_id;
	}
	public void setT10m_id(long t10m_id) {
		this.t10m_id = t10m_id;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public int getExtension() {
		return extension;
	}
	public void setExtension(int extension) {
		this.extension = extension;
	}
	public double getRutting() {
		return rutting;
	}
	public void setRutting(double rutting) {
		this.rutting = rutting;
	}
	public double getLeft_iri() {
		return left_iri;
	}
	public void setLeft_iri(double left_iri) {
		this.left_iri = left_iri;
	}
	public double getRight_iri() {
		return right_iri;
	}
	public void setRight_iri(double right_iri) {
		this.right_iri = right_iri;
	}
	public double getCri() {
		return cri;
	}
	public void setCri(double cri) {
		this.cri = cri;
	}
	public double getMax_iri() {
		return max_iri;
	}
	public void setMax_iri(double max_iri) {
		this.max_iri = max_iri;
	}
	public double getAvg_iri() {
		return avg_iri;
	}
	public void setAvg_iri(double avg_iri) {
		this.avg_iri = avg_iri;
	}
	public double getCant() {
		return cant;
	}
	public void setCant(double cant) {
		this.cant = cant;
	}
	public double getProfile_slope() {
		return profile_slope;
	}
	public void setProfile_slope(double profile_slope) {
		this.profile_slope = profile_slope;
	}
	public double getCurve_radius() {
		return curve_radius;
	}
	public void setCurve_radius(double curve_radius) {
		this.curve_radius = curve_radius;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getFront_img() {
		return front_img;
	}
	public void setFront_img(String front_img) {
		this.front_img = front_img;
	}
	public String getPave_img() {
		return pave_img;
	}
	public void setPave_img(String pave_img) {
		this.pave_img = pave_img;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	
}
