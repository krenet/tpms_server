package egovframework.com.tpms.rsm.service.impl;

import java.util.List;
import java.util.Map;

import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterVO;
import egovframework.com.tpms.rsm.service.TpmsPesVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsPavementSurveyMasterMapper")
public interface TpmsPavementSurveyMasterMapper {
	//<select id="getAll1Equip" resultType="map">
	public List<Map<String, Object>> getSelectEquip1(Map<String, Object> param);
	//<select id="searchSurveyMaster" parameterType="tpmsRoadMasterVO" resultType="map">
	public List<Map<String, Object>> searchSurveyMaster(TpmsRoadMasterVO vo);
	//<select id="getEquip2Crack" resultType="map" parameterType="map">
	public List<Map<String, Object>> getEquip2Crack(Map<String, Object> param);
	//<select id="getEquip2Hwd" resultType="map" parameterType="map">
	public List<Map<String, Object>> getEquip2Hwd(Map<String, Object> param);
	//<select id="getEquip2Gpr" resultType="map" parameterType="map">
	public List<Map<String, Object>> getEquip2Gpr(Map<String, Object> param);
	//<select id="getEquip1RuttIri" resultType="map">
	public List<Map<String, Object>> getEquip1RuttIri();
	//<select id="getEquip2ViewCrack" resultType="map">
	public List<Map<String, Object>> getEquip2ViewCrack();
}
