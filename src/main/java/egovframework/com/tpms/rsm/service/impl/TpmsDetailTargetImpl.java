package egovframework.com.tpms.rsm.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterVO;
import egovframework.com.tpms.rsm.service.TpmsDetailTargetService;
import egovframework.com.tpms.rsm.service.TpmsDetailTargetVO;


@Service("tpmsDetailTargetService")
@Transactional
public class TpmsDetailTargetImpl implements TpmsDetailTargetService {
	
	@Resource(name="tpmsDetailTargetMapper")
	private TpmsDetailTargetMapper tpmsDetailTargetMapper;
//
//	@Override
//	public List<Map<String, Object>> getAllDetail() {
//		// TODO Auto-generated method stub
//		return tpmsDetailTargetMapper.getAllDetail();
//	}

	@Override
	public List<Map<String, Object>> getYearList() {
	
		return tpmsDetailTargetMapper.getYearList();
	}

	@Override
	public List<Map<String, Object>> getAllDetail() {
		
		List<Map<String, Object>> list = tpmsDetailTargetMapper.getAllDetail();
		Map<String, Long> extParam = new HashMap<String, Long>();
		for(int i=0; i<list.size(); ++i) {
			extParam.put("s_ts_id", ((Number)list.get(i).get("s_ts_id")).longValue());
			extParam.put("s_ts_node_id",((Number)list.get(i).get("s_ts_node_id")).longValue());
			extParam.put("e_ts_id", ((Number)list.get(i).get("e_ts_id")).longValue());
			extParam.put("e_ts_node_id",((Number)list.get(i).get("e_ts_node_id")).longValue());
			String s_ext = tpmsDetailTargetMapper.getStartExt(extParam);
			String e_ext = tpmsDetailTargetMapper.getEndExt(extParam);
			if(s_ext==null) s_ext="0";
			if(e_ext==null) e_ext="0";
			list.get(i).put("s_ext", s_ext); // 시점 노드부터 몇 미터 떨어진 지점인지
			list.get(i).put("spos", list.get(i).get("s_node_name")+"+"+String.format("%.2f", (Double.parseDouble(s_ext)/1000))+"km");
			list.get(i).put("epos", list.get(i).get("e_node_name")+"+"+String.format("%.2f", (Double.parseDouble(e_ext)/1000))+"km");
		}
		
		return list;
	}

	@Override
	public List<Map<String, Object>> getCurrYearDetail(int year) {
		
		List<Map<String, Object>> list = tpmsDetailTargetMapper.getCurrYearDetail(year);
		Map<String, Long> extParam = new HashMap<String, Long>();
		for(int i=0; i<list.size(); ++i) {
			extParam.put("s_ts_id", ((Number)list.get(i).get("s_ts_id")).longValue());
			extParam.put("s_ts_node_id",((Number)list.get(i).get("s_ts_node_id")).longValue());
			extParam.put("e_ts_id", ((Number)list.get(i).get("e_ts_id")).longValue());
			extParam.put("e_ts_node_id",((Number)list.get(i).get("e_ts_node_id")).longValue());
			String s_ext = tpmsDetailTargetMapper.getStartExt(extParam);
			String e_ext = tpmsDetailTargetMapper.getEndExt(extParam);
			if(s_ext==null) s_ext="0";
			if(e_ext==null) e_ext="0";
			list.get(i).put("s_ext", s_ext); // 시점 노드부터 몇 미터 떨어진 지점인지
			list.get(i).put("spos", list.get(i).get("s_node_name")+"+"+String.format("%.2f", (Double.parseDouble(s_ext)/1000))+"km");
			list.get(i).put("epos", list.get(i).get("e_node_name")+"+"+String.format("%.2f", (Double.parseDouble(e_ext)/1000))+"km");
		}
		
		return list;
	}

	@Override
	public List<Map<String, Object>> getAllMaster() {
		
		List<Map<String, Object>> allMasterList = tpmsDetailTargetMapper.getAllMaster();
		
		if(allMasterList.size()>0) {
			for(int i=0; i<allMasterList.size(); i++) {
				allMasterList.get(i).put("roadNo", allMasterList.get(i).get("no"));
			}
		}
		
		return allMasterList;
	}
//	@Override
//	public List<Map<String, Object>> getAllMaster() {
//		
//		return tpmsDetailTargetMapper.getAllMaster();
//	}

	@Override
	public List<Map<String, Object>> getCurrYearEquip() {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		return tpmsDetailTargetMapper.getCurrYearEquip(year);
	}

	@Override
	public List<Map<String, Object>> searchMaster(Map<String, Object> map) {
		
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadMasterVO vo = mapper.convertValue(map, TpmsRoadMasterVO.class);
		
		return tpmsDetailTargetMapper.searchMaster(vo);
	}

	@Override
	public List<Map<String, Object>> searchEquip(Map<String, Object> map) {
		
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadMasterVO vo = mapper.convertValue(map, TpmsRoadMasterVO.class);
		
		return tpmsDetailTargetMapper.searchEquip(vo);
	}

	@Override
	public List<Map<String, Object>> addToDetail(Map<String, Object> map) {
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		List<String> keyList = new ArrayList<String>(map.keySet());
		String valueStr = null;
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<HashMap<String, Object>> list = null;
		List<Map<String, Object>> detailList = new ArrayList<Map<String,Object>>();
		for (String key : keyList) {
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				valueStr = java.net.URLDecoder.decode((String) map.get(key), "UTF-8");
				list = mapper.readValue(valueStr, new TypeReference<ArrayList<HashMap<String, Object>>>() {});
				System.out.println("무엇이냐: "+key+": "+list);
			
				if(list.get(0).get("detail_target_id") !=null) {
					System.out.println("무엇이냐: "+list);
					list.get(0).remove("in_code");
					list.get(0).remove("detail_target_id");
					detailList.add(list.get(0));
				}else {
					result.put("year", year);
					result.put("rank", list.get(0).get("rank"));
					result.put("no", list.get(0).get("no"));
					result.put("name", list.get(0).get("name"));
					result.put("main_num", list.get(0).get("main_num"));
					result.put("sub_num", list.get(0).get("sub_num"));
					result.put("dir", list.get(0).get("dir"));
					result.put("lanes", list.get(0).get("lane"));
					result.put("sector", list.get(0).get("sector"));
					result.put("section", list.get(0).get("section"));
					result.put("s_ts_id", list.get(0).get("ts_id"));
					result.put("target_id", list.get(0).get("target_id"));
					result.put("tri_id", list.get(0).get("tri_id"));
					result.put("tsm_id", list.get(0).get("tsm_id"));
					result.put("group_num", list.get(0).get("group_num"));
//				if(list.get(0).get("s_t10m_id") !=null) {
//					result.put("s_t10m_id", list.get(0).get("s_t10m_id"));
//				}else {
//					result.put("s_t10m_id", tpmsDetailTargetMapper.get_s_t10m((Integer)list.get(0).get("ts_id")));					
//				}
					result.put("s_t10m_id", tpmsDetailTargetMapper.get_s_t10m((Integer)list.get(0).get("ts_id")));					
					
					HashMap<String, Integer> paramMap = new HashMap<String, Integer>();
					paramMap.put("tsm_id", (Integer)list.get(0).get("tsm_id"));
					paramMap.put("order", (Integer)list.get(0).get("order"));
					
					Map<String, Long> extParam = new HashMap<String, Long>();
					extParam.put("s_ts_id", ((Number)list.get(0).get("ts_id")).longValue());
					// 시점
					Map<String, Object> s_node = new HashMap<String, Object>();
					Long s_ts_node_id=null;
					if(paramMap.get("order")!=1) {
						s_node = tpmsDetailTargetMapper.getNode_OrderNEQ1(paramMap);
						s_ts_node_id = (Long)s_node.get("id");
						if(s_node.size()==0) {
							s_node = tpmsDetailTargetMapper.getNode_OrderEQ1(paramMap);
							s_ts_node_id = (Long)s_node.get("ts_id");
						}
					}else {
						s_node = tpmsDetailTargetMapper.getNode_OrderEQ1(paramMap);	
						s_ts_node_id = (Long)s_node.get("ts_id");
					}
					extParam.put("s_ts_node_id", s_ts_node_id);
					String s_ext = tpmsDetailTargetMapper.getStartExt(extParam);
					if(s_ext==null) s_ext="0";
					result.put("s_ts_node_id", s_ts_node_id);
					result.put("s_ext", s_ext);  // 시점 노드부터 몇 미터 떨어진 지점인지
					result.put("s_node_name", s_node.get("node_name"));  // 시점 노드 이름
					result.put("s_node_type", s_node.get("node_type"));  // 시점 노드 타입
					result.put("spos", s_node.get("node_name")+"+"+String.format("%.2f", (Double.parseDouble(s_ext)/1000))+"km");
					
					// 종점
					Map<String, Object> e_node = new HashMap<String, Object>();
					paramMap.replace("tsm_id", (Integer)list.get(list.size()-1).get("tsm_id"));
					paramMap.replace("order", (Integer)list.get(list.size()-1).get("order"));
					result.put("e_ts_id", list.get(list.size()-1).get("ts_id"));
					extParam.put("e_ts_id", ((Number)list.get(list.size()-1).get("ts_id")).longValue());
					result.put("e_t10m_id", tpmsDetailTargetMapper.get_e_t10m((Integer)list.get(list.size()-1).get("ts_id")));

					Long e_ts_node_id=null;
					if(paramMap.get("order")!=1) {
						e_node = tpmsDetailTargetMapper.getNode_OrderNEQ1(paramMap);
						e_ts_node_id = (Long)e_node.get("id");
						if(e_node.size()==0) {
							e_node = tpmsDetailTargetMapper.getNode_OrderEQ1(paramMap);
							e_ts_node_id = (Long)e_node.get("ts_id");
						}
					}else {
						e_node = tpmsDetailTargetMapper.getNode_OrderEQ1(paramMap);
						e_ts_node_id = (Long)e_node.get("ts_id");
					}
					extParam.put("e_ts_node_id", e_ts_node_id);
					String e_ext = tpmsDetailTargetMapper.getEndExt(extParam);
					if(e_ext==null) e_ext="0";
					result.put("e_ts_node_id", e_ts_node_id);
					result.put("e_ext", e_ext);  // 종점 노드부터 몇 미터 떨어진 지점인지
					result.put("e_node_name", e_node.get("node_name"));  // 종점 노드 이름
					result.put("e_node_type", e_node.get("node_type"));  // 종점 노드 타입
					result.put("epos", e_node.get("node_name")+"+"+String.format("%.2f", (Double.parseDouble(e_ext)/1000))+"km");
					
					// 총연장 길이
					double total_ext = 0;
					for(int k=0; k<list.size(); ++k) {
						total_ext +=(double)list.get(k).get("sm_ext");
					}
					result.put("total_ext", total_ext);
					
					detailList.add(result);
				}
				
				
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return detailList;
	}


	@Override
	public int saveDetail(Map<String, Object> map) {
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		List<Map<String,Object>> currYearList = tpmsDetailTargetMapper.getCurrYearDetail(year);
		boolean del_result = true;
		if(currYearList.size()>0) {
			// 해당년도 상세조사구간 모두 삭제
			int del = tpmsDetailTargetMapper.delCurrYearDetail(year);			
			if(del != currYearList.size()) del_result = false;
		}
		// 상세조사구간 새로 저장
		if(del_result) {
//			List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("list");
			ObjectMapper mapper = new ObjectMapper();
			String valueStr = null;
			try {
				valueStr = java.net.URLDecoder.decode((String) map.get("list"), "UTF-8");
				List<Map<String, Object>> list = mapper.readValue(valueStr, new TypeReference<ArrayList<HashMap<String, Object>>>() {});
				List<Map<String, Object>> newList = new ArrayList<Map<String,Object>>();
				for(int i=0; i<list.size(); ++i) {
					Map<String, Object> dataMap = new HashMap<String, Object>();
					dataMap.put("target_id", list.get(i).get("target_id"));
					dataMap.put("detail_target_id", list.get(i).get("detail_target_id"));
					dataMap.put("s_t10m_id", list.get(i).get("s_t10m_id"));
					dataMap.put("e_t10m_id", list.get(i).get("e_t10m_id"));
					dataMap.put("s_ts_id", list.get(i).get("s_ts_id"));
					dataMap.put("e_ts_id", list.get(i).get("e_ts_id"));
					dataMap.put("in_code", list.get(i).get("in_code"));
					dataMap.put("year", list.get(i).get("year"));
					dataMap.put("dir", list.get(i).get("dir"));
					dataMap.put("tsm_id", list.get(i).get("tsm_id"));
					dataMap.put("total_ext", list.get(i).get("total_ext"));
					dataMap.put("s_node_name", list.get(i).get("s_node_name"));
					dataMap.put("e_node_name", list.get(i).get("e_node_name"));
					dataMap.put("s_ts_node_id", list.get(i).get("s_ts_node_id"));
					dataMap.put("e_ts_node_id", list.get(i).get("e_ts_node_id"));
					newList.add(dataMap);
				}
				
				ObjectMapper objectMapper = new ObjectMapper();
				List<TpmsDetailTargetVO> voList = objectMapper.convertValue(newList, new TypeReference<List<TpmsDetailTargetVO>>() {});	
				int chk=0;
				
				for (TpmsDetailTargetVO tpmsDetailTargetVO : voList) {
					int result = tpmsDetailTargetMapper.insertDetail(tpmsDetailTargetVO);
					if(result==1) chk +=1;
				}
				
				if(chk == list.size()) {
					return 1;
				}else {
					return 0;
				}
				
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 0;
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 0;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 0;
			}

		}else {
			return 2;			
		}
	}

	
	// 뭐지?
	@Override
	public List<Map<String, Object>> searchDetail(Map<String, Object> map) {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		map.put("year", year);
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadMasterVO vo = mapper.convertValue(map, TpmsRoadMasterVO.class);
//		System.out.println("검색 결과: "+tpmsDetailTargetMapper.searchDetail(vo));
		return tpmsDetailTargetMapper.searchDetail(vo);
	}

	@Override
	public Map<String, Object> getNode_OrderNEQ1(Map<String, Integer> map) {
		
		return tpmsDetailTargetMapper.getNode_OrderNEQ1(map);
	}

	@Override
	public Map<String, Object> getNode_OrderEQ1(Map<String, Integer> map) {
		
		return tpmsDetailTargetMapper.getNode_OrderEQ1(map);
	}

	@Override
	public String getStartExt(Map<String, Long> map) {
		
		return tpmsDetailTargetMapper.getStartExt(map);
	}

	@Override
	public String getEndExt(Map<String, Long> map) {
		// TODO Auto-generated method stub
		return tpmsDetailTargetMapper.getEndExt(map);
	}

	@Override
	public int getS_T10m(int ts_id) {
		// TODO Auto-generated method stub
		return tpmsDetailTargetMapper.get_s_t10m(ts_id);
	}
	
	@Override
	public int getE_T10m(int ts_id) {
		// TODO Auto-generated method stub
		return tpmsDetailTargetMapper.get_e_t10m(ts_id);
	}
	
	
	
	@Override
	public List<Map<String, Object>> getSelectedTS_ID(Map<String, Object> map) {
		
		String str = (String) map.get("detail_ids");
		String[] idArr = str.split(",");
		List<Map<String, Object>> allList = new ArrayList<Map<String,Object>>();
		for(int i=0; i<idArr.length; ++i) {
			List<Map<String, Object>> list =  tpmsDetailTargetMapper.getSelectedTS_ID(idArr[i]);
			for(int j=0; j<list.size(); ++j) {
				allList.add(list.get(j));
			}
		}
		
		return allList;
	}

	// 2021-02-24 박혜원 추가 
	@Override
	public List<Map<String, Object>> getTargetTsList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		
		ObjectMapper mapper = new ObjectMapper();
		String valueStr = null;
		List<Map<String, Object>> list = null;
		try {
			valueStr = java.net.URLDecoder.decode((String) map.get("list"), "UTF-8");
			list = mapper.readValue(valueStr, new TypeReference<ArrayList<HashMap<String, Object>>>() {});
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ArrayList<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
		if(list.size()>0) {
			Map<String, Integer> param = null;
			for (Map<String, Object> obj : list) {
				param = new HashMap<String, Integer>();
				param.put("s_ts_id", (Integer) obj.get("s_ts_id"));
				param.put("e_ts_id", (Integer) obj.get("e_ts_id"));
				obj.put("tsList", tpmsDetailTargetMapper.selectTargetTsList(param));
				
				res.add(obj);
			}
		}
		
		return res;
	}
	// 2021-02-24 박혜원 추가 


}