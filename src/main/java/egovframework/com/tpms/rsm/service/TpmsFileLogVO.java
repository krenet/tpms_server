package egovframework.com.tpms.rsm.service;

public class TpmsFileLogVO {

	private long id;
	private String code;
	private String filename;
	private String status;
	private String target;
	private String c_date;
	
	public TpmsFileLogVO() {
		
	}
	
	public TpmsFileLogVO(String code, String filename, String status, String target, String c_date) {
		super();
		this.code = code;
		this.filename = filename;
		this.status = status;
		this.target = target;
		this.c_date = c_date;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getC_date() {
		return c_date;
	}
	public void setC_date(String c_date) {
		this.c_date = c_date;
	}
	
	
}
