package egovframework.com.tpms.rsm.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TpmsPavementSurveyMasterService {
//	public List
	public List<Map<String, Object>> getSelectEquip1(HashMap<String,Object> map);
	public List<Map<String, Object>> searchSurveyMaster(Map<String, Object> map);
	public List<Map<String, Object>> getEquip2Crack(HashMap<String,Object> map, Object formula);
	public List<Map<String, Object>> getEquip2Hwd(HashMap<String,Object> map);
	public List<Map<String, Object>> getEquip2Gpr(HashMap<String,Object> map);
	public List<Map<String, Object>> getEquip1RuttIri();
	public List<Map<String, Object>> getEquip2ViewCrack();
}
