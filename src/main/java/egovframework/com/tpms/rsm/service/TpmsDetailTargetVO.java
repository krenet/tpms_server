package egovframework.com.tpms.rsm.service;

public class TpmsDetailTargetVO {
	private long id;
	private String target_id;
	private String detail_target_id;
	private long s_t10m_id;
	private long e_t10m_id;
	private long s_ts_id;
	private long e_ts_id;
	private String in_code;
	private int year;
	private int dir;
	private int lane;
	private long tsm_id;
	private boolean is_crack;
	private boolean is_gpr;
	private boolean is_hwd;
	private double total_ext;
	private String s_node_name;
	private String e_node_name;
	private long s_ts_node_id;
	private long e_ts_node_id;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getDetail_target_id() {
		return detail_target_id;
	}
	public void setDetail_target_id(String detail_target_id) {
		this.detail_target_id = detail_target_id;
	}
	public long getS_t10m_id() {
		return s_t10m_id;
	}
	public void setS_t10m_id(long s_t10m_id) {
		this.s_t10m_id = s_t10m_id;
	}
	public long getE_t10m_id() {
		return e_t10m_id;
	}
	public void setE_t10m_id(long e_t10m_id) {
		this.e_t10m_id = e_t10m_id;
	}
	public long getS_ts_id() {
		return s_ts_id;
	}
	public void setS_ts_id(long s_ts_id) {
		this.s_ts_id = s_ts_id;
	}
	public long getE_ts_id() {
		return e_ts_id;
	}
	public void setE_ts_id(long e_ts_id) {
		this.e_ts_id = e_ts_id;
	}
	public String getIn_code() {
		return in_code;
	}
	public void setIn_code(String in_code) {
		this.in_code = in_code;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getDir() {
		return dir;
	}
	public void setDir(int dir) {
		this.dir = dir;
	}
	public int getLane() {
		return lane;
	}
	public void setLane(int lane) {
		this.lane = lane;
	}
	public long getTsm_id() {
		return tsm_id;
	}
	public void setTsm_id(long tsm_id) {
		this.tsm_id = tsm_id;
	}
	public boolean isIs_crack() {
		return is_crack;
	}
	public void setIs_crack(boolean is_crack) {
		this.is_crack = is_crack;
	}
	public boolean isIs_gpr() {
		return is_gpr;
	}
	public void setIs_gpr(boolean is_gpr) {
		this.is_gpr = is_gpr;
	}
	public boolean isIs_hwd() {
		return is_hwd;
	}
	public void setIs_hwd(boolean is_hwd) {
		this.is_hwd = is_hwd;
	}
	public double getTotal_ext() {
		return total_ext;
	}
	public void setTotal_ext(double total_ext) {
		this.total_ext = total_ext;
	}
	public String getS_node_name() {
		return s_node_name;
	}
	public void setS_node_name(String s_node_name) {
		this.s_node_name = s_node_name;
	}
	public String getE_node_name() {
		return e_node_name;
	}
	public void setE_node_name(String e_node_name) {
		this.e_node_name = e_node_name;
	}
	public long getS_ts_node_id() {
		return s_ts_node_id;
	}
	public void setS_ts_node_id(long s_ts_node_id) {
		this.s_ts_node_id = s_ts_node_id;
	}
	public long getE_ts_node_id() {
		return e_ts_node_id;
	}
	public void setE_ts_node_id(long e_ts_node_id) {
		this.e_ts_node_id = e_ts_node_id;
	}

}
