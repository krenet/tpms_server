package egovframework.com.tpms.rsm.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import egovframework.com.tpms.cmm.service.FileUploadService;
import egovframework.com.tpms.rsm.service.Tpms10mVO;
import egovframework.com.tpms.rsm.service.TpmsCrackVO;
import egovframework.com.tpms.rsm.service.TpmsFileLogVO;
import egovframework.com.tpms.rsm.service.TpmsGprVO;
import egovframework.com.tpms.rsm.service.TpmsHwdVO;
import egovframework.com.tpms.rsm.service.TpmsPesVO;
import egovframework.com.tpms.rsm.service.TpmsRoadSurveyMasterService;
import egovframework.com.tpms.rsm.service.TpmsRoadSurveyMasterVO;

@Service("tpmsRoadSurveyMasterService")
@Transactional
public class TpmsRoadSurveyMasterImpl implements TpmsRoadSurveyMasterService{

	@Resource(name="tpmsRoadSurveyMasterMapper")
	private TpmsRoadSurveyMasterMapper tpmsRoadSurveyMasterMapper;
	
	@Autowired
	FileUploadService fileUploadService;

//	private TpmsGprVO vo;
	
	@Override
	public void insertPesDataList(List<MultipartFile> excelFiles, String table_columns, String table_types, String excel_columns, String excel_types) {
		
		String[] fieldNames = excel_columns.split(",");
		String[] fieldTypes = excel_types.split(",");

		/** excel file parsing */
		ArrayList<TpmsPesVO[]> excelParsingList = new ArrayList<>();
		for (MultipartFile excelFile : excelFiles) {
			TpmsPesVO[] pesList = makePesExcelParsing(excelFile, fieldNames, fieldTypes);
			if(pesList!=null) excelParsingList.add(pesList);
			// else return 실패결과값
		}
		
		String target_id = null;
		double extension = 0.0;
		double t_rutting = 0.0;
		double t_iri = 0.0;
		TpmsRoadSurveyMasterVO rsmVO = null;
		int version = 0;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String str_date = null;
		Date date = null;
		for (TpmsPesVO[] pesList : excelParsingList) {
			target_id = pesList[0].getTarget_id();
			
			for(TpmsPesVO vo : pesList) {
							
				extension = vo.getExtension();
				t_rutting += vo.getRutting();
				t_iri += vo.getAvg_iri();

			}
			/** station master data insert */
			/* tpms road survey master insert */
			// s_ts_id, e_ts_id, target_id, rutt, iri, t_rutting, t_iri, extension, date, version, un_use, -- t_crack, index, is_detail
			rsmVO = new TpmsRoadSurveyMasterVO();
			rsmVO.setRutt(false);
			rsmVO.setIri(false);
			if(t_rutting>0) rsmVO.setRutt(true);
			if(t_iri>0) rsmVO.setIri(true);
			rsmVO.setS_ts_id(tpmsRoadSurveyMasterMapper.getRoadSurveyMasterStartTsId(target_id));
			rsmVO.setE_ts_id(tpmsRoadSurveyMasterMapper.getRoadSurveyMasterEndTsId(target_id));
			rsmVO.setTarget_id(target_id);
			rsmVO.setT_rutting(t_rutting);
			rsmVO.setT_iri(t_iri);
			rsmVO.setExtension(extension);
			rsmVO.setYear(pesList[0].getYear()); 
			rsmVO.setMonth(pesList[0].getMonth());
			rsmVO.setDay(pesList[0].getDay());
			// 버전 생성
			version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterNewVersionCheck();
			if(version==0) {
				tpmsRoadSurveyMasterMapper.insertRoadSurveyMasterNewVersion();
				version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterNewVersionCheck();
			}
			rsmVO.setVersion(version);
			rsmVO.setUn_use(false);
			str_date = pesList[0].getYear() + "-" + pesList[0].getMonth() + "-" + pesList[0].getDay();
			try {
				date = format.parse(str_date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			str_date = format.format(date);
			rsmVO.setDate(str_date);
			tpmsRoadSurveyMasterMapper.insertRoadSurvey1DataToMaster(rsmVO);
			t_rutting = 0.0;
			t_iri = 0.0;
			/* tpms road survey master insert */
			
			/** station master data insert */
		}
		
		/* PES table insert */
		int pesInx = 0;
		for (TpmsPesVO[] pesList : excelParsingList) {
			target_id = pesList[0].getTarget_id();
			int inx = 0;
			long[] t10mIdList = tpmsRoadSurveyMasterMapper.select10mIdListForPesData(target_id);
			int t10mIdListLen = t10mIdList.length;
			int pesListLen = pesList.length;
			
			
			for(long t10mId : t10mIdList) {
				version = tpmsRoadSurveyMasterMapper.selectPesDataNewVersionCheck();
				if(version==0) {
					tpmsRoadSurveyMasterMapper.insertPesNewVersion();
					version = tpmsRoadSurveyMasterMapper.selectPesDataNewVersionCheck();
				}
				
				pesInx = (int)(Math.round(((double)pesListLen/(double)t10mIdListLen)*((double)(inx+1)))) -1;
				
				pesList[pesInx].setT10m_id(t10mId);
				pesList[pesInx].setVersion(version);
				pesList[pesInx].setUn_use(false);
				pesList[pesInx].setUpload_state("wait");
				tpmsRoadSurveyMasterMapper.insertPesData(pesList[pesInx]);
				++inx;
			}
			
		}
		/* PES table insert */
	}

	public TpmsPesVO[] makePesExcelParsing(MultipartFile excel, String[] fieldNames, String[] fieldTypes) {
		
		if(excel.getSize()>0) {
			String path = fileUploadService.restore(excel); 
			
			if(!(path.toLowerCase().endsWith(".xlsx")||path.toLowerCase().endsWith(".csv"))) {
				return null;
			}
			
			FileInputStream fs;
			ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
			XSSFWorkbook workbook = null;
			try {
				fs = new FileInputStream(new File(path));
				OPCPackage opcPackage = OPCPackage.open(fs);
				workbook = new XSSFWorkbook(opcPackage);
				opcPackage.close();
			} catch (InvalidFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			
			XSSFSheet sheet = workbook.getSheetAt(0);
			// sheet row
			Iterator<Row> rowIterator = sheet.iterator();
			// 한 행 저장
			HashMap<String, Object> oneRow = null;
			Row row;
			int rowCnt = 0;
			
			while(rowIterator.hasNext()) {
				row = rowIterator.next();
				if(++rowCnt < 3) continue;
				
				Iterator<Cell> cellIterator = row.cellIterator();
				oneRow = new HashMap<>();
				while(cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					if(cell.getColumnIndex()==0&&cell.getCellType()==Cell.CELL_TYPE_BLANK) break;
					
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						oneRow.put(fieldNames[cell.getColumnIndex()], cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						String value = "";
						if(fieldTypes[cell.getColumnIndex()].equals("string")) {
							value = String.valueOf(Math.floor(cell.getNumericCellValue()));
							if(value.contains(".")) {
								value = value.split("\\.")[0];
								oneRow.put(fieldNames[cell.getColumnIndex()], value);
							}
						}else {
							oneRow.put(fieldNames[cell.getColumnIndex()], cell.getNumericCellValue());
						}
						
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						oneRow.put(fieldNames[cell.getColumnIndex()], cell.getBooleanCellValue());
						break;
					case Cell.CELL_TYPE_BLANK:
						if(fieldTypes[cell.getColumnIndex()].equals("int")) {
							oneRow.put(fieldNames[cell.getColumnIndex()], 0);
						}else {
							oneRow.put(fieldNames[cell.getColumnIndex()], null);
						}
						break;
					default:
						break;
					}
					if(fieldTypes.length-1 == cell.getColumnIndex()) break;
				}
				if(oneRow.size()>0&&oneRow.get(fieldNames[0])!=null) rowList.add(oneRow);
				else break;
			}
			
			try {
				fs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;
			TpmsPesVO[] voList = null;
			try {
				jsonStr = objectMapper.writeValueAsString(rowList);
				voList = objectMapper.readValue(jsonStr, new TypeReference<TpmsPesVO[]>() {});
				return voList;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return null;
	}

	@Override
	public HashMap<String, Object> insertPesData(MultipartFile excelFile, String table_columns,
												String table_types, String excel_columns, String excel_types) {
		// TODO Auto-generated method stub
		HashMap<String, Object> res = new HashMap<>();

		String[] fieldNames = excel_columns.split(",");
		String[] fieldTypes = excel_types.split(",");

		/** excel file parsing */
		TpmsPesVO[] pesList = makePesExcelParsing(excelFile, fieldNames, fieldTypes);
		
		if(pesList.length<1) {
			res.put("res", 0);
			res.put("msg", "파일 데이터 파싱 실패");
			res.put("filename", excelFile.getOriginalFilename());
			return res;
		}

		
		
		double extension = 0.0;
		double t_rutting = 0.0;
		double t_iri = 0.0;
		TpmsRoadSurveyMasterVO rsmVO = null;
		int version = 0;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String str_date = null;
		Date date = null;
		
		/** tpms road survey master data insert */
		String target_id = pesList[0].getTarget_id();
		
		int existsCnt = tpmsRoadSurveyMasterMapper.selectExistsTargetIdCntPes(target_id);
		if(existsCnt>0) tpmsRoadSurveyMasterMapper.deleteEquip1Data(target_id);
		existsCnt = tpmsRoadSurveyMasterMapper.selectExistsTargetIdCnt(target_id);
		if(existsCnt>0) tpmsRoadSurveyMasterMapper.deleteSurveyMaster(target_id);
		
		int existsFileLogCnt = 0;
		TpmsFileLogVO fileLogVO = null;
		for(TpmsPesVO vo : pesList) {
			/** make file log - status : wait */
			fileLogVO = new TpmsFileLogVO("0207", vo.getFront_img(), "wait", target_id, "");
			existsFileLogCnt = tpmsRoadSurveyMasterMapper.selectExistsFileLogCheck(fileLogVO);
			if(existsFileLogCnt<1) tpmsRoadSurveyMasterMapper.insertFileLogData(fileLogVO);
			fileLogVO = new TpmsFileLogVO("0207", vo.getPave_img(), "wait", target_id, "");
			existsFileLogCnt = tpmsRoadSurveyMasterMapper.selectExistsFileLogCheck(fileLogVO);
			if(existsFileLogCnt<1) tpmsRoadSurveyMasterMapper.insertFileLogData(fileLogVO);
			/** make file log - status : wait */
			extension = vo.getExtension();
			t_rutting += vo.getRutting();
			t_iri += vo.getAvg_iri();

		}
		// s_ts_id, e_ts_id, target_id, rutt, iri, t_rutting, t_iri, extension, date, version, un_use, -- t_crack, index, is_detail
		rsmVO = new TpmsRoadSurveyMasterVO();
		rsmVO.setRutt(false);
		rsmVO.setIri(false);
		if(t_rutting>0) rsmVO.setRutt(true);
		if(t_iri>0) rsmVO.setIri(true);
		rsmVO.setS_ts_id(tpmsRoadSurveyMasterMapper.getRoadSurveyMasterStartTsId(target_id));
		rsmVO.setE_ts_id(tpmsRoadSurveyMasterMapper.getRoadSurveyMasterEndTsId(target_id));
		rsmVO.setTarget_id(target_id);
		rsmVO.setT_rutting(t_rutting);
		rsmVO.setT_iri(t_iri);
		rsmVO.setExtension(extension);
		rsmVO.setYear(pesList[0].getYear()); 
		rsmVO.setMonth(pesList[0].getMonth());
		rsmVO.setDay(pesList[0].getDay());
		
		
//		int smCnt = tpmsRoadSurveyMasterMapper.selectExistsTargetIdCnt(target_id);
//		if(smCnt == 0) {
//			version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterNewVersionCheck();
//			// 버전 생성
//			if(version==0) {
//				tpmsRoadSurveyMasterMapper.insertRoadSurveyMasterNewVersion();
//				version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterMaxVersion();
//			}
//		}else {
//			version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterMaxVersion();
//			tpmsRoadSurveyMasterMapper.insertRoadSurveyMasterUpdateVersion(version+1);
//			version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterMaxVersion();
//		}
//		
//		rsmVO.setVersion(version);
//		rsmVO.setUn_use(false);
		str_date = pesList[0].getYear() + "-" + pesList[0].getMonth() + "-" + pesList[0].getDay();
		try {
			date = format.parse(str_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		str_date = format.format(date);
		rsmVO.setDate(str_date);
		int result = tpmsRoadSurveyMasterMapper.insertRoadSurvey1DataToMaster(rsmVO);
		if(result==0) {
			res.put("res", 0);
			res.put("msg", "DB 입력 실패[Master 입력 실패]");
			res.put("filename", excelFile.getOriginalFilename());
			return res;
		}
		t_rutting = 0.0;
		t_iri = 0.0;
		/** tpms road survey master data insert */
		
		/* PES table insert */
		int pesInx = 0;
		int inx = 0;
		long[] t10mIdList = tpmsRoadSurveyMasterMapper.select10mIdListForPesData(target_id);
		int t10mIdListLen = t10mIdList.length;
		int pesListLen = pesList.length;
		

		
//		int pesCnt = tpmsRoadSurveyMasterMapper.selectExistsTargetIdCntPes(target_id);
//		if(pesCnt == 0) {
//			version = tpmsRoadSurveyMasterMapper.selectPesDataNewVersionCheck();
//			if(version==0) {
//				tpmsRoadSurveyMasterMapper.insertPesNewVersion();
//				version = tpmsRoadSurveyMasterMapper.selectPesDataNewVersionCheck();
//			}
//		}else {
//			tpmsRoadSurveyMasterMapper.updateUnUsePesData(target_id);
//			version = tpmsRoadSurveyMasterMapper.selectPesDataMaxVersionCheck();
//			tpmsRoadSurveyMasterMapper.insertPesDataUpdateVersion(version+1);
//			version = tpmsRoadSurveyMasterMapper.selectPesDataMaxVersionCheck();
//		}
		for(long t10mId : t10mIdList) {
			pesInx = (int)(Math.round(((double)pesListLen/(double)t10mIdListLen)*((double)(inx+1)))) -1;
			
			pesList[pesInx].setT10m_id(t10mId);
//			pesList[pesInx].setVersion(version);
//			pesList[pesInx].setUn_use(false);
			pesList[pesInx].setUpload_state("wait");
			result = tpmsRoadSurveyMasterMapper.insertPesData(pesList[pesInx]);
			if(result==0) {
				res.put("res", 0);
				res.put("msg", "DB 입력 실패[PES 데이터 입력 실패]");
				res.put("filename", excelFile.getOriginalFilename());
				return res;
			}
			++inx;
		}
			
		/* PES table insert */
		
		res.put("res", 1);
//		res.put("saveFile", saveFileName);
		return res;
	}
	
	/*@Override
	public HashMap<String, Object> insertPesData(MultipartFile excelFile, String table_columns,
												String table_types, String excel_columns, String excel_types) {
		// TODO Auto-generated method stub
		HashMap<String, Object> res = new HashMap<>();
		
//		Map<String, Object> imgFilePathMap = (Map<String, Object>) readIniFilePath;
//		String frontImgPath = (String)imgFilePathMap.get("roadPesFrontImg");
//		String paveImgPath = (String)imgFilePathMap.get("roadPesPaveImg");
		
		String[] fieldNames = excel_columns.split(",");
		String[] fieldTypes = excel_types.split(",");

		*//** excel file parsing *//*
		TpmsPesVO[] pesList = makePesExcelParsing(excelFile, fieldNames, fieldTypes);
		
		if(pesList.length<1) {
			res.put("res", 0);
			res.put("msg", "파일 데이터 파싱 실패");
			res.put("filename", excelFile.getOriginalFilename());
			return res;
		}
		
//		boolean isSaveFrontImg = false;
//		boolean isSavePaveImg = false;
//		MultipartFile savedFrontImg = null;
//		MultipartFile savedPaveImg = null;
		
		double extension = 0.0;
		double t_rutting = 0.0;
		double t_iri = 0.0;
		TpmsRoadSurveyMasterVO rsmVO = null;
		int version = 0;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String str_date = null;
		Date date = null;
		
		*//** tpms road survey master data insert *//*
		String target_id = pesList[0].getTarget_id();
		int existsFileLogCnt = 0;
		TpmsFileLogVO fileLogVO = null;
		for(TpmsPesVO vo : pesList) {
			*//** make file log - status : wait *//*
			fileLogVO = new TpmsFileLogVO("0207", vo.getFront_img(), "wait", target_id, "");
			existsFileLogCnt = tpmsRoadSurveyMasterMapper.selectExistsFileLogCheck(fileLogVO);
			if(existsFileLogCnt<1) tpmsRoadSurveyMasterMapper.insertFileLogData(fileLogVO);
			fileLogVO = new TpmsFileLogVO("0207", vo.getPave_img(), "wait", target_id, "");
			existsFileLogCnt = tpmsRoadSurveyMasterMapper.selectExistsFileLogCheck(fileLogVO);
			if(existsFileLogCnt<1) tpmsRoadSurveyMasterMapper.insertFileLogData(fileLogVO);
			*//** make file log - status : wait *//*
			extension = vo.getExtension();
			t_rutting += vo.getRutting();
			t_iri += vo.getAvg_iri();

		}
		// s_ts_id, e_ts_id, target_id, rutt, iri, t_rutting, t_iri, extension, date, version, un_use, -- t_crack, index, is_detail
		rsmVO = new TpmsRoadSurveyMasterVO();
		rsmVO.setRutt(false);
		rsmVO.setIri(false);
		if(t_rutting>0) rsmVO.setRutt(true);
		if(t_iri>0) rsmVO.setIri(true);
		rsmVO.setS_ts_id(tpmsRoadSurveyMasterMapper.getRoadSurveyMasterStartTsId(target_id));
		rsmVO.setE_ts_id(tpmsRoadSurveyMasterMapper.getRoadSurveyMasterEndTsId(target_id));
		rsmVO.setTarget_id(target_id);
		rsmVO.setT_rutting(t_rutting);
		rsmVO.setT_iri(t_iri);
		rsmVO.setExtension(extension);
		rsmVO.setYear(pesList[0].getYear()); 
		rsmVO.setMonth(pesList[0].getMonth());
		rsmVO.setDay(pesList[0].getDay());
		
		
		int smCnt = tpmsRoadSurveyMasterMapper.selectExistsTargetIdCnt(target_id);
		if(smCnt == 0) {
			version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterNewVersionCheck();
			// 버전 생성
			if(version==0) {
				tpmsRoadSurveyMasterMapper.insertRoadSurveyMasterNewVersion();
				version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterMaxVersion();
			}
		}else {
			version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterMaxVersion();
			tpmsRoadSurveyMasterMapper.insertRoadSurveyMasterUpdateVersion(version+1);
			version = tpmsRoadSurveyMasterMapper.selectRoadSurveyMasterMaxVersion();
		}
		
		rsmVO.setVersion(version);
		rsmVO.setUn_use(false);
		str_date = pesList[0].getYear() + "-" + pesList[0].getMonth() + "-" + pesList[0].getDay();
		try {
			date = format.parse(str_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		str_date = format.format(date);
		rsmVO.setDate(str_date);
		int result = tpmsRoadSurveyMasterMapper.insertRoadSurvey1DataToMaster(rsmVO);
		if(result==0) {
			res.put("res", 0);
			res.put("msg", "DB 입력 실패[Master 입력 실패]");
			res.put("filename", excelFile.getOriginalFilename());
			return res;
		}
		t_rutting = 0.0;
		t_iri = 0.0;
		*//** tpms road survey master data insert *//*
		
		 PES table insert 
		int pesInx = 0;
		int inx = 0;
		long[] t10mIdList = tpmsRoadSurveyMasterMapper.select10mIdListForPesData(target_id);
		int t10mIdListLen = t10mIdList.length;
		int pesListLen = pesList.length;
		
		*//**  image files copy to file server *//*
//		ArrayList<String> saveFileName = new ArrayList<>();
//		String saveFrontImgPath = frontImgPath + File.separator + target_id;
//		String savePaveImgPath = paveImgPath + File.separator + target_id;
		*//**  image files copy to file server *//*
		
		
		int pesCnt = tpmsRoadSurveyMasterMapper.selectExistsTargetIdCntPes(target_id);
		if(pesCnt == 0) {
			version = tpmsRoadSurveyMasterMapper.selectPesDataNewVersionCheck();
			if(version==0) {
				tpmsRoadSurveyMasterMapper.insertPesNewVersion();
				version = tpmsRoadSurveyMasterMapper.selectPesDataNewVersionCheck();
			}
		}else {
			tpmsRoadSurveyMasterMapper.updateUnUsePesData(target_id);
			version = tpmsRoadSurveyMasterMapper.selectPesDataMaxVersionCheck();
			tpmsRoadSurveyMasterMapper.insertPesDataUpdateVersion(version+1);
			version = tpmsRoadSurveyMasterMapper.selectPesDataMaxVersionCheck();
		}
		for(long t10mId : t10mIdList) {
			pesInx = (int)(Math.round(((double)pesListLen/(double)t10mIdListLen)*((double)(inx+1)))) -1;
			
			*//**  image files copy to file server *//*
//			for(MultipartFile imgFile : imgFiles) {
//				
//				if(imgFile.getOriginalFilename().equals(pesList[pesInx].getFront_img())){
//					fileUploadService.restore(imgFile, saveFrontImgPath + File.separator + version);
//					saveFileName.add(imgFile.getOriginalFilename());
////						imgFiles.remove(imgFile);
//					savedFrontImg = imgFile;
//					isSaveFrontImg = true;
//				}
//				if(imgFile.getOriginalFilename().equals(pesList[pesInx].getPave_img())){
//					fileUploadService.restore(imgFile, savePaveImgPath + File.separator + version);
//					saveFileName.add(imgFile.getOriginalFilename());
////						imgFiles.remove(imgFile);
//					savedPaveImg = imgFile;
//					isSavePaveImg = true;
//				}
//				if(isSaveFrontImg && isSavePaveImg) break;
//			}
//			imgFiles.remove(savedFrontImg);
//			imgFiles.remove(savedPaveImg);
//			isSaveFrontImg = false;
//			isSavePaveImg = false;
			*//**  image files copy to file server *//*
			
			pesList[pesInx].setT10m_id(t10mId);
			pesList[pesInx].setVersion(version);
			pesList[pesInx].setUn_use(false);
			pesList[pesInx].setUpload_state("wait");
			result = tpmsRoadSurveyMasterMapper.insertPesData(pesList[pesInx]);
			if(result==0) {
				res.put("res", 0);
				res.put("msg", "DB 입력 실패[PES 데이터 입력 실패]");
				res.put("filename", excelFile.getOriginalFilename());
				return res;
			}
			++inx;
		}
			
		 PES table insert 
		
		res.put("res", 1);
//		res.put("saveFile", saveFileName);
		return res;
	}*/

	@Override
	public int buildFileData(MultipartFile file, Object iniFilePath) {
		// TODO Auto-generated method stub
		Map<String, Object> imgFilePathMap = (Map<String, Object>) iniFilePath;
		String pesImgPath = (String)imgFilePathMap.get("roadSurvey1Img");
		String tempPath = (String)imgFilePathMap.get("tempDirectory");
		
		String filename = file.getOriginalFilename();
		String[] f_split = filename.split("_");
		String target_id = f_split[0];
		String type = null; 
		if(f_split[1].substring(0, 1).equals("V")) type = "front";
		else type = "pave";
		
		TpmsFileLogVO logVO = new TpmsFileLogVO("0207", filename, "wait", target_id, "");
		int cnt = tpmsRoadSurveyMasterMapper.selectExistsFileLogCheck(logVO);
		int res = 0;
		String movePath = null;
		if(cnt>0) { // 입력되어 있는 대기중 상태의 로그가 있음.
			// temp directory로 이미지 복사
			movePath = tempPath + File.separator + target_id + File.separator;
			if(type.equals("front")) {
				movePath += "front_img";
			}else {
				movePath += "pave_img";
			}
			fileUploadService.restore(file, movePath);
			
			res = tpmsRoadSurveyMasterMapper.updateFileLog(logVO); // 대기 상태 로그 -> ok 상태로 수정
			if(res>0) { // 수정 ok
				cnt = tpmsRoadSurveyMasterMapper.selectWaitLogCntCheck(logVO); // target id에 해당하는 이미지들 중 아직 대기 상태의 로그가 있는지 확인
				if(cnt > 0 ) { // 대기 중인 상태의 이미지들이 있음.
					return 1;
				}else {
//					File folder_front =  new File(pesImgPath + File.separator + target_id + File.separator + "front_img");
//					if(folder_front.exists()) folder_front.delete();
//					File folder_pave =  new File(pesImgPath + File.separator + target_id + File.separator + "pave_img");
//					if(folder_pave.exists()) folder_pave.delete();
					fileUploadService.moveFile(tempPath + File.separator + target_id, pesImgPath + File.separator + target_id);
					tpmsRoadSurveyMasterMapper.updateImgUploadState(target_id);
					tpmsRoadSurveyMasterMapper.insertConvertStatus(target_id);
					return 1;
				}
			}else {
				return 0;
			}
			
		}else {
			return 0; // 입력되어 있는 대기중 상태의 로그가 없음. error not found matching filename
		}
				
				
	}

	
public TpmsCrackVO[] makeCrackExcelParsing(MultipartFile file, String[] fieldNames, String[] fieldTypes) {
		
		if(file.getSize()>0) {
			String path = fileUploadService.restore(file); 
			
			if(!(path.toLowerCase().endsWith(".xlsx")||path.toLowerCase().endsWith(".csv"))) {
				return null;
			}
			
			FileInputStream fs;
			ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
			XSSFWorkbook workbook = null;
			try {
				fs = new FileInputStream(new File(path));
				OPCPackage opcPackage = OPCPackage.open(fs);
				workbook = new XSSFWorkbook(opcPackage);
				opcPackage.close();
			} catch (InvalidFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			
			XSSFSheet sheet = workbook.getSheetAt(0);
			// sheet row
			Iterator<Row> rowIterator = sheet.iterator();
			// 한 행 저장
			HashMap<String, Object> oneRow = null;
			Row row;
			int rowCnt = 0;
			String str_date = null;
			Date date = null;
			String value = "";
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			while(rowIterator.hasNext()) {
				row = rowIterator.next();
				if(++rowCnt < 3) continue;
				
				Iterator<Cell> cellIterator = row.cellIterator();
				oneRow = new HashMap<>();
				while(cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					if(cell.getColumnIndex()==0&&cell.getCellType()==Cell.CELL_TYPE_BLANK) break;
					if(cell.getColumnIndex()>fieldTypes.length-1) break;
					
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						value = "";
						if(fieldNames[cell.getColumnIndex()].equals("year") ||
								fieldNames[cell.getColumnIndex()].equals("month") ||
								fieldNames[cell.getColumnIndex()].equals("day")) {
							value = cell.getStringCellValue();
							if(value.contains(".")) {
								value = value.split("\\.")[0];
								oneRow.put(fieldNames[cell.getColumnIndex()], value);
							}
							
						}else {
							oneRow.put(fieldNames[cell.getColumnIndex()], cell.getStringCellValue());
						}
						
						break;
					case Cell.CELL_TYPE_NUMERIC:
						value = "";
						if(fieldTypes[cell.getColumnIndex()].equals("string")) {
							value = String.valueOf(Math.floor(cell.getNumericCellValue()));
							if(value.contains(".")) {
								value = value.split("\\.")[0];
								oneRow.put(fieldNames[cell.getColumnIndex()], value);
							}
						}else if(fieldTypes[cell.getColumnIndex()].equals("int")) {
							oneRow.put(fieldNames[cell.getColumnIndex()], Integer.parseInt(String.valueOf(Math.round(cell.getNumericCellValue()))));
						}else {
							oneRow.put(fieldNames[cell.getColumnIndex()], cell.getNumericCellValue());
						}
						
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						oneRow.put(fieldNames[cell.getColumnIndex()], cell.getBooleanCellValue());
						break;
					case Cell.CELL_TYPE_BLANK:
						if(fieldTypes[cell.getColumnIndex()].equals("int")) {
							oneRow.put(fieldNames[cell.getColumnIndex()], 0);
						}else {
							oneRow.put(fieldNames[cell.getColumnIndex()], null);
						}
						break;
					default:
						break;
					}
					if(fieldTypes.length-1 == cell.getColumnIndex()) break;
				}
				if(oneRow.size()>0&&oneRow.get(fieldNames[0])!=null) {
					if(oneRow.containsKey(fieldNames[0])) if(oneRow.get(fieldNames[0]).equals("합계") || oneRow.get(fieldNames[0]).equals("평균")) continue;
					str_date = oneRow.get("year")+"-"+oneRow.get("month")+"-"+oneRow.get("day");
					try {
						date = format.parse(str_date);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					oneRow.put("date", format.format(date));
					rowList.add(oneRow);
				}
				else break;
			}
			
			try {
				fs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;
			TpmsCrackVO[] voList = null;
			try {
				jsonStr = objectMapper.writeValueAsString(rowList);
				voList = objectMapper.readValue(jsonStr, new TypeReference<TpmsCrackVO[]>() {});
				return voList;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return null;
	}

	@Override
	public HashMap<String, Object> insertCrackData(MultipartFile file, String columns, String types, String indexFormula) {
		// TODO Auto-generated method stub
		HashMap<String, Object> res = new HashMap<>();
		String[] fieldNames = columns.split(",");
		String[] fieldTypes = types.split(",");
		System.out.println(fieldNames);
		System.out.println(fieldTypes);
//		/** excel file parsing */
		TpmsCrackVO[] CrackList = makeCrackExcelParsing(file, fieldNames, fieldTypes);
		
		HashMap<String, Object> detailTarget = null;
		HashMap<String, ArrayList<TpmsCrackVO>> group = new HashMap<>();
		ArrayList<TpmsCrackVO> groupList = null;
		for (TpmsCrackVO tpmsCrackVO : CrackList) {
			groupList = new ArrayList<>();
			if(group.containsKey(tpmsCrackVO.getDetail_target_id())) {
				groupList = group.get(tpmsCrackVO.getDetail_target_id());
			}
			if(tpmsCrackVO.getDetail_target_id()!=null) {
				detailTarget = tpmsRoadSurveyMasterMapper.selectDetailTargetRow(tpmsCrackVO.getDetail_target_id());
				tpmsCrackVO.setS_t10m_id((Long)detailTarget.get("s_t10m_id"));
				tpmsCrackVO.setE_t10m_id((Long)detailTarget.get("e_t10m_id"));
			}
			groupList.add(tpmsCrackVO);
			group.put(tpmsCrackVO.getDetail_target_id(), groupList);
		}
		
		HashMap<String, ArrayList> List = new HashMap<>();
		group.forEach((key, list)->{
			if(key==null) return;
			ArrayList<Tpms10mVO> t10mList = tpmsRoadSurveyMasterMapper.select10mListForCrack(list.get(0));
			int t10mCnt = t10mList.size(); // 10m 단위 링크 개수
			int listCnt = list.size(); //실 데이터 개수
			int crackInx = 0;
			int inx = 0;
			tpmsRoadSurveyMasterMapper.deleteCrackData(key);
			tpmsRoadSurveyMasterMapper.updateIsCrack(key);
			TpmsCrackVO crackVO = null;
			for (Tpms10mVO tpms10mVO : t10mList) {
				crackInx = (int)(Math.round(((double)listCnt/(double)t10mCnt)*((double)(inx+1)))) -1;
				if(crackInx<0) crackInx = 0;
				crackVO = list.get(crackInx);
				crackVO.setT10m_id(tpms10mVO.getId());
				
				/* index 구하기 */
				TpmsPesVO pes = tpmsRoadSurveyMasterMapper.selectRdIri(crackVO);
				if(pes!=null) {
					double spi = getSpi(crackVO.getCr(), pes.getRutting(), pes.getAvg_iri());
					crackVO.setSpi(spi);
					double nhpci = getNhpci(crackVO.getCr(), pes.getRutting(), pes.getAvg_iri());
					crackVO.setNhpci(nhpci);
					double mpci = getMpci(crackVO.getCr(), pes.getRutting(), pes.getAvg_iri());
					crackVO.setMpci(mpci);
				}
				/* index 구하기 */
				
				int crackRes = tpmsRoadSurveyMasterMapper.insertCrackData(crackVO);
				
				if(crackRes == 0) {
					res.put(key, 0); break;
				}
				
				++inx;
			}
			tpmsRoadSurveyMasterMapper.updateTcrack(crackVO);
		});

		return res;
	}
	
	public double getSpi(double cr, double rutt, double _iri) {
		System.out.println("균열율 : "+cr+", 소성변형 : "+rutt+", 평탄성 : "+_iri);
		double crack = 10-2.23*(Math.pow(cr, 0.3));
		double rutting = 10-0.2*rutt;
		double iri = 10-0.667*_iri;
		
		// 포장파손지수
		double pdi = Math.pow((Math.pow((10.0-crack), 5.0) + Math.pow((10.0-rutting), 5.0) + Math.pow((10.0-iri), 5.0)), 0.2) ;
		double spi = 10-pdi;
		
		System.out.println("균열율지수 : "+cr+", 소성변형지수 : "+rutt+", 평탄성지수 : "+_iri+", pdi : "+pdi+", spi : "+spi);
		if(spi<0) spi = 0;
		if(spi>10) spi = 10;
		
		return spi;
	}
	
	public double getNhpci(double cr, double rutt, double iri) {
		double nhpci = 1.0/ Math.pow((0.33+0.003*cr+0.004*rutt+0.0183*iri), 2); 
		
		if(nhpci<0) nhpci = 0;
		if(nhpci>10) nhpci = 10;
		
		return nhpci;
	}
	
	public double getMpci(double cr, double rutt, double iri) {
		
		double pcicr = 10.0-1.67*Math.pow(cr, 0.47);
		double pcirutt = 10.0-0.40*Math.pow(rutt, 0.85);
		double pciiri = 10.0-0.87*iri;
		
		double mpci = 10-Math.pow((Math.pow((10.0-pcicr), 5)+Math.pow((10.0-pcirutt), 5)+Math.pow((10.0-pciiri), 5)), 0.2);
				
		if(mpci<0) mpci = 0;
		if(mpci>10) mpci = 10;
		
		return mpci;
	}
	
	public TpmsGprVO[] makeGprExcelParsing(MultipartFile file, String[] fieldNames, String[] fieldTypes) {
			
			if(file.getSize()>0) {
				String path = fileUploadService.restore(file); 
				
				if(!(path.toLowerCase().endsWith(".xlsx")||path.toLowerCase().endsWith(".csv"))) {
					return null;
				}
				
				FileInputStream fs;
				ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
				XSSFWorkbook workbook = null;
				try {
					fs = new FileInputStream(new File(path));
					OPCPackage opcPackage = OPCPackage.open(fs);
					workbook = new XSSFWorkbook(opcPackage);
					opcPackage.close();
				} catch (InvalidFormatException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
				
				XSSFSheet sheet = workbook.getSheetAt(0);
				// sheet row
				Iterator<Row> rowIterator = sheet.iterator();
				// 한 행 저장
				HashMap<String, Object> oneRow = null;
				Row row;
				int rowCnt = 0;
				String str_date = null;
				Date date = null;
				String value = "";
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				while(rowIterator.hasNext()) {
					row = rowIterator.next();
					if(++rowCnt < 3) continue;
					
					Iterator<Cell> cellIterator = row.cellIterator();
					oneRow = new HashMap<>();
					while(cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						if(cell.getColumnIndex()==0&&cell.getCellType()==Cell.CELL_TYPE_BLANK) break;
						if(cell.getColumnIndex()>fieldTypes.length-1) break;
						
						switch (cell.getCellType()) {
						case Cell.CELL_TYPE_STRING:
							value = "";
							if(fieldNames[cell.getColumnIndex()].equals("year") ||
									fieldNames[cell.getColumnIndex()].equals("month") ||
									fieldNames[cell.getColumnIndex()].equals("day")) {
								value = cell.getStringCellValue();
								if(value.contains(".")) {
									value = value.split("\\.")[0];
									oneRow.put(fieldNames[cell.getColumnIndex()], value);
								}
								
							}else {
								oneRow.put(fieldNames[cell.getColumnIndex()], cell.getStringCellValue());
							}
							
							break;
						case Cell.CELL_TYPE_NUMERIC:
							value = "";
							if(fieldTypes[cell.getColumnIndex()].equals("string")) {
								value = String.valueOf(Math.floor(cell.getNumericCellValue()));
								if(value.contains(".")) {
									value = value.split("\\.")[0];
									oneRow.put(fieldNames[cell.getColumnIndex()], value);
								}
							}else if(fieldTypes[cell.getColumnIndex()].equals("int")) {
								oneRow.put(fieldNames[cell.getColumnIndex()], Integer.parseInt(String.valueOf(Math.round(cell.getNumericCellValue()))));
							}else {
								oneRow.put(fieldNames[cell.getColumnIndex()], cell.getNumericCellValue());
							}
							
							break;
						case Cell.CELL_TYPE_BOOLEAN:
							oneRow.put(fieldNames[cell.getColumnIndex()], cell.getBooleanCellValue());
							break;
						case Cell.CELL_TYPE_BLANK:
							if(fieldTypes[cell.getColumnIndex()].equals("int")) {
								oneRow.put(fieldNames[cell.getColumnIndex()], 0);
							}else {
								oneRow.put(fieldNames[cell.getColumnIndex()], null);
							}
							break;
						default:
							break;
						}
						if(fieldTypes.length-1 == cell.getColumnIndex()) break;
					}
					if(oneRow.size()>0&&oneRow.get(fieldNames[0])!=null) {
						if(oneRow.containsKey(fieldNames[0])) if(oneRow.get(fieldNames[0]).equals("합계") || oneRow.get(fieldNames[0]).equals("평균")) continue;
						str_date = oneRow.get("year")+"-"+oneRow.get("month")+"-"+oneRow.get("day");
						try {
							date = format.parse(str_date);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						oneRow.put("date", format.format(date));
						rowList.add(oneRow);
					}
					else break;
				}
				
				try {
					fs.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonStr;
				TpmsGprVO[] voList = null;
				try {
					jsonStr = objectMapper.writeValueAsString(rowList);
					voList = objectMapper.readValue(jsonStr, new TypeReference<TpmsGprVO[]>() {});
					return voList;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} 
			
			return null;
	}

	@Override
	public HashMap<String, Object> insertGprData(MultipartFile file, String columns, String types) {
		// TODO Auto-generated method stub
		HashMap<String, Object> res = new HashMap<>();
		String[] fieldNames = columns.split(",");
		String[] fieldTypes = types.split(",");
		System.out.println(fieldNames);
		System.out.println(fieldTypes);
		TpmsGprVO[] GprList = makeGprExcelParsing(file, fieldNames, fieldTypes);
//		System.out.println(GprList);
		
		tpmsRoadSurveyMasterMapper.deleteGprData(GprList[0].getDetail_target_id());
		tpmsRoadSurveyMasterMapper.updateIsGpr(GprList[0].getDetail_target_id());
		for (TpmsGprVO tpmsGprVO : GprList) {
			
			System.out.println(tpmsGprVO);
			tpmsRoadSurveyMasterMapper.insertGprData(tpmsGprVO);
		}
		return res;
	}
	
	public TpmsHwdVO[] makeHwdExcelParsing(MultipartFile file, String[] fieldNames, String[] fieldTypes) {
		
		if(file.getSize()>0) {
			String path = fileUploadService.restore(file); 
			
			if(!(path.toLowerCase().endsWith(".xlsx")||path.toLowerCase().endsWith(".csv"))) {
				return null;
			}
			
			FileInputStream fs;
			ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
			XSSFWorkbook workbook = null;
			try {
				fs = new FileInputStream(new File(path));
				OPCPackage opcPackage = OPCPackage.open(fs);
				workbook = new XSSFWorkbook(opcPackage);
				opcPackage.close();
			} catch (InvalidFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			
			XSSFSheet sheet = workbook.getSheetAt(0);
			// sheet row
			Iterator<Row> rowIterator = sheet.iterator();
			// 한 행 저장
			HashMap<String, Object> oneRow = null;
			Row row;
			int rowCnt = 0;
			String str_date = null;
			Date date = null;
			String value = "";
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			while(rowIterator.hasNext()) {
				row = rowIterator.next();
				if(++rowCnt < 3) continue;
				
				Iterator<Cell> cellIterator = row.cellIterator();
				oneRow = new HashMap<>();
				while(cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					if(cell.getColumnIndex()==0&&cell.getCellType()==Cell.CELL_TYPE_BLANK) break;
					if(cell.getColumnIndex()>fieldTypes.length-1) break;
					
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						value = "";
						if(fieldNames[cell.getColumnIndex()].equals("year") ||
								fieldNames[cell.getColumnIndex()].equals("month") ||
								fieldNames[cell.getColumnIndex()].equals("day")) {
							value = cell.getStringCellValue();
							if(value.contains(".")) {
								value = value.split("\\.")[0];
								oneRow.put(fieldNames[cell.getColumnIndex()], value);
							}
							
						}else {
							oneRow.put(fieldNames[cell.getColumnIndex()], cell.getStringCellValue());
						}
						
						break;
					case Cell.CELL_TYPE_NUMERIC:
						value = "";
						if(fieldTypes[cell.getColumnIndex()].equals("string")) {
							value = String.valueOf(Math.floor(cell.getNumericCellValue()));
							if(value.contains(".")) {
								value = value.split("\\.")[0];
								oneRow.put(fieldNames[cell.getColumnIndex()], value);
							}
						}else if(fieldTypes[cell.getColumnIndex()].equals("int")) {
							oneRow.put(fieldNames[cell.getColumnIndex()], Integer.parseInt(String.valueOf(Math.round(cell.getNumericCellValue()))));
						}else {
							oneRow.put(fieldNames[cell.getColumnIndex()], cell.getNumericCellValue());
						}
						
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						oneRow.put(fieldNames[cell.getColumnIndex()], cell.getBooleanCellValue());
						break;
					case Cell.CELL_TYPE_BLANK:
						if(fieldTypes[cell.getColumnIndex()].equals("int")) {
							oneRow.put(fieldNames[cell.getColumnIndex()], 0);
						}else {
							oneRow.put(fieldNames[cell.getColumnIndex()], null);
						}
						break;
					default:
						break;
					}
					if(fieldTypes.length-1 == cell.getColumnIndex()) break;
				}
				if(oneRow.size()>0&&oneRow.get(fieldNames[0])!=null) {
					if(oneRow.containsKey(fieldNames[0])) if(oneRow.get(fieldNames[0]).equals("합계") || oneRow.get(fieldNames[0]).equals("평균")) continue;
					str_date = oneRow.get("year")+"-"+oneRow.get("month")+"-"+oneRow.get("day");
					try {
						date = format.parse(str_date);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					oneRow.put("date", format.format(date));
					rowList.add(oneRow);
				}
				else break;
			}
			
			try {
				fs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonStr;
			TpmsHwdVO[] voList = null;
			try {
				jsonStr = objectMapper.writeValueAsString(rowList);
				voList = objectMapper.readValue(jsonStr, new TypeReference<TpmsHwdVO[]>() {});
				return voList;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return null;
	}
	
	@Override
	public HashMap<String, Object> insertHwdData(MultipartFile file, String columns, String types) {
		// TODO Auto-generated method stub	HashMap<String, Object> res = new HashMap<>();
		HashMap<String, Object> res = new HashMap<>();
		String[] fieldNames = columns.split(",");
		String[] fieldTypes = types.split(",");
		System.out.println(fieldNames);
		System.out.println(fieldTypes);
		TpmsHwdVO[] HwdList = makeHwdExcelParsing(file, fieldNames, fieldTypes);
		tpmsRoadSurveyMasterMapper.deleteHwdData(HwdList[0].getDetail_target_id());
		tpmsRoadSurveyMasterMapper.updateIsHwd(HwdList[0].getDetail_target_id());
		for (TpmsHwdVO tpmsHwdVO : HwdList) {
			System.out.println(tpmsHwdVO);
			tpmsRoadSurveyMasterMapper.insertHwdData(tpmsHwdVO);
		}
		
		return res;
	}

	@Override
	public HashMap<String, Object> selectRoadSectionInfo(Long id, String resourceUrl, String formula) {
		// TODO Auto-generated method stub
		HashMap<String, Object> equip1Map = tpmsRoadSurveyMasterMapper.select10MEquip1Data(id);
		HashMap<String, Object> crackMap = tpmsRoadSurveyMasterMapper.select10MCrackData(id);
		HashMap<String, Object> repairMap = tpmsRoadSurveyMasterMapper.select10MRepairData(id);
		
		
		String frontImgPath = null;
		String paveImgPath = null;
		if(equip1Map.containsKey("target_id")) {
			frontImgPath = resourceUrl + equip1Map.get("target_id").toString() + "/front_img/" + equip1Map.get("front_img").toString();
			paveImgPath = resourceUrl + equip1Map.get("target_id").toString() + "/pave_img/" + equip1Map.get("pave_img").toString();
		}
		
		equip1Map.put("detail_target_id", crackMap.get("detail_target_id"));
		equip1Map.put("line_cr", crackMap.get("line_cr"));
		equip1Map.put("fetching", crackMap.get("fetching"));
		equip1Map.put("turtle_lamp_cr", crackMap.get("turtle_lamp_cr"));
		equip1Map.put("pothole", crackMap.get("pothole"));
		equip1Map.put("cr", crackMap.get("cr"));
		equip1Map.put("frontImgPath", frontImgPath);
		equip1Map.put("paveImgPath", paveImgPath);
		equip1Map.put("dir", ((int)equip1Map.get("dir")==0)?"상행":"하행");
		switch (formula) {
		case "spi":
			equip1Map.put("index", crackMap.get("spi"));
			break;
		case "mpci":
			equip1Map.put("index", crackMap.get("mpci"));
			break;
		case "nhpci":
			equip1Map.put("index", crackMap.get("nhpci"));
			break;
		}
		
		equip1Map.put("repair_target_id", repairMap.get("repair_target_id"));
		equip1Map.put("pave_type", repairMap.get("pave_type"));
		equip1Map.put("method_type", repairMap.get("method_type"));
		equip1Map.put("method", repairMap.get("method"));
		equip1Map.put("pave_meterial", repairMap.get("pave_meterial"));
		equip1Map.put("split_thin", repairMap.get("split_thin"));
		equip1Map.put("layer_reinforce", repairMap.get("layer_reinforce"));
		equip1Map.put("surface_thin", repairMap.get("surface_thin"));
		equip1Map.put("comment", repairMap.get("comment"));
		equip1Map.put("price", repairMap.get("price"));
		equip1Map.put("date", repairMap.get("date"));
		equip1Map.put("sigongsa", repairMap.get("sigongsa"));
		equip1Map.put("sigong_pm", repairMap.get("sigong_pm"));
		equip1Map.put("gamrisa", repairMap.get("gamrisa"));
		equip1Map.put("gamrisa_pm", repairMap.get("gamrisa_pm"));
		equip1Map.put("gamriwon", repairMap.get("gamriwon"));
		equip1Map.put("gamriwon_pm", repairMap.get("gamriwon_pm"));
		
		
		return equip1Map;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectRoadSectionList(HashMap<String, Object> param, String resourceUrl, String formula) {
		HashMap<String, Object> result = new HashMap<>();
		// TODO Auto-generated method stub
		
		param.put("tri_id", Long.parseLong(param.get("tri_id")+""));
		param.put("main_num", Integer.parseInt(param.get("main_num")+""));
		param.put("sub_num", Integer.parseInt(param.get("sub_num")+""));
		param.put("dir", Integer.parseInt(param.get("dir")+""));
		ArrayList<HashMap<String, Object>> equip1AIList = tpmsRoadSurveyMasterMapper.select10MEquip1List(param);
		ArrayList<HashMap<String, Object>> crackList = tpmsRoadSurveyMasterMapper.select10MCrackList(param);
		ArrayList<HashMap<String, Object>> repairList = tpmsRoadSurveyMasterMapper.select10MRepairList(param);
		
		ArrayList<HashMap<String, Object>> resultList = new ArrayList<>();
		
		String frontImgPath = null;
		String paveImgPath = null;
		String frontVideo = null;
		String paveVideo = null;
		
		int index = 0;
		String targetId = "";
		int seq = 0;
		for (HashMap<String, Object> hashMap : equip1AIList) {
			
			hashMap.put("dir", ((int)hashMap.get("dir")==0)?"상행":"하행");
			
			if(hashMap.containsKey("target_id")) {
				if(!targetId.equals(hashMap.get("target_id")+"")) {
					targetId = hashMap.get("target_id")+"";
					seq = 0;
				}
				frontImgPath = resourceUrl + hashMap.get("target_id").toString() + "/front_img/" + hashMap.get("front_img").toString();
				paveImgPath = resourceUrl + hashMap.get("target_id").toString() + "/pave_img/" + hashMap.get("pave_img").toString();
				frontVideo = resourceUrl + hashMap.get("target_id").toString() + "/movie/front_img.mp4";
				paveVideo = resourceUrl + hashMap.get("target_id").toString() + "/movie/pave_img.mp4";
				hashMap.put("frontImgPath", frontImgPath);
				hashMap.put("paveImgPath", paveImgPath);
				hashMap.put("frontVideo", frontVideo);
				hashMap.put("paveVideo", paveVideo);
				hashMap.put("seq", seq);
				++seq;
			}
			
			switch (formula) {
			case "spi":
				hashMap.put("index", hashMap.get("spi"));
				hashMap.put("ai_index", hashMap.get("ai_spi"));
				break;
			case "mpci":
				hashMap.put("index", hashMap.get("mpci"));
				hashMap.put("ai_index", hashMap.get("ai_mpci"));
				break;
			case "nhpci":
				hashMap.put("index", hashMap.get("nhpci"));
				hashMap.put("ai_index", hashMap.get("ai_nhpci"));
				break;
			}
			
			hashMap.put("repair_target_id", repairList.get(index).get("repair_target_id"));
			hashMap.put("pave_type", repairList.get(index).get("pave_type"));
			hashMap.put("method_type", repairList.get(index).get("method_type"));
			hashMap.put("method", repairList.get(index).get("method"));
			hashMap.put("pave_meterial", repairList.get(index).get("pave_meterial"));
			hashMap.put("split_thin", repairList.get(index).get("split_thin"));
			hashMap.put("layer_reinforce", repairList.get(index).get("layer_reinforce"));
			hashMap.put("surface_thin", repairList.get(index).get("surface_thin"));
			hashMap.put("comment", repairList.get(index).get("comment"));
			hashMap.put("price", repairList.get(index).get("price"));
			hashMap.put("date", repairList.get(index).get("date"));
			hashMap.put("sigongsa", repairList.get(index).get("sigongsa"));
			hashMap.put("sigong_pm", repairList.get(index).get("sigong_pm"));
			hashMap.put("gamrisa", repairList.get(index).get("gamrisa"));
			hashMap.put("gamrisa_pm", repairList.get(index).get("gamrisa_pm"));
			hashMap.put("gamriwon", repairList.get(index).get("gamriwon"));
			hashMap.put("gamriwon_pm", repairList.get(index).get("gamriwon_pm"));
			
			resultList.add(hashMap);
			++index;
		}
		
//		result.put("equip1List", equip1List);
//		result.put("crackList", crackList);
//		result.put("repairList", repairList);
		
		return resultList;
	}
	
	@Override
	public HashMap<String, Object> selectRoadSectionLineExt(HashMap<String, Object> param) {
		// TODO Auto-generated method stub
		param.put("tri_id", Long.parseLong(param.get("tri_id")+""));
		param.put("main_num", Integer.parseInt(param.get("main_num")+""));
		param.put("sub_num", Integer.parseInt(param.get("sub_num")+""));
		param.put("dir", Integer.parseInt(param.get("dir")+""));
		ArrayList<HashMap<String, Object>> tsExtList = tpmsRoadSurveyMasterMapper.selectSectionTsExtList(param);
		ArrayList<HashMap<String, Object>> t10mExtList = tpmsRoadSurveyMasterMapper.selectSectionTs10mExtList(Long.parseLong(param.get("ts_id")+""));
		
		param.put("t10m_id", Long.parseLong(param.get("t10m_id")+""));
		HashMap<String, Object> prevMap = tpmsRoadSurveyMasterMapper.selectPrevLinkData(param);
		HashMap<String, Object> nextMap = tpmsRoadSurveyMasterMapper.selectNextLinkData(param);
		
		HashMap<String, Object> res = new HashMap<>();
		res.put("tsList", tsExtList);
		res.put("t10mList", t10mExtList);
		res.put("prev", prevMap);
		res.put("next", nextMap);
		
		return res;
	}
	 
	
}
