package egovframework.com.tpms.rsm.service;

public class TpmsHwdVO {
	private long t10m_id;
	private String target_id;
	private String detail_target_id;
	private int num;
	private int extension;
	private int count;
	private double weight;
	private double d0;
	private double air_temper;
	private double pave_temper;
	private double terper_correct;
	private String date;
	
	private int year;
	private int month;
	private int day;
	
	public long getT10m_id() {
		return t10m_id;
	}
	public void setT10m_id(long t10m_id) {
		this.t10m_id = t10m_id;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getDetail_target_id() {
		return detail_target_id;
	}
	public void setDetail_target_id(String detail_target_id) {
		this.detail_target_id = detail_target_id;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getExtension() {
		return extension;
	}
	public void setExtension(int extension) {
		this.extension = extension;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getD0() {
		return d0;
	}
	public void setD0(double d0) {
		this.d0 = d0;
	}
	public double getAir_temper() {
		return air_temper;
	}
	public void setAir_temper(double air_temper) {
		this.air_temper = air_temper;
	}
	public double getPave_temper() {
		return pave_temper;
	}
	public void setPave_temper(double pave_temper) {
		this.pave_temper = pave_temper;
	}
	public double getTerper_correct() {
		return terper_correct;
	}
	public void setTerper_correct(double terper_correct) {
		this.terper_correct = terper_correct;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
}
