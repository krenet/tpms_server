package egovframework.com.tpms.rsm.service;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TpmsRoadSurveyMasterVO {
	private long id;
	private long s_ts_id;
	private long e_ts_id;
	private String target_id;
	private boolean rutt;
	private boolean iri;
	private double t_rutting;
	private double t_iri;
	private double extension;
	private String date;
	private int version;
	private boolean un_use;
	private double t_crack;
	private double index;
	private boolean is_detail;
	
	private int year;
	private int month;
	private int day;
	
	
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getS_ts_id() {
		return s_ts_id;
	}
	public void setS_ts_id(long s_ts_id) {
		this.s_ts_id = s_ts_id;
	}
	public long getE_ts_id() {
		return e_ts_id;
	}
	public void setE_ts_id(long e_ts_id) {
		this.e_ts_id = e_ts_id;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public boolean isRutt() {
		return rutt;
	}
	public void setRutt(boolean rutt) {
		this.rutt = rutt;
	}
	public boolean isIri() {
		return iri;
	}
	public void setIri(boolean iri) {
		this.iri = iri;
	}
	public double getT_rutting() {
		return t_rutting;
	}
	public void setT_rutting(double t_rutting) {
		this.t_rutting = t_rutting;
	}
	public double getT_iri() {
		return t_iri;
	}
	public void setT_iri(double t_iri) {
		this.t_iri = t_iri;
	}
	public double getExtension() {
		return extension;
	}
	public void setExtension(double extension) {
		this.extension = extension;
	}
	public String getDate() {
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
////		String date = format.format(new Date(this.year, this.month, this.day));
//		return format.format(new Date(this.year, this.month, this.day));
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public boolean isUn_use() {
		return un_use;
	}
	public void setUn_use(boolean un_use) {
		this.un_use = un_use;
	}
	public double getT_crack() {
		return t_crack;
	}
	public void setT_crack(double t_crack) {
		this.t_crack = t_crack;
	}
	public double getIndex() {
		return index;
	}
	public void setIndex(double index) {
		this.index = index;
	}
	public boolean isIs_detail() {
		return is_detail;
	}
	public void setIs_detail(boolean is_detail) {
		this.is_detail = is_detail;
	}
	
	
}
