package egovframework.com.tpms.rsm.service;

public class TpmsCrackVO {

	private long t10m_id;
	private String target_id;
	private String detail_target_id;
	private int extension;
	private String filename;
	private double line_cr;
	private double fetching;
	private double turtle_lamp_cr;
	private double pothole;
	private double etc;
	private double cr;
	private String date;
	private double spi;
	private double nhpci;
	private double hpci;
	private double mpci;
	
	private int year;
	private int month;
	private int day;
	
	// for t 10m id matching
	private long s_t10m_id;
	private long e_t10m_id;
	
	
	public double getSpi() {
		return spi;
	}
	public void setSpi(double spi) {
		this.spi = spi;
	}
	public double getNhpci() {
		return nhpci;
	}
	public void setNhpci(double nhpci) {
		this.nhpci = nhpci;
	}
	public double getHpci() {
		return hpci;
	}
	public void setHpci(double hpci) {
		this.hpci = hpci;
	}
	public double getMpci() {
		return mpci;
	}
	public void setMpci(double mpci) {
		this.mpci = mpci;
	}
	public long getS_t10m_id() {
		return s_t10m_id;
	}
	public void setS_t10m_id(long s_t10m_id) {
		this.s_t10m_id = s_t10m_id;
	}
	public long getE_t10m_id() {
		return e_t10m_id;
	}
	public void setE_t10m_id(long e_t10m_id) {
		this.e_t10m_id = e_t10m_id;
	}
	public long getT10m_id() {
		return t10m_id;
	}
	public void setT10m_id(long t10m_id) {
		this.t10m_id = t10m_id;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public int getExtension() {
		return extension;
	}
	public void setExtension(int extension) {
		this.extension = extension;
	}
	public String getDetail_target_id() {
		return detail_target_id;
	}
	public void setDetail_target_id(String detail_target_id) {
		this.detail_target_id = detail_target_id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public double getLine_cr() {
		return line_cr;
	}
	public void setLine_cr(double line_cr) {
		this.line_cr = line_cr;
	}
	public double getFetching() {
		return fetching;
	}
	public void setFetching(double fetching) {
		this.fetching = fetching;
	}
	public double getTurtle_lamp_cr() {
		return turtle_lamp_cr;
	}
	public void setTurtle_lamp_cr(double turtle_lamp_cr) {
		this.turtle_lamp_cr = turtle_lamp_cr;
	}
	public double getPothole() {
		return pothole;
	}
	public void setPothole(double pothole) {
		this.pothole = pothole;
	}
	public double getEtc() {
		return etc;
	}
	public void setEtc(double etc) {
		this.etc = etc;
	}
	public double getCr() {
		return cr;
	}
	public void setCr(double cr) {
		this.cr = cr;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	
}
