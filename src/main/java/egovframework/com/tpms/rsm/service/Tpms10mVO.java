package egovframework.com.tpms.rsm.service;

public class Tpms10mVO {

	private long id;
	private long tsm_id;
	private long tri_id;
	private int dir;
	private int main_num;
	private int sub_num;
	private int order;
	private long ts_id;
	private int t10m_order;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTsm_id() {
		return tsm_id;
	}
	public void setTsm_id(long tsm_id) {
		this.tsm_id = tsm_id;
	}
	public long getTri_id() {
		return tri_id;
	}
	public void setTri_id(long tri_id) {
		this.tri_id = tri_id;
	}
	public int getDir() {
		return dir;
	}
	public void setDir(int dir) {
		this.dir = dir;
	}
	public int getMain_num() {
		return main_num;
	}
	public void setMain_num(int main_num) {
		this.main_num = main_num;
	}
	public int getSub_num() {
		return sub_num;
	}
	public void setSub_num(int sub_num) {
		this.sub_num = sub_num;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public long getTs_id() {
		return ts_id;
	}
	public void setTs_id(long ts_id) {
		this.ts_id = ts_id;
	}
	public int getT10m_order() {
		return t10m_order;
	}
	public void setT10m_order(int t10m_order) {
		this.t10m_order = t10m_order;
	}
	
	
}
