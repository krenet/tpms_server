package egovframework.com.tpms.rsm.service.impl;

import java.util.List;
import java.util.Map;

import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterVO;
import egovframework.com.tpms.rsm.service.TpmsDetailTargetVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper ("tpmsDetailTargetMapper")
public interface TpmsDetailTargetMapper {

//	public List<Map<String, Object>> searchDetail(TpmsRoadMasterVO vo);
	public List<Map<String, Object>> getYearList();
	public List<Map<String, Object>> getAllDetail();
	public List<Map<String, Object>> getCurrYearDetail(int year);
	public List<Map<String, Object>> getAllMaster();
	public List<Map<String, Object>> getCurrYearEquip(int year);
	public List<Map<String, Object>> searchMaster(TpmsRoadMasterVO vo);
	public List<Map<String, Object>> searchEquip(TpmsRoadMasterVO vo);
	public Map<String, Object> getNode_OrderNEQ1(Map<String, Integer> map);
	public Map<String, Object> getNode_OrderEQ1(Map<String, Integer> map);
	public String getStartExt(Map<String, Long> map);
	public String getEndExt(Map<String, Long> map);
	public int get_s_t10m(int ts_id);
	public int get_e_t10m(int ts_id);
	public int delCurrYearDetail(int year);
	public int insertDetail(TpmsDetailTargetVO vo);
	public List<Map<String, Object>> searchDetail(TpmsRoadMasterVO vo);
	public List<Map<String, Object>> getSelectedTS_ID(String detail_ids);
	// 2021-02-24 박혜원 추가 
	public List<Long> selectTargetTsList(Map<String, Integer> map);
	// 2021-02-24 박혜원 추가 
}