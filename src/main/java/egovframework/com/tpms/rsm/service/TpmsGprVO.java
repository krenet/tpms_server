package egovframework.com.tpms.rsm.service;

public class TpmsGprVO {

	private long t10m_id;
	private String target_id;
	private String detail_target_id;
	private int group_no;
	private double ascon_thin;
	private String date;
	
	private int year;
	private int month;
	private int day;
	
	// for t 10m id matching
//	private long s_t10m_id;
//	private long e_t10m_id;
//	
	public long getT10m_id() {
		return t10m_id;
	}
	public void setT10m_id(long t10m_id) {
		this.t10m_id = t10m_id;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getDetail_target_id() {
		return detail_target_id;
	}
	public void setDetail_target_id(String detail_target_id) {
		this.detail_target_id = detail_target_id;
	}
	public int getGroup_no() {
		return group_no;
	}
	public void setGroup_no(int group_no) {
		this.group_no = group_no;
	}
	public double getAscon_thin() {
		return ascon_thin;
	}
	public void setAscon_thin(double ascon_thin) {
		this.ascon_thin = ascon_thin;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
//	public long getS_t10m_id() {
//		return s_t10m_id;
//	}
//	public void setS_t10m_id(long s_t10m_id) {
//		this.s_t10m_id = s_t10m_id;
//	}
//	public long getE_t10m_id() {
//		return e_t10m_id;
//	}
//	public void setE_t10m_id(long e_t10m_id) {
//		this.e_t10m_id = e_t10m_id;
//	}
}
