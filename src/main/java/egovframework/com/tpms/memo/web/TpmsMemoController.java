package egovframework.com.tpms.memo.web;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.cmm.service.TpmsCmmService;
import egovframework.com.tpms.memo.service.TpmsMemoService;

@RestController
public class TpmsMemoController {
	
	@Resource(name="tpmsMemoService")
	private TpmsMemoService tpmsMemoService;
	
	@Resource(name="tpmsCmmService")
	private TpmsCmmService tpmsCmmService;
	
	@RequestMapping(value="/tpms/insertMemoWithoutImg.do", method=RequestMethod.POST)
	public ModelAndView insertMemoWithoutImg(@RequestParam HashMap<String, Object> param){
	
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("res", tpmsMemoService.insertMemoWithoutImg(param));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/insertMemo.do", method=RequestMethod.POST)
	public ModelAndView insertMemo(@RequestParam("filePath") MultipartFile file, @RequestParam HashMap<String, Object> map) {
		Map<String, Object> param = new HashMap<>();
		param.put("section", "directorys");
		System.out.println("map: "+map);
		System.out.println("file: "+file);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		int result = tpmsMemoService.insertMemo(map, file, tpmsCmmService.readIniFile(param));

		mav.addObject("res", result);		
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getAllMemo.do", method=RequestMethod.POST)
	public ModelAndView getAllMemo(){
	
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsMemoService.getAllMemo());		
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/deleteMemo.do", method=RequestMethod.POST)
	public ModelAndView deleteMemo(@RequestParam HashMap<String, Object> map){
	
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		Map<String, Object> param = new HashMap<>();
		param.put("section", "directorys");
		int result = tpmsMemoService.deleteMemo(map, tpmsCmmService.readIniFile(param));
		mav.addObject("res", result);		
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getMemoImg.do", method=RequestMethod.POST)
	public ModelAndView getMemoImg(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		Map<String, Object> param = new HashMap<>();
		param.put("section", "resource");
		Map<String, Object> imgFilePathMap = (Map<String, Object>) tpmsCmmService.readIniFile(param);
		String memoImgPath = (String)imgFilePathMap.get("memo");
		memoImgPath = memoImgPath + map.get("id") + "/"+ map.get("fileName");
		System.out.println("memoImgPath: "+memoImgPath);
		mav.addObject("res", memoImgPath);			
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/modifyMemo.do", method=RequestMethod.POST)
	public ModelAndView modifyMemo(@RequestParam("filePath") MultipartFile file, @RequestParam HashMap<String, Object> map) {
		Map<String, Object> param = new HashMap<>();
		param.put("section", "directorys");
		System.out.println("map: "+map);
		System.out.println("file: "+file);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		int result = tpmsMemoService.modifyMemo(map, file, tpmsCmmService.readIniFile(param));

		mav.addObject("res", result);		
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/modifyMemoWithoutImg.do", method=RequestMethod.POST)
	public ModelAndView modifyMemoWithoutImg(@RequestParam HashMap<String, Object> param){
	
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("res", tpmsMemoService.modifyMemoWithoutImg(param));
		
		return mav;
	}

}
