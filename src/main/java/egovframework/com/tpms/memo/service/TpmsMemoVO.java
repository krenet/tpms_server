package egovframework.com.tpms.memo.service;

public class TpmsMemoVO {
	
	private long id;
	private String category;
	private String title;
	private String content;
	private int alerm_yn;
	private String date;
	private String time;
	private String file;
	private double coord_X;
	private double coord_Y;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getAlerm_yn() {
		return alerm_yn;
	}
	public void setAlerm_yn(int alerm_yn) {
		this.alerm_yn = alerm_yn;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public double getCoord_X() {
		return coord_X;
	}
	public void setCoord_X(double coord_X) {
		this.coord_X = coord_X;
	}
	public double getCoord_Y() {
		return coord_Y;
	}
	public void setCoord_Y(double coord_Y) {
		this.coord_Y = coord_Y;
	}
	
}
