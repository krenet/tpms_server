package egovframework.com.tpms.memo.service.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import egovframework.com.tpms.cmm.service.FileUploadService;
import egovframework.com.tpms.memo.service.TpmsMemoService;
import egovframework.com.tpms.memo.service.TpmsMemoVO;


@Service("tpmsMemoService")
@Transactional
public class TpmsMemoImpl implements TpmsMemoService {
	
	@Resource(name="tpmsMemoMapper")
	private TpmsMemoMapper tpmsMemoMapper;
	
	@Autowired
	FileUploadService fileUploadService;

	

	@Override
	public int insertMemoWithoutImg(Map<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper();
		TpmsMemoVO vo = mapper.convertValue(map, TpmsMemoVO.class);
		
		return tpmsMemoMapper.insertMemoWithoutImg(vo);
	}
	
	
	@Override
	public int insertMemo(Map<String, Object> map, MultipartFile file, Object iniFilePath) {
		
		ObjectMapper mapper = new ObjectMapper();
		TpmsMemoVO vo = mapper.convertValue(map, TpmsMemoVO.class);
		tpmsMemoMapper.insertMemo(vo);

		Map<String, Object> imgFilePathMap = (Map<String, Object>) iniFilePath;
		String memoImgPath = (String)imgFilePathMap.get("memo");
		long memoId = vo.getId();
		if(memoId>0) {
			String movePath = memoImgPath + File.separator + memoId;
			fileUploadService.restore(file, movePath);
			return 1;
		}else {
			return 0;
		}
	}


	@Override
	public List<Map<String, Object>> getAllMemo() {
		
		return tpmsMemoMapper.getAllMemo();
	}


	@Override
	public int deleteMemo(Map<String, Object> map, Object iniFilePath) {
		ObjectMapper mapper = new ObjectMapper();
		TpmsMemoVO vo = mapper.convertValue(map, TpmsMemoVO.class);
		Map<String, Object> memoData = tpmsMemoMapper.getFileName(vo);
		int id = Integer.parseInt(String.valueOf(map.get("id")));
		if(memoData.containsKey("file")) {
			String fileName = (String) memoData.get("file");
			Map<String, Object> imgFilePathMap = (Map<String, Object>) iniFilePath;
			String memoImgPath = (String)imgFilePathMap.get("memo");
			File file = new File(memoImgPath + File.separator + id + File.separator + fileName);
			if(file.exists()) {
				file.delete();
				File folder = new File(memoImgPath + File.separator + id);
				if(folder.exists()) folder.delete();
				
			}
		}
		
		int result = tpmsMemoMapper.deleteMemo(id);
		
		return result;
	}


	@Override
	public int modifyMemo(Map<String, Object> map, MultipartFile file, Object iniFilePath) {
		ObjectMapper mapper = new ObjectMapper();
		TpmsMemoVO vo = mapper.convertValue(map, TpmsMemoVO.class);
		Map<String, Object> memoData = tpmsMemoMapper.getFileName(vo);
		String origin_fileName = "";
		if(memoData.containsKey("file")) {
			origin_fileName = (String) memoData.get("file");
		}
		System.out.println("fileName: "+origin_fileName);
		int modify_result = tpmsMemoMapper.modifyMemo(vo);

		Map<String, Object> imgFilePathMap = (Map<String, Object>) iniFilePath;
		String memoImgPath = (String)imgFilePathMap.get("memo");
		long memoId = vo.getId();
		if(memoId>0) {
			String movePath = memoImgPath + File.separator + memoId;
			if(! origin_fileName.equals("")) {
				File orignFile = new File(movePath + File.separator + origin_fileName);
				if(orignFile.exists()) {
					orignFile.delete();
				}
			}
			fileUploadService.restore(file, movePath);
			return 1;
		}else {
			return 0;
		}
	}


	@Override
	public int modifyMemoWithoutImg(Map<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper();
		TpmsMemoVO vo = mapper.convertValue(map, TpmsMemoVO.class);
		
		return tpmsMemoMapper.modifyMemoWithoutImg(vo);
	}


//	@Override
//	public int saveFileData(MultipartFile file, Object iniFilePath) {
//		
//		Map<String, Object> imgFilePathMap = (Map<String, Object>) iniFilePath;
//		String memoImgPath = (String)imgFilePathMap.get("memo");
//		String filename = file.getOriginalFilename();
//		System.out.println("filename: "+filename);
//		String movePath = memoImgPath + File.separator + filename;
//		fileUploadService.restore(file, movePath);
//		
//		System.out.println("저장?: "+fileUploadService.restore(file, movePath));
//		
//		return 0;
//	}

	
}
