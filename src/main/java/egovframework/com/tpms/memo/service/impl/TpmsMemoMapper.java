package egovframework.com.tpms.memo.service.impl;

import java.util.List;
import java.util.Map;

import egovframework.com.tpms.memo.service.TpmsMemoVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper ("tpmsMemoMapper")
public interface TpmsMemoMapper {
	
	public int insertMemoWithoutImg(TpmsMemoVO vo);
	public int insertMemo(TpmsMemoVO vo);
	public List<Map<String, Object>> getAllMemo();
	public int deleteMemo(int id);
	public Map<String, Object> getFileName(TpmsMemoVO vo);
	public int modifyMemo(TpmsMemoVO vo);
	public int modifyMemoWithoutImg(TpmsMemoVO vo);
}
