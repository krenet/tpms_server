package egovframework.com.tpms.memo.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

public interface TpmsMemoService {

	public int insertMemoWithoutImg(Map<String, Object> map);
	public int insertMemo(Map<String, Object> map, MultipartFile file, Object iniFilePath);
	public List<Map<String, Object>> getAllMemo();
	public int deleteMemo(Map<String, Object> map, Object iniFilePath);
	public int modifyMemo(Map<String, Object> map, MultipartFile file, Object iniFilePath);
	public int modifyMemoWithoutImg(Map<String, Object> map);
}
