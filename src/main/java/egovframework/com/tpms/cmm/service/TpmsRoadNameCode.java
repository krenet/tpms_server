package egovframework.com.tpms.cmm.service;

public class TpmsRoadNameCode {
	
	private String code;
	private String road_name;
	private String sido_name;
	private String sgg_name;
	private String emd_name;
	private String emd_type;
	private String emd_code;
	private String target_code;
	private long id;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRoad_name() {
		return road_name;
	}
	public void setRoad_name(String road_name) {
		this.road_name = road_name;
	}
	public String getSido_name() {
		return sido_name;
	}
	public void setSido_name(String sido_name) {
		this.sido_name = sido_name;
	}
	public String getSgg_name() {
		return sgg_name;
	}
	public void setSgg_name(String sgg_name) {
		this.sgg_name = sgg_name;
	}
	public String getEmd_name() {
		return emd_name;
	}
	public void setEmd_name(String emd_name) {
		this.emd_name = emd_name;
	}
	public String getEmd_type() {
		return emd_type;
	}
	public void setEmd_type(String emd_type) {
		this.emd_type = emd_type;
	}
	public String getEmd_code() {
		return emd_code;
	}
	public void setEmd_code(String emd_code) {
		this.emd_code = emd_code;
	}
	public String getTarget_code() {
		return target_code;
	}
	public void setTarget_code(String target_code) {
		this.target_code = target_code;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
}
