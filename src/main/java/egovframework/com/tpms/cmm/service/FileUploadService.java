package egovframework.com.tpms.cmm.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileUploadService {
	// 리눅스 기준으로 파일 경로를 작성 ( 루트 경로인 /으로 시작한다. )
	// 윈도우라면 workspace의 드라이브를 파악하여 JVM이 알아서 처리해준다.
	// 따라서 workspace가 C드라이브에 있다면 C드라이브에 upload 폴더를 생성해 놓아야 한다.
	private String save_path = "F:/upload/";
	private static final String PREFIX_URL = "F:/upload/";
	  
	public String getSave_path() {
		return save_path;
	}


	public void setSave_path(String save_path) {
		this.save_path = save_path;
	}


	public String restore(MultipartFile multipartFile) {
	    String url = null;
		     
	    try {
	      // 파일 정보
	      String originFilename = multipartFile.getOriginalFilename();
	      String extName = originFilename.substring(originFilename.lastIndexOf("."), originFilename.length());
	      Long size = multipartFile.getSize();
	       
	      // 서버에서 저장 할 파일 이름
//		      String saveFileName = genSaveFileName(extName);
	      String saveFileName = originFilename;
	      
	      System.out.println("originFilename : " + originFilename);
	      System.out.println("extensionName : " + extName);
	      System.out.println("size : " + size);
	      System.out.println("saveFileName : " + saveFileName);
	       
	      String property = "java.io.tmpdir";
	      String tempDir = System.getProperty(property);
	      setSave_path(tempDir);
	      writeFile(multipartFile, saveFileName);
	      
	      //url = tempDir + saveFileName;
	      url = FilenameUtils.normalize(tempDir+"/"+saveFileName);
	    }
	    catch (IOException e) {
	      // 원래라면 RuntimeException 을 상속받은 예외가 처리되어야 하지만
	      // 편의상 RuntimeException을 던진다.
	      // throw new FileUploadException();
	      throw new RuntimeException(e);
	    }
	    return url;
	}

	public String restore(MultipartFile multipartFile, String savePath) {
	    String url = null;
	     
	    try {
	      // 파일 정보
	      String originFilename = multipartFile.getOriginalFilename();
	      String extName
	        = originFilename.substring(originFilename.lastIndexOf("."), originFilename.length());
	      Long size = multipartFile.getSize();
	       
	      // 서버에서 저장 할 파일 이름
//		      String saveFileName = genSaveFileName(extName);
	      String saveFileName = originFilename;
	      
	      System.out.println("originFilename : " + originFilename);
	      System.out.println("extensionName : " + extName);
	      System.out.println("size : " + size);
	      System.out.println("saveFileName : " + saveFileName);
	       
	      File dir = new File(savePath);

	      if(!dir.exists()){
	    	  dir.mkdirs();
	      }
	      setSave_path(savePath);
	      writeFile(multipartFile, saveFileName);
	      url = savePath + "\\" +saveFileName;
	    }
	    catch (IOException e) {
	      // 원래라면 RuntimeException 을 상속받은 예외가 처리되어야 하지만
	      // 편의상 RuntimeException을 던진다.
	      // throw new FileUploadException();
	      throw new RuntimeException(e);
	    }
	    return url;
	}
	   
	// 현재 시간을 기준으로 파일 이름 생성
	private String genSaveFileName(String extName) {
		String fileName = "";
     
	    Calendar calendar = Calendar.getInstance();
	    fileName += calendar.get(Calendar.YEAR);
	    fileName += calendar.get(Calendar.MONTH);
	    fileName += calendar.get(Calendar.DATE);
	    fileName += calendar.get(Calendar.HOUR);
	    fileName += calendar.get(Calendar.MINUTE);
	    fileName += calendar.get(Calendar.SECOND);
	    fileName += calendar.get(Calendar.MILLISECOND);
	    fileName += extName;
     
	    return fileName;
	}
	   
	   
	// 파일을 실제로 write 하는 메서드
	private boolean writeFile(MultipartFile multipartFile, String saveFileName) throws IOException{
		boolean result = false;
	 
	    byte[] data = multipartFile.getBytes();
	    FileOutputStream fos = new FileOutputStream(save_path + "/" + saveFileName);
	    fos.write(data);
	    fos.close();
     
	    return result;
	}
	  
	public void moveFile(String source, String target){
		File dir = new File(target);
		if(!dir.exists()){
			dir.mkdirs();
		}else {
			deleteFolder(target);
			dir.mkdirs();
		}
//		File file = new File(source);
		try {
			Files.move(Paths.get(source), Paths.get(target), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	  
	public void fileCopy(File sourceF, File targetF){
			
		File[] ff = sourceF.listFiles();
		for (File file : ff) {
			File temp = new File(targetF.getAbsolutePath() + File.separator + file.getName());
			if(file.isDirectory()){
				temp.mkdir();
				fileCopy(file, temp);
			} else {
				FileInputStream fis = null;
				FileOutputStream fos = null;
				try {
					fis = new FileInputStream(file);
					fos = new FileOutputStream(temp) ;
					byte[] b = new byte[4096];
					int cnt = 0;
					while((cnt=fis.read(b)) != -1){
						fos.write(b, 0, cnt);
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally{
					try {
						fis.close();
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
			}
		}
			
	}
	
	public static void deleteFolder(String path) {
		
	    File folder = new File(path);
	    try {
		if(folder.exists()){
                File[] folder_list = folder.listFiles(); //파일리스트 얻어오기
				
		for (int i = 0; i < folder_list.length; i++) {
		    if(folder_list[i].isFile()) {
			folder_list[i].delete();
//			System.out.println("파일이 삭제되었습니다.");
		    }else {
			deleteFolder(folder_list[i].getPath()); //재귀함수호출
//			System.out.println("폴더가 삭제되었습니다.");
		    }
		    folder_list[i].delete();
		 }
		 folder.delete(); //폴더 삭제
	       }
	   } catch (Exception e) {
		e.getStackTrace();
	   }
    }
}
