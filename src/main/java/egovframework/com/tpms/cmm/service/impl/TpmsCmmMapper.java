package egovframework.com.tpms.cmm.service.impl;

import java.util.ArrayList;
import java.util.Map;

import egovframework.com.tpms.cmm.service.TpmsCodeVO;
import egovframework.com.tpms.cmm.service.TpmsConvertStatusVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsCmmMapper")
public interface TpmsCmmMapper {

	//<select id="selectTpmsCode" resultType="tpmsCodeVO">
	public ArrayList<TpmsCodeVO> selectTpmsCode();
	//<select id="selectTargetIdTest" resultType="tpmsTempTargetVO">
	public ArrayList<Map<String, Object>> selectTargetIdTest();
	//<select id="selectRoadNameCode" resultType="tpmsRoadNameCode" parameterType="String">
	public String selectRoadNameCode(String name);
	//<select id="selectConvertCnt" parameterType="String" resultType="int">
	public int selectConvertCnt(String user_id); 
	//<select id="selectUserIdConvert" parameterType="tpmsConvertStatusVO" resultType="String">
	public String selectUserIdConvert(TpmsConvertStatusVO vo);
	//<select id="selectConvertData" parameterType="String" resultType="tpmsConvertStatusVO">
	public ArrayList<TpmsConvertStatusVO> selectConvertData(String user_id); 
}
