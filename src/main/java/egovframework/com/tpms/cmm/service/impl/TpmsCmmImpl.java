package egovframework.com.tpms.cmm.service.impl;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.ini4j.Config;
import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;
import org.springframework.stereotype.Service;

import egovframework.com.tpms.cmm.service.TpmsCmmService;
import egovframework.com.tpms.cmm.service.TpmsCodeVO;
import egovframework.com.tpms.cmm.service.TpmsConvertStatusVO;
import egovframework.com.tpms.roadinfo.service.TpmsRoadInfoVO;
import egovframework.com.tpms.roadinfo.service.impl.TpmsRoadInfoMapper;
import egovframework.com.tpms.station.service.TpmsRoadStationMasterVO;
import egovframework.com.tpms.station.service.impl.TpmsRoadStationMasterMapper;

@Service("tpmsCmmService")
@Transactional
public class TpmsCmmImpl implements TpmsCmmService{

	@Resource(name="tpmsCmmMapper")
	private TpmsCmmMapper tpmsCmmMapper;

	@Resource(name="tpmsRoadInfoMapper")
	private TpmsRoadInfoMapper tpmsRoadInfoMapper;
	
	@Resource(name="tpmsRoadStationMasterMapper")
	private TpmsRoadStationMasterMapper tpmsRoadStationMasterMapper;
	
	
	@Override
	public ArrayList<TpmsCodeVO> selectTpmsCode() {
		return tpmsCmmMapper.selectTpmsCode();
	}

	
	/**
	 * sectionList - Map contains key ["target_id", "group_num", "tri_id", "tsm_id", "dir", "year"] 필수
	 * String type - 조사대상구간 : "T", 상세조사구간 : "D", 보수대상구간 : "R"
	 */
	@Override
	public ArrayList<Map<String, Object>> getTargetId(String type, List<Map<String, Object>> sectionList) {
		// TODO Auto-generated method stub
//		ArrayList<Map<String, Object>> getList = tpmsCmmMapper.selectTargetIdTest();
		List<Map<String, Object>> getList = sectionList;
		//grouping
		HashMap<String, ArrayList<Map<String, Object>>> group = new HashMap<>();
		
		ArrayList<Map<String, Object>> tempGroup = new ArrayList<>();
		
		String stCode = "";
		for (Map<String, Object> forMap : getList) {
			if(type.equals("T")){
				if(forMap.containsKey("target_id")) continue;
			}
			
			stCode = String.valueOf(forMap.get("group_num"));
			if(group.containsKey(stCode)) {
				tempGroup = group.get(stCode);
				tempGroup.add(forMap);
			}else {
				tempGroup = new ArrayList<>();
				tempGroup.add(forMap);
			}
			group.put(stCode, tempGroup);
		}
		
		ArrayList<Map<String, Object>> targetGroup = new ArrayList<>();
		
		if(type.equals("D")) {
			
			HashMap<String, Object> targetMap = new HashMap<>();
			
			group.forEach((key, value)->{
				ArrayList<Map<String, Object>> groupList = group.get(key);
				Map<String, Object> groupMap = groupList.get(0);
				
				String year = String.valueOf(groupMap.get("year"));
				year = year.substring(year.length()-2, year.length()); /** year */
				
				TpmsRoadInfoVO infoVO = tpmsRoadInfoMapper.selectRoadInfoDataById(Long.parseLong(String.valueOf(groupMap.get("tri_id"))));
				String rank = infoVO.getRank();						
				rank = rank.substring(rank.length()-1, rank.length()); /** rank */
				
				String in_code = ""; /** in_code */
				String no = infoVO.getNo();
				if(no.equals("-")) { // 노선 번호가 없을 경우, 도로명 코드 사용

					in_code = tpmsCmmMapper.selectRoadNameCode(infoVO.getName());
					
					if(in_code==null) { // 노선 번호가 없고, 도로명 코드도 없을 경우, tri_id 사용 
						in_code += "B";
						int triIdLen = String.valueOf(infoVO.getId()).length();
						for(int i=1;i<=6-triIdLen;++i) {
							in_code+="0";
						}
						in_code+=String.valueOf(infoVO.getId());
					}
					
				}else { // 노선 번호가 있을 경우, 맨 앞의 문자 'A' 고정, 나머지 문자는 노선번호로 사용
					in_code += "A";
					int noLen = no.length();
					for(int i=1;i<=6-noLen;++i) {
						in_code+="0";
					}
					in_code+=no;
				}
				
				String dir = String.valueOf(groupMap.get("dir")); /** dir */
				if(dir.equals("0")) {
					dir = "U";
				}else {
					dir = "D";
				}
				
				String lane = "1"; /** lane */
				
				/** sector, section */
				long tsm_id = 999999999;
				for(Map<String, Object> obj : groupList) {
					long obj_tsm_id = Long.parseLong(String.valueOf(obj.get("tsm_id")));
					if(tsm_id>obj_tsm_id){
						tsm_id = obj_tsm_id;
					}
				}
				TpmsRoadStationMasterVO tsmVO = tpmsRoadStationMasterMapper.selectStationMasterById(tsm_id);
				String sector = String.valueOf(tsmVO.getSector());
				if(sector.length()<2) sector = "0" + sector;
				String section = String.valueOf(tsmVO.getSection());
				if(section.length()<2) section = "0" + section;
				
				String seqNum = "";
				String tempInCode = type+year+rank+in_code+dir+lane+sector+section;
				if(targetMap.containsKey(tempInCode)) {
					int cnt = Integer.parseInt(targetMap.get(tempInCode)+"")+1;
					targetMap.put(tempInCode, cnt);
					seqNum = "-"+cnt;
				}else {
					targetMap.put(tempInCode, 1);
					seqNum = "-1";
				}
				
				// make result value
				for(Map<String, Object> obj : groupList) {
					obj.put("in_code", in_code);
					obj.put("detail_target_id", tempInCode + seqNum);
					
					targetGroup.add(obj);
				}
			});
		}else {
			group.forEach((key, value)->{
				ArrayList<Map<String, Object>> groupList = group.get(key);
				Map<String, Object> groupMap = groupList.get(0);
				
				String year = String.valueOf(groupMap.get("year"));
				year = year.substring(year.length()-2, year.length()); /** year */
				
				TpmsRoadInfoVO infoVO = tpmsRoadInfoMapper.selectRoadInfoDataById(Long.parseLong(String.valueOf(groupMap.get("tri_id"))));
				String rank = infoVO.getRank();						
				rank = rank.substring(rank.length()-1, rank.length()); /** rank */
				
				String in_code = ""; /** in_code */
				String no = infoVO.getNo();
				if(no.equals("-")) { // 노선 번호가 없을 경우, 도로명 코드 사용

					in_code = tpmsCmmMapper.selectRoadNameCode(infoVO.getName());
					
					if(in_code==null) { // 노선 번호가 없고, 도로명 코드도 없을 경우, tri_id 사용 
						in_code += "B";
						int triIdLen = String.valueOf(infoVO.getId()).length();
						for(int i=1;i<=6-triIdLen;++i) {
							in_code+="0";
						}
						in_code+=String.valueOf(infoVO.getId());
					}
					
				}else { // 노선 번호가 있을 경우, 맨 앞의 문자 'A' 고정, 나머지 문자는 노선번호로 사용
					in_code += "A";
					int noLen = no.length();
					for(int i=1;i<=6-noLen;++i) {
						in_code+="0";
					}
					in_code+=no;
				}
				
				String dir = String.valueOf(groupMap.get("dir")); /** dir */
				if(dir.equals("0")) {
					dir = "U";
				}else {
					dir = "D";
				}
				
				String lane = "1"; /** lane */
				
				/** sector, section */
				long tsm_id = 999999999;
				for(Map<String, Object> obj : groupList) {
					long obj_tsm_id = Long.parseLong(String.valueOf(obj.get("tsm_id")));
					if(tsm_id>obj_tsm_id){
						tsm_id = obj_tsm_id;
					}
				}
				TpmsRoadStationMasterVO tsmVO = tpmsRoadStationMasterMapper.selectStationMasterById(tsm_id);
				String sector = String.valueOf(tsmVO.getSector());
				if(sector.length()<2) sector = "0" + sector;
				String section = String.valueOf(tsmVO.getSection());
				if(section.length()<2) section = "0" + section;
				
				
				// make result value
				for(Map<String, Object> obj : groupList) {
					obj.put("in_code", in_code);
					
					if(type.equals("T")) {
						obj.put("target_id", type+year+rank+in_code+dir+lane+sector+section);
					}else if(type.equals("R")) {
						obj.put("repair_target_id", type+year+rank+in_code+dir+lane+sector+section);
					}
					
					targetGroup.add(obj);
				}
			});
		}
		
		
		
		return targetGroup;
	}


	@Override
	public Object readIniFile(Map<String, Object> map) {
		// TODO Auto-generated method stub
		Map mapValue = new HashMap<>();
		Config.getGlobal().setEscape(false);
		
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream("tpms.ini");
//		InputStream inputStream = servletContext.getResourceAsStream("/WEB-INF/tpms.ini");
		try {
			Ini ini = new Ini(new InputStreamReader(inputStream, "MS949"));
			if(map.get("options")==null) {
				mapValue = ini.get(map.get("section"));
				return mapValue;
			}
			
			map.put("value", ini.get(map.get("section"), map.get("options")));
			return map;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Map<String, Object>> dataReplace(List<Map<String, Object>> resultList) {
	
		if(resultList.size()>0) {
			double extension = 0;
			for(int i=0; i<resultList.size(); i++) {
				int tri_id = Integer.parseInt(String.valueOf(resultList.get(i).get("tri_id"))); 
				int dir = Integer.parseInt(String.valueOf(resultList.get(i).get("dir")));
				int mainNum = Integer.parseInt(String.valueOf(resultList.get(i).get("main_num")));
				int subNum = Integer.parseInt(String.valueOf(resultList.get(i).get("sub_num")));
				String nodeType = (String) resultList.get(i).get("node_type");
				String name = (String) resultList.get(i).get("name");
				
//				//연도 입력
//				resultList.get(i).put("target_year", currYear);
				
				// 노선명 없을때 -로 변환
				if(name.equals("")) resultList.get(i).put("name", "-");
				
				// 차선수가 전부 null 이므로 임의로 "-" 입력
				resultList.get(i).put("lane", "-");
				// 행선 변환 (0=상행, 1=하행)
				if(dir==0) {
					resultList.get(i).put("roadDir", "상행");
				}else if(dir==1) {
					resultList.get(i).put("roadDir", "하행");
				}
				// 노드타입 변환
				if(nodeType !=null) {
					switch (nodeType) {
					case "101" : 
						resultList.get(i).replace("node_type", "교차로");
						break;
					case "102" :
						resultList.get(i).replace("node_type", "도로시·종점");
						break;
					case "103" :
						resultList.get(i).replace("node_type", "속성변화점");
						break;
					case "104" :
						resultList.get(i).replace("node_type", "도로시설물");
						break;
					case "105" :
						resultList.get(i).replace("node_type", "행정경계");
						break;
					case "106" :
						resultList.get(i).replace("node_type", "연결로접속부");
						break;
					case "108" :
						resultList.get(i).replace("node_type", "IC/JC");
						break;
					}
				}
				// 연장 누적 계산	
				if(i==0) {
					extension = (double) resultList.get(i).get("sm_ext");
					resultList.get(i).put("ext_sum", String.format("%.2f",extension));
				}else if(i>0) {
					int prev_triId = Integer.parseInt(String.valueOf(resultList.get(i-1).get("tri_id"))); 
					int prev_dir = Integer.parseInt(String.valueOf(resultList.get(i-1).get("dir")));
					int prev_mainNum = Integer.parseInt(String.valueOf(resultList.get(i-1).get("main_num")));
					int prev_subNum = Integer.parseInt(String.valueOf(resultList.get(i-1).get("sub_num")));
					if(tri_id==prev_triId && dir==prev_dir && mainNum==prev_mainNum && subNum==prev_subNum) {
						extension = extension + (double) resultList.get(i).get("sm_ext");
						resultList.get(i).put("ext_sum", String.format("%.2f",extension));
					}else {
						extension = (double) resultList.get(i).get("sm_ext");
						resultList.get(i).put("ext_sum", String.format("%.2f",extension));
					}
				}
				// 개별연장 소수 둘째자리 변수 추가
				resultList.get(i).put("ext", String.format("%.2f", resultList.get(i).get("sm_ext")));
			}
		}
		
		return resultList;
	}


	@Override
	public String getIndexFormula() {
		Map<String, Object> param = new HashMap<>();
		param.put("section", "setting");
		param.put("options", "formula");
		
		Map<String, Object> ini = (Map<String, Object>) readIniFile(param);
		String formula = (String)ini.get("value");
		
		// TODO Auto-generated method stub
		return formula;
	}


	@Override
	public boolean setIndexFormula(String formula, String contextPath) {
		HashMap<String, Object> param = new HashMap<>();
		param.put("section", "setting");
		HashMap<String, String> options = new HashMap<>();
		options.put("formula", formula);
		param.put("options", options);
		
		boolean res = writeIniFile(param, contextPath);
		
		return res;
	}


	@Override
	public boolean writeIniFile(HashMap<String, Object> param, String contextPath) {
		
		Config.getGlobal().setEscape(false);
		
		Map<String, String> options = (Map<String, String>) param.get("options");
		ClassLoader classLoader = getClass().getClassLoader();
		try {
			InputStream inputStream = classLoader.getResourceAsStream("tpms.ini");
			Wini iniFile = new Wini(new InputStreamReader(inputStream, "MS949"));
			
			Iterator<String> keys = options.keySet().iterator();
	        while( keys.hasNext() ){
	            String key = keys.next();
	            iniFile.put(param.get("section").toString(), key, options.get(key));
	            System.out.println( String.format("키 : %s, 값 : %s", key, param.get(key)) );
	        }
			
            FileOutputStream file_output=new FileOutputStream(contextPath);		 
			DataOutputStream data_out=new DataOutputStream(file_output);
			iniFile.store(new OutputStreamWriter(data_out,"MS949"));
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (InvalidFileFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} 
	}


	@Override
	public int selectConvertCnt(String user_id) {
		// TODO Auto-generated method stub
		return tpmsCmmMapper.selectConvertCnt(user_id);
	}


	@Override
	public String selectUserIdConvert(TpmsConvertStatusVO vo) {
		// TODO Auto-generated method stub
		return tpmsCmmMapper.selectUserIdConvert(vo);
	}



}
