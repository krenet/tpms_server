package egovframework.com.tpms.cmm.service;

public class TpmsConvertStatusVO {

	private long id;
	private String target_id;
	private String ai_status;
	private String pave_video_status;
	private String front_video_status;
	private double ai_progress;
	private double pave_video_progress;
	private double front_video_progress;
	private String convert_start_time;
	private String convert_finish_time;
	private String user_id;
	private String type;
	
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getAi_status() {
		return ai_status;
	}
	public void setAi_status(String ai_status) {
		this.ai_status = ai_status;
	}
	public String getPave_video_status() {
		return pave_video_status;
	}
	public void setPave_video_status(String pave_video_status) {
		this.pave_video_status = pave_video_status;
	}
	public String getFront_video_status() {
		return front_video_status;
	}
	public void setFront_video_status(String front_video_status) {
		this.front_video_status = front_video_status;
	}
	public double getAi_progress() {
		return ai_progress;
	}
	public void setAi_progress(double ai_progress) {
		this.ai_progress = ai_progress;
	}
	public double getPave_video_progress() {
		return pave_video_progress;
	}
	public void setPave_video_progress(double pave_video_progress) {
		this.pave_video_progress = pave_video_progress;
	}
	public double getFront_video_progress() {
		return front_video_progress;
	}
	public void setFront_video_progress(double front_video_progress) {
		this.front_video_progress = front_video_progress;
	}
	public String getConvert_start_time() {
		return convert_start_time;
	}
	public void setConvert_start_time(String convert_start_time) {
		this.convert_start_time = convert_start_time;
	}
	public String getConvert_finish_time() {
		return convert_finish_time;
	}
	public void setConvert_finish_time(String convert_finish_time) {
		this.convert_finish_time = convert_finish_time;
	}
	
	
}
