package egovframework.com.tpms.cmm.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TpmsCmmService {

	//<select id="selectTpmsCode" resultType="tpmsCodeVO">
	public ArrayList<TpmsCodeVO> selectTpmsCode();
	public ArrayList<Map<String, Object>> getTargetId(String type, List<Map<String, Object>> sectionList);
	public Object readIniFile(Map<String, Object> map);
	public boolean writeIniFile(HashMap<String, Object> param, String contextPath);
	public List<Map<String, Object>> dataReplace(List<Map<String, Object>> resultList);
	public String getIndexFormula();
	public boolean setIndexFormula(String formula, String contextPath);
	public int selectConvertCnt(String user_id);
	public String selectUserIdConvert(TpmsConvertStatusVO vo);
}
