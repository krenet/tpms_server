package egovframework.com.tpms.cmm.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.antlr.grammar.v3.CodeGenTreeWalker.element_action_return;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import egovframework.com.tpms.cmm.service.TpmsConvertStatusVO;
import egovframework.com.tpms.cmm.service.impl.TpmsCmmMapper;

@Component
public class TpmsWebSocket extends TextWebSocketHandler{
	
	private static List<WebSocketSession> list = new ArrayList<WebSocketSession>();
	private static HashMap<String, WebSocketSession> map = new HashMap<>();
	/*private List<WebSocketSession> listeners = new ArrayList<WebSocketSession>();
	
	public void addListener(WebSocketSession toAdd) {
		listeners.add(toAdd);
	}*/
	@Resource(name="tpmsCmmMapper")
	private TpmsCmmMapper tpmsCmmMapper;
	
	public void sendDataToClient(TpmsConvertStatusVO vo) {

		/*if(vo.getType().equals("ai")) {
			if(vo.getAi_progress()>=100) {
				
			}
		}*/
		
		
		Set keys = map.keySet();
		
		String key = "";
//		String user_id = "";
		for (Object object : keys) {
			key=object.toString();
//			if(key.contains("_")) user_id = key.split("_")[0];
			if(key.contains(vo.getUser_id())) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					String jsonString = mapper.writeValueAsString(vo);
					map.get(key).sendMessage(new TextMessage(jsonString));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	// 클라이언트 연결 시도 시 호출
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception{
		list.add(session);
		System.out.println("add new session");
	}
	
	public HashMap<String, Object> getProgressSet(TpmsConvertStatusVO vo, String type){
		HashMap<String, Object> map = new HashMap<>();
		
//		map.put("target_id", vo.getTarget_id());
		switch (type) {
		case "ai":
			if(vo.getAi_status().equals("complete")) {
				map.put("title", "균열율 분석 AI 진행상황");
				map.put("type", "ai");
				map.put("progress", 100);
			}else {
				map.put("title", "균열율 분석 AI 진행상황");
				map.put("type", "ai");
				map.put("progress", vo.getAi_progress());
			}
			break;
		case "pave":
			if(vo.getPave_video_status().equals("complete")) {
				map.put("title", "포장 이미지 동영상 변환 진행상황");
				map.put("type", "pave");
				map.put("progress", 100);
			}else {
				map.put("title", "포장 이미지 동영상 변환 진행상황");
				map.put("type", "pave");
				map.put("progress", vo.getPave_video_progress());
			}
			break;
		case "front":
			if(vo.getFront_video_status().equals("complete")) {
				map.put("title", "전방 이미지 동영상 변환 진행상황");
				map.put("type", "front");
				map.put("progress", 100);
			}else {
				map.put("title", "전방 이미지 동영상 변환 진행상황");
				map.put("type", "front");
				map.put("progress", vo.getFront_video_progress());
			}
		
		}
		
		return map;
	}
	
	// 클라이언트가 메시지 보냈을 때 호출
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		String user_id = message.getPayload();
		System.out.println("client send user_id : "+ user_id);
		
		String mapId = user_id+"_"+session.getId();
		
		if(!map.containsKey(mapId)) {
			map.remove(mapId);
			map.put(mapId, session); // [user_id, session];
			ArrayList<TpmsConvertStatusVO> list = tpmsCmmMapper.selectConvertData(user_id);
			ArrayList<HashMap<String, Object>> convertList = null;
			HashMap<String, Object> res = new HashMap<>();
			for (TpmsConvertStatusVO vo: list) {
				convertList = new ArrayList<HashMap<String, Object>>();
				convertList.add(getProgressSet(vo, "ai"));
				convertList.add(getProgressSet(vo, "pave"));
				convertList.add(getProgressSet(vo, "front"));
				res.put(vo.getTarget_id(), convertList);
			}
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				String jsonString = mapper.writeValueAsString(res);
				session.sendMessage(new TextMessage(jsonString));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/** pass data to client */
		/*HashMap<String, Object> test = new HashMap<>();
		test.put("test1", "test1");
		test.put("test2", "test2");
		JSONObject json = new JSONObject(test);
		session.sendMessage(new TextMessage(json.toString()));*/
		
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		System.out.println("클라이언트와의 연결 해제");
		list.remove(session);
		map.values().remove(session);
	}

	
	
}
