package egovframework.com.tpms.cmm.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

@Configuration
@EnableWebSocket
public class TpmsWsConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer{

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		// TODO Auto-generated method stub
		registry.addHandler(tpmsWebSocketHandler(), "/tpms/ws.do").addInterceptors(new HttpSessionHandshakeInterceptor()).setAllowedOrigins("*");
	}
	
	@Bean
	public TpmsWebSocket tpmsWebSocketHandler() {
		return new TpmsWebSocket();
	}

}
