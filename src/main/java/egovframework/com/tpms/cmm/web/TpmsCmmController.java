package egovframework.com.tpms.cmm.web;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.cmm.service.TpmsCodeVO;
import egovframework.com.tpms.cmm.service.TpmsConvertStatusVO;
import egovframework.com.tpms.cmm.service.TpmsCmmService;

@RestController
public class TpmsCmmController {

	@Resource(name="tpmsCmmService")
	private TpmsCmmService tpmsCmmService;
	
	/***
	 * Test select tpms code
	 */
//	@JsonSerialize 
//	@RequestMapping(value="/testCode.do", method=RequestMethod.GET, produces="application/json;charset=UTF-8")
//	public ArrayList<TestVO> testSelectTpmsCode(){
//		return tpmsCmmService.testSelectTpmsCode();
//	}
	@RequestMapping(value="/tpms/selectTpmsCode.do", method=RequestMethod.POST)
	public ModelAndView selectTpmsCode(){
		ArrayList<TpmsCodeVO> list = tpmsCmmService.selectTpmsCode();
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("codeList", list);
		
		return mav;
	}
	
	/*
	 * tpmsCmmService.getTargetId Test Code
	 *  
	@RequestMapping(value="/tpms/getTest.do", method=RequestMethod.GET)
	public ModelAndView getTest(){
		ArrayList<Map<String, Object>> res = tpmsCmmService.getTargetId("T", null);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}*/
	
	@RequestMapping(value="/tpms/getIndexFormula.do", method=RequestMethod.POST)
	public ModelAndView getIndexFormula(){
		String formula = tpmsCmmService.getIndexFormula();
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", formula); 
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/setIndexFormula.do", method=RequestMethod.POST)
	public ModelAndView setIndexFormula(@RequestParam String formula, HttpServletRequest request){
		boolean res =  tpmsCmmService.setIndexFormula(formula, request.getServletContext().getRealPath("/WEB-INF/classes/tpms.ini"));
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", res);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getConvertMediaCnt.do", method=RequestMethod.POST)
	public ModelAndView getConvertMediaCnt(@RequestParam String user_id) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsCmmService.selectConvertCnt(user_id));
		return mav;
	}
	
	/*@RequestMapping(value="/tpms/test.do", method=RequestMethod.GET)
	public ModelAndView test(@RequestParam HashMap<String, Object> param) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", param);
		return mav;
	}*/
	
	@RequestMapping(value="/tpms/changeProgress.do", method=RequestMethod.GET)
	public void test(@RequestParam HashMap<String, Object> param) {
		TpmsWebSocket tws = new TpmsWebSocket();
		TpmsConvertStatusVO convertVo = new TpmsConvertStatusVO();
		convertVo.setType(param.get("type")+"");
		convertVo.setTarget_id(param.get("target_id")+"");
		
		String user_id = tpmsCmmService.selectUserIdConvert(convertVo);
		convertVo.setUser_id(user_id);
		
		switch (param.get("type")+"") {
		case "ai":
			convertVo.setAi_progress(Double.parseDouble(param.get("progress")+""));
			break;
		case "front":
			convertVo.setFront_video_progress(Double.parseDouble(param.get("progress")+""));
			break;
		case "pave":
			convertVo.setPave_video_progress(Double.parseDouble(param.get("progress")+""));
			break;
		default:
			break;
		}
		
		tws.sendDataToClient(convertVo);
	}
	
}
