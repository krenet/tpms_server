package egovframework.com.tpms.surveysc.service.impl;

import java.util.List;
import java.util.Map;

import egovframework.com.tpms.surveysc.service.TpmsRoadSurveyScVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsSurveyScMapper")
public interface TpmsRoadSurveyScMapper {
	//<select id="selectNewRoadSurveyScVersion" resultType="int">
	public int selectNewRoadSurveyScVersion();
	public int insertNewRoadSurverScVersion(String etc);
	public int insertUpdateRoadSurverScVersion(String etc);
	public int insertRoadSurveyScData(TpmsRoadSurveyScVO vo);
	public Map<String, Object> selectScDgreeNewVersion(Map<String, Object> map);
	public List<Map<String, Object>> selectSmMaster(Map<String, Object> map);
	public List<Map<String, Object>> selectDegreeMaster(TpmsRoadSurveyScVO vo);
	public List<Map<String, Object>> selectDegreeView(TpmsRoadSurveyScVO vo);
	public int insertSurveySc(TpmsRoadSurveyScVO vo);
	public int deleteScForDegree(int degree);
}
