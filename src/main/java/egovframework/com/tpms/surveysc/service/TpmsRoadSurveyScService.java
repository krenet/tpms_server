package egovframework.com.tpms.surveysc.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TpmsRoadSurveyScService {
	public int insertRoadSurveyScData(HashMap<String,Object> map);
	public Map<String, Object> selectScDgreeNewVersion(Map<String,Object> map);
	public List<Map<String, Object>> selectSmMaster(Map<String, Object> map);
	public List<Map<String,Object>> selectDegreeMaster(HashMap<String,Object> map);
	public List<Map<String,Object>> selectDegreeView(HashMap<String,Object> map);
	public int insertSurveySc(HashMap<String,Object> map);
}
