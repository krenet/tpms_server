package egovframework.com.tpms.surveysc.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import egovframework.com.tpms.surveysc.service.TpmsRoadSurveyScService;
import egovframework.com.tpms.surveysc.service.TpmsRoadSurveyScVO;

@Service("tpmsRoadSurveyScService")
@Transactional
public class TpmsRoadSurveyScImpl implements TpmsRoadSurveyScService{
	@Resource(name="tpmsSurveyScMapper")
	private TpmsRoadSurveyScMapper tpmsSurveyScMapper;

	@Override
	public int insertRoadSurveyScData(HashMap<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadSurveyScVO vo = mapper.convertValue(map, TpmsRoadSurveyScVO.class);	
		int version = tpmsSurveyScMapper.selectNewRoadSurveyScVersion();
		
		if(version == 0){
			tpmsSurveyScMapper.insertNewRoadSurverScVersion("");
			version = 1;
		}else {
			tpmsSurveyScMapper.insertUpdateRoadSurverScVersion("");
			version = tpmsSurveyScMapper.selectNewRoadSurveyScVersion();
		}
		
		vo.setVersion(version);
		int res = tpmsSurveyScMapper.insertRoadSurveyScData(vo);
		
		return res;
	}

	@Override
	public Map<String, Object> selectScDgreeNewVersion(Map<String, Object> map) {
		Map<String, Object> result = tpmsSurveyScMapper.selectScDgreeNewVersion(map);
		
		return result;
	}

	@Override
	public List<Map<String, Object>> selectSmMaster(Map<String, Object> map) {
		
		List<Map<String, Object>> smList = tpmsSurveyScMapper.selectSmMaster(map);
		
		return smList;
	}

	@Override
	public List<Map<String, Object>> selectDegreeMaster(HashMap<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadSurveyScVO vo = mapper.convertValue(map, TpmsRoadSurveyScVO.class);	
		List<Map<String, Object>> scsmList = tpmsSurveyScMapper.selectDegreeMaster(vo);
		return scsmList;
	}

	@Override
	public List<Map<String, Object>> selectDegreeView(HashMap<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadSurveyScVO vo = mapper.convertValue(map, TpmsRoadSurveyScVO.class);	
		List<Map<String, Object>> viewList = tpmsSurveyScMapper.selectDegreeView(vo);
		return viewList;
	}

	@Override
	public int insertSurveySc(HashMap<String, Object> map) {
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = null;
		TpmsRoadSurveyScVO[] voList = null;
		try {
			jsonStr = java.net.URLDecoder.decode((String)map.get("scList"), "UTF-8");
			voList = mapper.readValue(jsonStr, TpmsRoadSurveyScVO[].class);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
//		TpmsRoadSurveyScVO[] voList = mapper.convertValue(map.get("scList"), TpmsRoadSurveyScVO[].class);
		tpmsSurveyScMapper.deleteScForDegree(voList[0].getDegree());
		int result = 0;
		for (TpmsRoadSurveyScVO vo : voList) {
			result = tpmsSurveyScMapper.insertSurveySc(vo);
		}
		System.out.println(result);
		return result;
	}

	
//	@Override
//	public TpmsRoadSurveyScVO insertRoadSurveyScData(HashMap<String, Object> map) {
//		ObjectMapper mapper = new ObjectMapper();
//		TpmsRoadSurveyScVO vo = mapper.convertValue(map, TpmsRoadSurveyScVO.class);		
//		
//		int version = tpmsSurveyScMapper.selectNewRoadSurveyScVersion();
//		
//		if(version == 0){
//			tpmsSurveyScMapper.insertNewRoadSurverScVersion("");
//			version = 1;
//		}else {
//			tpmsSurveyScMapper.insertUpdateRoadSurverScVersion("");
//			version = tpmsSurveyScMapper.selectNewRoadSurveyScVersion();
//		}
//		
//		vo.setVersion(version);
//		tpmsSurveyScMapper.insertRoadSurveyScData(vo);
//		
//		return vo;
//	}
}
