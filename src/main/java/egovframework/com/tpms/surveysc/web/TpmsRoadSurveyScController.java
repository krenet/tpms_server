package egovframework.com.tpms.surveysc.web;

import java.util.HashMap;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.surveysc.service.TpmsRoadSurveyScService;

@RestController
public class TpmsRoadSurveyScController {
	@Resource(name="tpmsRoadSurveyScService")
	private TpmsRoadSurveyScService tpmsRoadSurveyScService;

	@RequestMapping(value="/tpms/insertRoadSurveyScData.do", method=RequestMethod.POST)
	public ModelAndView insertRoadSurveyScData(@RequestParam HashMap<String, Object> map){

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		int res = tpmsRoadSurveyScService.insertRoadSurveyScData(map);
		mav.addObject("res", res);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/selectScDgreeNewVersion.do", method=RequestMethod.POST)
	public ModelAndView selectScDgreeNewVersion(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res",  tpmsRoadSurveyScService.selectScDgreeNewVersion(map));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/selectSmMaster.do", method=RequestMethod.POST)
	public ModelAndView selectSmMaster(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res",  tpmsRoadSurveyScService.selectSmMaster(map));
		
		return mav;
	}
	
	
	@RequestMapping(value="/tpms/selectDegreeMaster.do", method=RequestMethod.POST)
	public ModelAndView selectDegreeMaster(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res",  tpmsRoadSurveyScService.selectDegreeMaster(map));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/selectDegreeView.do", method=RequestMethod.POST)
	public ModelAndView selectDegreeView(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res",  tpmsRoadSurveyScService.selectDegreeView(map));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/insertSurveySc.do", method=RequestMethod.POST)
	public ModelAndView insertSurveySc(@RequestParam HashMap<String, Object> map){

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		int res = tpmsRoadSurveyScService.insertSurveySc(map);
		mav.addObject("res", res);
		
		return mav;
	}
	
}
