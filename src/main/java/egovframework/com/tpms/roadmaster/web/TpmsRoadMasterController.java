package egovframework.com.tpms.roadmaster.web;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterService;

@RestController
public class TpmsRoadMasterController {

	@Resource(name="tpmsRoadMasterService")
	private TpmsRoadMasterService tpmsRoadMasterService;
	
	
	// POST방식 RequestParam방식 된다.
	@RequestMapping(value="/tpms/getNoList.do", method=RequestMethod.POST)
	public ModelAndView getNoList(@RequestParam HashMap<String, Object> map){

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadMasterService.getNoList(map));		
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getNameList.do", method=RequestMethod.POST)
	public ModelAndView getNameList(@RequestParam HashMap<String, Object> map){

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadMasterService.getNameList(map));		

		return mav;
	}
	
	@RequestMapping(value="/tpms/getSmList.do", method=RequestMethod.POST)
	public ModelAndView getSmList(@RequestParam HashMap<String, Object> map){

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		System.out.println("map: "+map);
		mav.addObject("res", tpmsRoadMasterService.getSmList(map));		
		
//		List<Map<String, Object>> result = tpmsRoadMasterService.getSmList(map);
//		System.out.println("result: "+result);

		return mav;
	}
	
	@RequestMapping(value="/tpms/geoXYData.do", method=RequestMethod.POST)
	public ModelAndView geoXYData(@RequestParam HashMap<String, Object> map){

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRoadMasterService.geoXYData(map));		

		return mav;
	}
}