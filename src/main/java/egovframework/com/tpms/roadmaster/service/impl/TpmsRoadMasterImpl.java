package egovframework.com.tpms.roadmaster.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterService;
import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterVO;


@Service("tpmsRoadMasterService")
@Transactional
public class TpmsRoadMasterImpl implements TpmsRoadMasterService{
	
	@Resource(name="tpmsRoadMasterMapper")
	private TpmsRoadMasterMapper tpmsRoadMasterMapper;
	
	@Override
	public ArrayList<String> getNoList(Map<String, Object> map){
		
		ArrayList<String> list = tpmsRoadMasterMapper.getNoList(map);
		return ObjectArraySort(list);
	}
	
	public ArrayList<String> ObjectArraySort(ArrayList<String> list){

		ArrayList<String> res = new ArrayList<>();
		ArrayList<Integer> tempList = new ArrayList<>();
		int no = 0;
		for (String string : list) {
			try {
				no = Integer.parseInt(string);
				tempList.add(no);
			}catch(Exception e) {
				res.add(string);
			}
		}
		
		Collections.sort(tempList);
		
		for (Integer integer : tempList) {
			res.add(integer.toString());
		}
		
		return res;
	}
	
	@Override
	public List<Map<String, Object>> getNameList(Map<String, Object> map){

		List<Map<String, Object>> result = tpmsRoadMasterMapper.getNameList(map);
		for(int i=0; i<result.size(); i++) {
			String name = (String)result.get(i).get("name");
			if(name.equals("")) {
				result.get(i).replace("name","-");
			}
		}
		
		return result;
	}
	
	@Override
	public List<Map<String, Object>> getSmList(Map<String, Object> map){
		
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadMasterVO vo = mapper.convertValue(map, TpmsRoadMasterVO.class);
		
		List<Map<String, Object>> masterList = tpmsRoadMasterMapper.getSmList(vo);
		
		if(masterList.size()>0) {
			double extension = 0;
			for(int i=0; i<masterList.size(); i++) {
				int tri_id = Integer.parseInt(String.valueOf(masterList.get(i).get("tri_id"))); 
				int dir = Integer.parseInt(String.valueOf(masterList.get(i).get("dir")));
				int mainNum = Integer.parseInt(String.valueOf(masterList.get(i).get("main_num")));
				int subNum = Integer.parseInt(String.valueOf(masterList.get(i).get("sub_num")));
				String nodeType = (String) masterList.get(i).get("node_type");
				String name = (String) masterList.get(i).get("name");
				
				// 노선명 없을때 -로 변환
				if(name.equals("")) masterList.get(i).put("name", "-");
				
				// 차선수가 전부 null 이므로 임의로 "-" 입력
				masterList.get(i).put("lane", "-");
				// 행선 변환 (0=상행, 1=하행)
				if(dir==0) {
					masterList.get(i).put("rev_dir", "상행");
				}else if(dir==1) {
					masterList.get(i).put("rev_dir", "하행");
				}
				// 노드타입 변환
				if(nodeType !=null) {
					switch (nodeType) {
					case "101" : 
						masterList.get(i).replace("node_type", "교차로");
						break;
					case "102" :
						masterList.get(i).replace("node_type", "도로시·종점");
						break;
					case "103" :
						masterList.get(i).replace("node_type", "속성변화점");
						break;
					case "104" :
						masterList.get(i).replace("node_type", "도로시설물");
						break;
					case "105" :
						masterList.get(i).replace("node_type", "행정경계");
						break;
					case "106" :
						masterList.get(i).replace("node_type", "연결로접속부");
						break;
					case "108" :
						masterList.get(i).replace("node_type", "IC/JC");
						break;
					}
				}
				// 연장 누적 계산	
				if(i==0) {
					extension = (double) masterList.get(i).get("sm_ext");
					masterList.get(i).put("ext_sum",  String.format("%.2f",extension));
				}else if(i>0) {
					int prev_triId = Integer.parseInt(String.valueOf(masterList.get(i-1).get("tri_id"))); 
					int prev_dir = Integer.parseInt(String.valueOf(masterList.get(i-1).get("dir")));
					int prev_mainNum = Integer.parseInt(String.valueOf(masterList.get(i-1).get("main_num")));
					int prev_subNum = Integer.parseInt(String.valueOf(masterList.get(i-1).get("sub_num")));
					if(tri_id==prev_triId && dir==prev_dir && mainNum==prev_mainNum && subNum==prev_subNum) {
						extension = extension + (double) masterList.get(i).get("sm_ext");
						masterList.get(i).put("ext_sum",  String.format("%.2f",extension));
					}else {
						extension = (double) masterList.get(i).get("sm_ext");
						masterList.get(i).put("ext_sum",  String.format("%.2f",extension));
					}
				}
				// 개별연장 소수 둘째자리 변수 추가
				masterList.get(i).put("ext", String.format("%.2f", masterList.get(i).get("sm_ext")));
			}

		}
		
		System.out.println("확인: "+masterList);
		return masterList;
	}

	@Override
	public List<Map<String, Object>> geoXYData(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> result = tpmsRoadMasterMapper.geoXYData(map);
		
		return result;
	}


	
}
