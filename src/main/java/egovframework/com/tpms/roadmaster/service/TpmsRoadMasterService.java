package egovframework.com.tpms.roadmaster.service;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface TpmsRoadMasterService {
	public ArrayList<String> getNoList(Map<String, Object> map);	
	public List<Map<String, Object>> getNameList(Map<String, Object> map);	
	public List<Map<String, Object>> getSmList(Map<String, Object> map);
	public List<Map<String, Object>> geoXYData(HashMap<String,Object> map);
}
