package egovframework.com.tpms.roadmaster.service;

public class TpmsRoadMasterVO{
	
	private int year;
	private String rank;
	private String no;
	private int tri_id;
	private int dir=-1;
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public int getTri_id() {
		return tri_id;
	}
	public void setTri_id(int tri_id) {
		this.tri_id = tri_id;
	}
	public int getDir() {
		return dir;
	}
	public void setDir(int dir) {
		this.dir = dir;
	}
	
}