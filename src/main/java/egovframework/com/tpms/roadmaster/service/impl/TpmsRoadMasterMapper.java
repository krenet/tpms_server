package egovframework.com.tpms.roadmaster.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsRoadMasterMapper")
public interface TpmsRoadMasterMapper {
	// <select id="selectNoList" parameterType="String"  resultType="map">
	public ArrayList<String> getNoList(Map<String, Object> map);
	public List<Map<String, Object>> getNameList(Map<String, Object> map);
	public List<Map<String, Object>> getSmList(TpmsRoadMasterVO vo);
	//<select id="geoXYData" resultType="map">
	public List<Map<String, Object>> geoXYData(Map<String, Object> param); 
	
}
