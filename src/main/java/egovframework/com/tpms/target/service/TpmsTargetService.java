package egovframework.com.tpms.target.service;

import java.util.Map;
import java.util.List;

public interface TpmsTargetService {
	
	public List<Map<String, Object>> getMasterForTarget();
	public int checkTargetList(Map<String, Object> map);
	public List<Map<String, Object>> getCurrentYearTarget(Map<String, Object> map);
	public List<Map<String, Object>> getYearList();
	public List<Map<String, Object>> getAllTarget(Map<String, Object> map);
	public List<Map<String, Object>> searchRecord(Map<String, Object> map);
	public List<Map<String, Object>> searchTarget(Map<String, Object> map);
	public List<Map<String, Object>> getSchedule(Map<String, Object> map);	
	public int getMaxGroupNum(Map<String, Object> map);
	public List<Map<String, Object>> getUpdateTempList(Map<String, Object> map);
	public int deleteFromTemp(Map<String, Object> map);
	public List<Map<String, Object>> afterDelete(Map<String, Object> map);
	public List<Map<String, Object>> getAllTempTarget();
	public int deleteFromTarget(List<Map<String, Object>> list);
	public int saveUpdatedTarget(List<Map<String, Object>> list);
	public int deleteAllTarget(int year);
}
