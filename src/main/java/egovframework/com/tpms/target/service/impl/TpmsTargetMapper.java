package egovframework.com.tpms.target.service.impl;

import java.util.List;
import java.util.Map;

import egovframework.com.tpms.target.service.TpmsTargetVO;
import egovframework.com.tpms.target.service.TpmsTempTargetVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;


@Mapper ("tpmsTargetMapper")
public interface TpmsTargetMapper {
	
	public List<Map<String, Object>> getMasterForTarget();
	public int checkTargetList(int year);
	public List<Map<String, Object>> getCurrentYearTarget(int year);
	public int deleteAllTemp();
	public int insertTempTarget(TpmsTempTargetVO vo);
	public List<Map<String, Object>> getYearList();
	public List<Map<String, Object>> getAllTarget(TpmsTargetVO vo);
	public List<Map<String, Object>> searchRecord(TpmsTargetVO vo);
	public List<Map<String, Object>> searchTarget(TpmsTargetVO vo);
	public List<Map<String, Object>> searchSchedule(TpmsTargetVO vo);
	public Map<String, Object> getScDegree();
	public List<Map<String, Object>> getSchedule(Map<String, Integer> map);
	public int getMaxGroupNum(Map<String, Integer> map);
	public Map<String, Object> getMatchData(int ts_id);
	public int deleteFromTemp(int ts_id);
	public List<Map<String, Object>> getAllTempTarget();
	public List<Map<String, Object>> getOriginTarget(int year);
	public int deleteFromTarget(Map<String, Integer> map);
	public int saveUpdatedTarget(TpmsTempTargetVO vo);
	public int deleteAllTarget(int year);
}
