package egovframework.com.tpms.target.service;

public class TpmsTempTargetVO {
	private long id;
	private int year;
	private long tri_id;
	private long tsm_id;
	private long ts_id;
	private String rank;
	private String no;
	private String name;
	private int main_num;
	private int sub_num;
	private int dir;
	private String rev_dir;
	private String roadDir;
	private int sm_order;
	private int lane;
	private int sector;
	private int section;
	private int history_n;
	private int seq_n;
	private int order;
	private String type;
	private String node_id;
	private String node_name;
	private String node_type;
	private int ransom_node;
	private String target_id;
	private double sm_ext;
	private double st_ext;
	private double ext_sum;
	private int group_num;
	private String in_code;
	private double ext;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public long getTri_id() {
		return tri_id;
	}
	public void setTri_id(long tri_id) {
		this.tri_id = tri_id;
	}
	public long getTsm_id() {
		return tsm_id;
	}
	public void setTsm_id(long tsm_id) {
		this.tsm_id = tsm_id;
	}
	public long getTs_id() {
		return ts_id;
	}
	public void setTs_id(long ts_id) {
		this.ts_id = ts_id;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMain_num() {
		return main_num;
	}
	public void setMain_num(int main_num) {
		this.main_num = main_num;
	}
	public int getSub_num() {
		return sub_num;
	}
	public void setSub_num(int sub_num) {
		this.sub_num = sub_num;
	}
	public int getDir() {
		return dir;
	}
	public void setDir(int dir) {
		this.dir = dir;
	}
	public String getRev_dir() {
		return rev_dir;
	}
	public void setRev_dir(String rev_dir) {
		this.rev_dir = rev_dir;
	}
	public String getRoadDir() {
		return roadDir;
	}
	public void setRoadDir(String roadDir) {
		this.roadDir = roadDir;
	}
	public int getSm_order() {
		return sm_order;
	}
	public void setSm_order(int sm_order) {
		this.sm_order = sm_order;
	}
	public int getLane() {
		return lane;
	}
	public void setLane(int lane) {
		this.lane = lane;
	}
	public int getSector() {
		return sector;
	}
	public void setSector(int sector) {
		this.sector = sector;
	}
	public int getSection() {
		return section;
	}
	public void setSection(int section) {
		this.section = section;
	}
	public int getHistory_n() {
		return history_n;
	}
	public void setHistory_n(int history_n) {
		this.history_n = history_n;
	}
	public int getSeq_n() {
		return seq_n;
	}
	public void setSeq_n(int seq_n) {
		this.seq_n = seq_n;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNode_id() {
		return node_id;
	}
	public void setNode_id(String node_id) {
		this.node_id = node_id;
	}
	public String getNode_name() {
		return node_name;
	}
	public void setNode_name(String node_name) {
		this.node_name = node_name;
	}
	public String getNode_type() {
		return node_type;
	}
	public void setNode_type(String node_type) {
		this.node_type = node_type;
	}
	public int getRansom_node() {
		return ransom_node;
	}
	public void setRansom_node(int ransom_node) {
		this.ransom_node = ransom_node;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public double getSm_ext() {
		return sm_ext;
	}
	public void setSm_ext(double sm_ext) {
		this.sm_ext = sm_ext;
	}
	public double getSt_ext() {
		return st_ext;
	}
	public void setSt_ext(double st_ext) {
		this.st_ext = st_ext;
	}
	public double getExt_sum() {
		return ext_sum;
	}
	public void setExt_sum(double ext_sum) {
		this.ext_sum = ext_sum;
	}
	public int getGroup_num() {
		return group_num;
	}
	public void setGroup_num(int group_num) {
		this.group_num = group_num;
	}
	public String getIn_code() {
		return in_code;
	}
	public void setIn_code(String in_code) {
		this.in_code = in_code;
	}
	public double getExt() {
		return ext;
	}
	public void setExt(double ext) {
		this.ext = ext;
	}
	
}
