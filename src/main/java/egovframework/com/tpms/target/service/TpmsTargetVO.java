package egovframework.com.tpms.target.service;

public class TpmsTargetVO{
	
	private int planYear;
	private int year;
	private String rank;
	private String no;
	private int tri_id;
	private int dir=-1;
	private int degree;
	private int trsd_id;
	private String scheduleOn;
	
	public int getPlanYear() {
		return planYear;
	}
	public void setPlanYear(int planYear) {
		this.planYear = planYear;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public int getTri_id() {
		return tri_id;
	}
	public void setTri_id(int tri_id) {
		this.tri_id = tri_id;
	}
	public int getDir() {
		return dir;
	}
	public void setDir(int dir) {
		this.dir = dir;
	}
	public int getDegree() {
		return degree;
	}
	public void setDegree(int degree) {
		this.degree = degree;
	}
	public int getTrsd_id() {
		return trsd_id;
	}
	public void setTrsd_id(int trsd_id) {
		this.trsd_id = trsd_id;
	}
	public String getScheduleOn() {
		return scheduleOn;
	}
	public void setScheduleOn(String scheduleOn) {
		this.scheduleOn = scheduleOn;
	}

	
}