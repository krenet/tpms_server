package egovframework.com.tpms.target.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.map.HashedMap;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import egovframework.com.tpms.roadinfo.service.TpmsRoadInfoVO;
import egovframework.com.tpms.roadmaster.service.TpmsRoadMasterVO;
import egovframework.com.tpms.target.service.TpmsTargetService;
import egovframework.com.tpms.target.service.TpmsTargetVO;
import egovframework.com.tpms.target.service.TpmsTempTargetVO;

@Service("tpmsTargetService")
@Transactional
public class TpmsTargetImpl implements TpmsTargetService{
	
	@Resource(name="tpmsTargetMapper")
	private TpmsTargetMapper tpmsTargetMapper;
	
	
	@Override
	public List<Map<String, Object>> getMasterForTarget() {
		
		List<Map<String, Object>> masterList = tpmsTargetMapper.getMasterForTarget();
		if(masterList.size()>0) {
			double extension = 0;
			for(int i=0; i<masterList.size(); i++) {
				int tri_id = Integer.parseInt(String.valueOf(masterList.get(i).get("tri_id"))); 
				int dir = Integer.parseInt(String.valueOf(masterList.get(i).get("dir")));
				int mainNum = Integer.parseInt(String.valueOf(masterList.get(i).get("main_num")));
				int subNum = Integer.parseInt(String.valueOf(masterList.get(i).get("sub_num")));
				String nodeType = (String) masterList.get(i).get("node_type");
				String name = (String) masterList.get(i).get("name");
				
				// 노선명 없을때 -로 변환
				if(name.equals("")) masterList.get(i).put("name", "-");
				
				// 차선수가 전부 null 이므로 임의로 "-" 입력
				masterList.get(i).put("lane", "-");
				// 행선 변환 (0=상행, 1=하행)
				if(dir==0) {
					masterList.get(i).put("rev_dir", "상행");
				}else if(dir==1) {
					masterList.get(i).put("rev_dir", "하행");
				}
				// 노드타입 변환
				if(nodeType !=null) {
					switch (nodeType) {
					case "101" : 
						masterList.get(i).replace("node_type", "교차로");
						break;
					case "102" :
						masterList.get(i).replace("node_type", "도로시·종점");
						break;
					case "103" :
						masterList.get(i).replace("node_type", "속성변화점");
						break;
					case "104" :
						masterList.get(i).replace("node_type", "도로시설물");
						break;
					case "105" :
						masterList.get(i).replace("node_type", "행정경계");
						break;
					case "106" :
						masterList.get(i).replace("node_type", "연결로접속부");
						break;
					case "108" :
						masterList.get(i).replace("node_type", "IC/JC");
						break;
					}
				}
				// 연장 누적 계산	
				if(i==0) {
					extension = (double) masterList.get(i).get("sm_ext");
					masterList.get(i).put("ext_sum",  String.format("%.2f",extension));
				}else if(i>0) {
					int prev_triId = Integer.parseInt(String.valueOf(masterList.get(i-1).get("tri_id"))); 
					int prev_dir = Integer.parseInt(String.valueOf(masterList.get(i-1).get("dir")));
					int prev_mainNum = Integer.parseInt(String.valueOf(masterList.get(i-1).get("main_num")));
					int prev_subNum = Integer.parseInt(String.valueOf(masterList.get(i-1).get("sub_num")));
					if(tri_id==prev_triId && dir==prev_dir && mainNum==prev_mainNum && subNum==prev_subNum) {
						extension = extension + (double) masterList.get(i).get("sm_ext");
						masterList.get(i).put("ext_sum",  String.format("%.2f",extension));
					}else {
						extension = (double) masterList.get(i).get("sm_ext");
						masterList.get(i).put("ext_sum",  String.format("%.2f",extension));
					}
				}
				// 개별연장 소수 둘째자리 변수 추가
				masterList.get(i).put("ext", String.format("%.2f", masterList.get(i).get("sm_ext")));
			}

		}
		return masterList;
	}

	@Override
	public int checkTargetList(Map<String, Object> map) {
		
//		map.replace("year", 2050);
		int year = Integer.parseInt(String.valueOf(map.get("year")));
		int checkTarget = tpmsTargetMapper.checkTargetList(year);
//		System.out.println("대상구간 개수: "+checkTarget);
		
		return checkTarget;
	}
	
	@Override
	public List<Map<String, Object>> getCurrentYearTarget(Map<String, Object> map) {
		
		int year = Integer.parseInt(String.valueOf(map.get("year")));
		List<Map<String, Object>> targetList = tpmsTargetMapper.getCurrentYearTarget(year);	
		
		tpmsTargetMapper.deleteAllTemp();
		
		ObjectMapper objectMapper = new ObjectMapper();
		List<TpmsTempTargetVO> list = objectMapper.convertValue(targetList, new TypeReference<List<TpmsTempTargetVO>>() {});	
		
		for (TpmsTempTargetVO tpmsTempTargetVO : list) {
			tpmsTargetMapper.insertTempTarget(tpmsTempTargetVO);
		}
		/*
		 * for(int i=0; i<targetList.size(); i++) { //
		 * System.out.println("노드 아이디: "+targetList.get(i).get("node_id"));
		 * tpmsTargetMapper.insertTempTarget(targetList.get(i)); }
		 */
//		targetList = dataReplace(targetList);
		
		return targetList;
	}
	
	@Override
	public List<Map<String, Object>> getYearList() {
		List<Map<String, Object>> list = tpmsTargetMapper.getYearList();

		return list;
	}
	
	@Override
	public List<Map<String, Object>> getAllTarget(Map<String, Object> map) {
		
		ObjectMapper mapper = new ObjectMapper();
		TpmsTargetVO vo = mapper.convertValue(map, TpmsTargetVO.class);
		return tpmsTargetMapper.getAllTarget(vo);
	}

	@Override
	public List<Map<String, Object>> searchRecord(Map<String, Object> map) {
		
//		int planYear = Integer.parseInt(String.valueOf(map.get("planYear")));
		ObjectMapper mapper = new ObjectMapper();
		TpmsTargetVO vo = mapper.convertValue(map, TpmsTargetVO.class);
		List<Map<String, Object>> searchRecordList = tpmsTargetMapper.searchRecord(vo);		
		
//		searchRecordList = dataReplace(searchRecordList);
		
		return searchRecordList;
	}
	
	@Override
	public List<Map<String, Object>> searchTarget(Map<String, Object> map) {
		
		List<Map<String, Object>> searchTargetList = new ArrayList<Map<String,Object>>();
		ObjectMapper mapper = new ObjectMapper();
		TpmsTargetVO vo = mapper.convertValue(map, TpmsTargetVO.class);
		searchTargetList = tpmsTargetMapper.searchTarget(vo);		
//		searchTargetList = dataReplace(searchTargetList);			
		
		return searchTargetList;
	}
	
	@Override
	public List<Map<String, Object>> getSchedule(Map<String, Object> map){
		
		// check sc_degree 
		Map<String, Object> degreeInfo = tpmsTargetMapper.getScDegree();
		
		System.out.println("degreeInfo: "+degreeInfo);
		
		int currYear = Integer.parseInt(String.valueOf(map.get("year")));
		int scYear = Integer.parseInt(String.valueOf(degreeInfo.get("start_year")));
		int scDegree = Integer.parseInt(String.valueOf(degreeInfo.get("degree")));
		int degree = ((currYear- scYear)%scDegree)+1;
//		int degree = ((currYear - 2021)%scDegree)+1;
		
		Map<String, Integer> degreeParam = new HashMap<>();
		degreeParam.put("degree", degree);
		degreeParam.put("trsd_id", Integer.parseInt(String.valueOf(degreeInfo.get("id"))));
//		degreeParam.put("trsd_id", 63);
		
//		System.out.println("degreeParam: "+degreeParam);
//		System.out.println("result: "+ tpmsTargetMapper.getSchedule(degreeParam));
		
		List<Map<String, Object>> scheduleList = tpmsTargetMapper.getSchedule(degreeParam);
		if(scheduleList.size()>0) {
			for(int i=0; i<scheduleList.size(); i++) {
				//연도 입력
				scheduleList.get(i).put("year", currYear);
			}
		}
		
//		List<Map<String, Object>> totalList = tpmsTargetMapper.getAllTempTarget();
//		System.out.println("추가 전: "+totalList.size());
//		for(int j=0; j<scheduleList.size(); j++) {
//			int chk=0;
//			for(int k=0; k<totalList.size(); k++) {
//				if(scheduleList.get(j).get("ts_id").equals(totalList.get(k).get("ts_id"))) {
//					chk +=1;
//				}
//			}
//			if(chk==0) totalList.add(scheduleList.get(j));
//		}
//	   
//		tpmsTargetMapper.deleteAllTemp();
//		
//		ObjectMapper objectMapper = new ObjectMapper();
//		List<TpmsTempTargetVO> list = objectMapper.convertValue(totalList, new TypeReference<List<TpmsTempTargetVO>>() {});	
//		
//		for (TpmsTempTargetVO tpmsTempTargetVO : list) {
//			tpmsTargetMapper.insertTempTarget(tpmsTempTargetVO);
//		}
//		
//		List<Map<String, Object>> resultList = tpmsTargetMapper.getAllTempTarget();
////		System.out.println("최종: "+rersultList);
////		resultList = dataReplace(resultList);
		
		return scheduleList;
	}
	
	@Override
	public int getMaxGroupNum(Map<String, Object> map) {
		
		// check sc_degree 
		Map<String, Object> degreeInfo = tpmsTargetMapper.getScDegree();
		
		System.out.println("degreeInfo: "+degreeInfo);
		
		int currYear = Integer.parseInt(String.valueOf(map.get("year")));
		int scYear = Integer.parseInt(String.valueOf(degreeInfo.get("start_year")));
		int scDegree = Integer.parseInt(String.valueOf(degreeInfo.get("degree")));
		int degree = ((currYear- scYear)%scDegree)+1;
//		int degree = ((currYear - 2021)%scDegree)+1;
		
		Map<String, Integer> degreeParam = new HashMap<>();
		degreeParam.put("degree", degree);
		degreeParam.put("trsd_id", Integer.parseInt(String.valueOf(degreeInfo.get("id"))));
//		degreeParam.put("trsd_id", 63);
		
		return tpmsTargetMapper.getMaxGroupNum(degreeParam);
	}
	

	@Override
	public List<Map<String, Object>> getUpdateTempList(Map<String, Object> map) {
		
		Map<String, Object> params = new HashMap<>();
		params.put("year", map.get("year"));
		if(map.get("planYear").equals("undefined")) {
			params.put("planYear", "");			
		}else params.put("planYear", map.get("planYear"));	
		params.put("rank", map.get("rank"));
		params.put("no", map.get("no"));
		params.put("tri_id", map.get("tri_id"));
		params.put("dir", map.get("dir"));
		List<Map<String, Object>> updateTempList = new ArrayList<Map<String,Object>>();
		
		boolean success = false;
		int chk=0;
		for(int i=0; i<map.size()-7; i++) {
			Map<String, Object> result = new HashMap<>();		
			int ts_id = Integer.parseInt((String) map.get(String.valueOf(i)));
			result = tpmsTargetMapper.getMatchData(ts_id);
			result.put("group_num", map.get("groupNum"));
			result.put("year", map.get("year"));
			System.out.println("result: "+result);
			
			ObjectMapper mapper = new ObjectMapper();
			TpmsTempTargetVO vo = mapper.convertValue(result, TpmsTempTargetVO.class);	
			int insert_result = tpmsTargetMapper.insertTempTarget(vo);
			System.out.println(insert_result);
			if(insert_result==1) chk +=1;
		}
		
		if(chk==map.size()-7) success = true;
		if(success) {
			
			ObjectMapper mapper2 = new ObjectMapper();
			TpmsTargetVO vo = mapper2.convertValue(params, TpmsTargetVO.class);
			
			updateTempList = tpmsTargetMapper.searchTarget(vo);		
//			updateTempList = dataReplace(updateTempList);
		}
		
		
		return updateTempList;
	}
	

	@Override
	public int deleteFromTemp(Map<String, Object> map) {
	
		int ts_id = Integer.parseInt((String)(map.get("ts_id")));
		
		return tpmsTargetMapper.deleteFromTemp(ts_id);
	}
	

	@Override
	public List<Map<String, Object>> afterDelete(Map<String, Object> map) {
		
		Map<String, Object> params = new HashMap<>();
		params.put("year", map.get("year"));
		if(map.get("planYear").equals("undefined")) {
			params.put("planYear", "");			
		}else params.put("planYear", map.get("planYear"));	
		params.put("rank", map.get("rank"));
		params.put("no", map.get("no"));
		params.put("tri_id", map.get("tri_id"));
		params.put("dir", map.get("dir"));
		
		ObjectMapper mapper = new ObjectMapper();
		TpmsTargetVO vo = mapper.convertValue(params, TpmsTargetVO.class);
		
		List<Map<String, Object>> afterDeleteList = tpmsTargetMapper.searchTarget(vo);		
//		afterDeleteList = dataReplace(afterDeleteList);
		
		return afterDeleteList;
	}
	
	
	@Override
	public List<Map<String, Object>> getAllTempTarget() {
		
		return tpmsTargetMapper.getAllTempTarget();
	}
	
	
	@Override
	public int deleteFromTarget(List<Map<String, Object>> list) {
		
		int success = 1;
		
		int currYear = (int) list.get(0).get("year");
		List<Map<String, Object>> currTargetList = tpmsTargetMapper.getOriginTarget(currYear);
		
		for(int i=0; i<currTargetList.size(); i++) {
			int chk=0;
			for(int j=0; j<list.size(); j++) {
				if((list.get(j).get("ts_id")).equals(currTargetList.get(i).get("ts_id"))) {
					chk += 1;
				}
			}
			if(chk==0) {
				System.out.println("여기 오니");
				try {
					Map<String, Integer> map = new HashMap<>();
					int ts_id = Integer.parseInt(String.valueOf(currTargetList.get(i).get("ts_id")));
					map.put("year", currYear);
					map.put("ts_id", ts_id);
					
					int result = tpmsTargetMapper.deleteFromTarget(map);
		
					if(result==0) {
						success=0;
						break;
					}
				}catch (Exception e) {
					// TODO: handle exception
					success=0;
					e.printStackTrace();
				}
			}
		}
		
		return success;
	}

	@Override
	public int saveUpdatedTarget(List<Map<String, Object>> list) {
		
		int chk=0;

		for(int i=0; i<list.size(); ++i) {
			list.get(i).remove("sm_geo");
			String lane = (String)list.get(i).get("lane");
			if(lane.equals("-")) list.get(i).replace("lane", -1);
			list.get(i).remove("link_geo");
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		List<TpmsTempTargetVO> voList = objectMapper.convertValue(list, new TypeReference<List<TpmsTempTargetVO>>() {});	
		
		for (TpmsTempTargetVO tpmsTempTargetVO : voList) {
			int result = tpmsTargetMapper.saveUpdatedTarget(tpmsTempTargetVO);
			if(result==1) chk +=1;
		}
		System.out.println("chk: "+chk);
		
		return chk;
		
//		if(chk==list.size()) {
//			return 1;
//		}else {
//			return 0;
//		}
	}

	@Override
	public int deleteAllTarget(int year) {
		
		return tpmsTargetMapper.deleteAllTarget(year);
	}
	
	
	
//	public List<Map<String, Object>> dataReplace(List<Map<String, Object>> resultList){
//		
//		if(resultList.size()>0) {
//			double extension = 0;
//			for(int i=0; i<resultList.size(); i++) {
//				int tri_id = Integer.parseInt(String.valueOf(resultList.get(i).get("tri_id"))); 
//				int dir = Integer.parseInt(String.valueOf(resultList.get(i).get("dir")));
//				int mainNum = Integer.parseInt(String.valueOf(resultList.get(i).get("main_num")));
//				int subNum = Integer.parseInt(String.valueOf(resultList.get(i).get("sub_num")));
//				String nodeType = (String) resultList.get(i).get("node_type");
//				String name = (String) resultList.get(i).get("name");
//				
////				//연도 입력
////				resultList.get(i).put("target_year", currYear);
//				
//				// 노선명 없을때 -로 변환
//				if(name.equals("")) resultList.get(i).put("name", "-");
//				
//				// 차선수가 전부 null 이므로 임의로 "-" 입력
//				resultList.get(i).put("lane", "-");
//				// 행선 변환 (0=상행, 1=하행)
//				if(dir==0) {
//					resultList.get(i).put("rev_dir", "상행");
//				}else if(dir==1) {
//					resultList.get(i).put("rev_dir", "하행");
//				}
//				// 노드타입 변환
//				if(nodeType !=null) {
//					switch (nodeType) {
//					case "101" : 
//						resultList.get(i).replace("node_type", "교차로");
//						break;
//					case "102" :
//						resultList.get(i).replace("node_type", "도로시·종점");
//						break;
//					case "103" :
//						resultList.get(i).replace("node_type", "속성변화점");
//						break;
//					case "104" :
//						resultList.get(i).replace("node_type", "도로시설물");
//						break;
//					case "105" :
//						resultList.get(i).replace("node_type", "행정경계");
//						break;
//					case "106" :
//						resultList.get(i).replace("node_type", "연결로접속부");
//						break;
//					case "108" :
//						resultList.get(i).replace("node_type", "IC/JC");
//						break;
//					}
//				}
//				// 연장 누적 계산	
//				if(i==0) {
//					extension = (double) resultList.get(i).get("sm_ext");
//					resultList.get(i).put("ext_sum", extension);
//				}else if(i>0) {
//					int prev_triId = Integer.parseInt(String.valueOf(resultList.get(i-1).get("tri_id"))); 
//					int prev_dir = Integer.parseInt(String.valueOf(resultList.get(i-1).get("dir")));
//					int prev_mainNum = Integer.parseInt(String.valueOf(resultList.get(i-1).get("main_num")));
//					int prev_subNum = Integer.parseInt(String.valueOf(resultList.get(i-1).get("sub_num")));
//					if(tri_id==prev_triId && dir==prev_dir && mainNum==prev_mainNum && subNum==prev_subNum) {
//						extension = extension + (double) resultList.get(i).get("sm_ext");
//						resultList.get(i).put("ext_sum", extension);
//					}else {
//						extension = (double) resultList.get(i).get("sm_ext");
//						resultList.get(i).put("ext_sum", extension);
//					}
//				}
//			}
//		}
//		
//		return resultList;
//	}


}
