package egovframework.com.tpms.target.web;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.cmm.service.TpmsCmmService;
import egovframework.com.tpms.rsm.service.TpmsDetailTargetService;
import egovframework.com.tpms.target.service.TpmsTargetService;


@RestController
public class TpmsTargetController {
	
	@Resource(name="tpmsTargetService")
	private TpmsTargetService tpmsTargetService;
	
	@Resource(name="tpmsCmmService")
	private TpmsCmmService tpmsCmmService;
	
	@Resource(name="tpmsDetailTargetService")
	private TpmsDetailTargetService tpmsDetailTargetService;
	
	
	
	@RequestMapping(value="/tpms/getMasterForTarget.do", method=RequestMethod.POST)
	public ModelAndView getMasterForTarget(){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsTargetService.getMasterForTarget());	
		
		return mav;
	}
	
	
	@RequestMapping(value="/tpms/checkTargetList.do", method=RequestMethod.POST)
	public ModelAndView checkTargetList(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsTargetService.checkTargetList(map));	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getCurrentYearTarget.do", method=RequestMethod.POST)
	public ModelAndView getCurrentYearTarget(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		List<Map<String, Object>> list = tpmsTargetService.getCurrentYearTarget(map);
		if(list.size()>0) list = tpmsCmmService.dataReplace(list);
		mav.addObject("res", list);	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getYearList.do", method=RequestMethod.POST)
	public ModelAndView getYearList(){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsTargetService.getYearList());
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getAllTarget.do", method=RequestMethod.POST)
	public ModelAndView getAllTarget(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		if(map.get("year").equals("all")) {
			map.replace("year", "1");
		}
//		System.out.println("searchYear: "+map.get("year"));
		
		List<Map<String, Object>> allTargetList = tpmsTargetService.getAllTarget(map);
//		System.out.println("allTargetList: "+allTargetList);
		
		if(allTargetList.size()>0) {
			Map<String, Object> params = new HashMap<String, Object>();
			for(int i=0; i<allTargetList.size(); ++i) {
				String key = (String) allTargetList.get(i).get("target_id");
				List<Object> tempArr = new ArrayList<Object>();
//				List<Map<String, Object>> tempArr = new ArrayList<Map<String, Object>>();
				if(params.containsKey(key)){
					tempArr = (List) params.get(key);
				}
				tempArr.add(allTargetList.get(i));
				params.put(key, tempArr);
			}
			
			List<String> keyList = new ArrayList<String>(params.keySet());
			System.out.println("keyList: "+keyList);
			ArrayList<HashMap<String, Object>> list = null;
			List<Map<String, Object>> recordList = new ArrayList<Map<String,Object>>();
			for (String key : keyList) {
				Map<String, Object> result = new HashMap<String, Object>();
				list = (ArrayList<HashMap<String, Object>>) params.get(key);
				System.out.println("list 1: "+list.get(0));
				result.put("year", list.get(0).get("year"));
				result.put("target_id", list.get(0).get("target_id"));
				result.put("rank", list.get(0).get("rank"));
				result.put("no", list.get(0).get("no"));
				result.put("name", list.get(0).get("name"));
				if(list.get(0).get("name").equals("")) {
					result.replace("name", "-");
				}
				result.put("main_num", list.get(0).get("main_num"));
				result.put("sub_num", list.get(0).get("sub_num"));
				result.put("dir", list.get(0).get("dir"));
				result.put("rev_dir", list.get(0).get("rev_dir"));
				result.put("s_ts_id", list.get(0).get("ts_id"));
//				result.put("lanes", list.get(0).get("lane"));
				
				HashMap<String, Integer> paramMap = new HashMap<String, Integer>();
				paramMap.put("tsm_id", Integer.parseInt((list.get(0).get("tsm_id")).toString()));
				paramMap.put("order",  Integer.parseInt((list.get(0).get("order")).toString()));
				
				Map<String, Long> extParam = new HashMap<String, Long>();
				System.out.println("ts_id: "+list.get(0).get("ts_id"));
				extParam.put("s_ts_id", Long.valueOf(String.valueOf(list.get(0).get("ts_id"))));
				// 시점
				Map<String, Object> s_node = new HashMap<String, Object>();
				Long s_ts_node_id=null;
				if(paramMap.get("order")!=1) {
					s_node = tpmsDetailTargetService.getNode_OrderNEQ1(paramMap);
					s_ts_node_id = (Long)s_node.get("id");
					if(s_node.size()==0) {
						s_node = tpmsDetailTargetService.getNode_OrderEQ1(paramMap);
						s_ts_node_id = (Long)s_node.get("ts_id");
					}
				}else {
					s_node = tpmsDetailTargetService.getNode_OrderEQ1(paramMap);	
					s_ts_node_id = (Long)s_node.get("ts_id");
				}
				extParam.put("s_ts_node_id", s_ts_node_id);
				String s_ext = tpmsDetailTargetService.getStartExt(extParam);
				if(s_ext==null) s_ext="0";
				result.put("s_ext", s_ext);  // 시점 노드부터 몇 미터 떨어진 지점인지
				result.put("s_node_name", s_node.get("node_name"));  // 시점 노드 이름
				result.put("s_node_type", s_node.get("node_type"));  // 시점 노드 타입
				result.put("spos", s_node.get("node_name")+"+"+String.format("%.2f", (Double.parseDouble(s_ext)/1000))+"km");
				
				// 종점
				Map<String, Object> e_node = new HashMap<String, Object>();
				if(list.size()>1) {
					paramMap.replace("tsm_id", Integer.parseInt((list.get(list.size()-1).get("tsm_id")).toString()));
					paramMap.replace("order", Integer.parseInt((list.get(list.size()-1).get("order")).toString()));
					result.put("e_ts_id", list.get(list.size()-1).get("ts_id"));
					extParam.put("e_ts_id", Long.valueOf(String.valueOf(list.get(list.size()-1).get("ts_id"))));
				}else {
					result.put("e_ts_id", list.get(0).get("ts_id"));
					extParam.put("e_ts_id", Long.valueOf(String.valueOf(list.get(0).get("ts_id"))));
				}
				Long e_ts_node_id=null;
				if(paramMap.get("order")!=1) {
					e_node = tpmsDetailTargetService.getNode_OrderNEQ1(paramMap);
					e_ts_node_id = (Long)e_node.get("id");
					if(e_node.size()==0) {
						e_node = tpmsDetailTargetService.getNode_OrderEQ1(paramMap);
						e_ts_node_id = (Long)e_node.get("ts_id");
					}
				}else {
					e_node = tpmsDetailTargetService.getNode_OrderEQ1(paramMap);
					e_ts_node_id = (Long)e_node.get("ts_id");
				}
				extParam.put("e_ts_node_id", e_ts_node_id);
				String e_ext = tpmsDetailTargetService.getEndExt(extParam);
				if(e_ext==null) e_ext="0";
				result.put("e_ext", e_ext);  // 종점 노드부터 몇 미터 떨어진 지점인지
				result.put("e_node_name", e_node.get("node_name"));  // 종점 노드 이름
				result.put("e_node_type", e_node.get("node_type"));  // 종점 노드 타입
				result.put("epos", e_node.get("node_name")+"+"+String.format("%.2f", (Double.parseDouble(e_ext)/1000))+"km");
				
				// 총연장 길이
				double total_ext = 0;
				for(int k=0; k<list.size(); ++k) {
					total_ext +=(double)list.get(k).get("sm_ext");
				}
				result.put("total_ext", total_ext);
				
				recordList.add(result);
			}
			mav.addObject("res", recordList);
		}else {
			mav.addObject("res", null);
		}
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/searchRecord.do", method=RequestMethod.POST)
	public ModelAndView searchRecord(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		List<Map<String, Object>> list = tpmsTargetService.searchRecord(map);
		if(list.size()>0) list = tpmsCmmService.dataReplace(list);
		mav.addObject("res", list);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/searchTarget.do", method=RequestMethod.POST)
	public ModelAndView searchTarget(@RequestParam HashMap<String, Object> map){

		System.out.println("searchTarget_map: "+map);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		List<Map<String, Object>> list = tpmsTargetService.searchTarget(map);
		if(list.size()>0) list = tpmsCmmService.dataReplace(list);
		mav.addObject("res", list);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getSchedule.do", method=RequestMethod.POST)
	public ModelAndView getSchedule(@RequestParam HashMap<String, Object> map){

		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		List<Map<String, Object>> list = tpmsTargetService.getSchedule(map);
		if(list.size()>0) list = tpmsCmmService.dataReplace(list);
		mav.addObject("res", list);
		mav.addObject("maxNum",tpmsTargetService.getMaxGroupNum(map));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getUpdateTempList.do", method=RequestMethod.POST)
	public ModelAndView getUpdateTempList(@RequestParam HashMap<String, Object> map){

		System.out.println("map: "+ map);
		
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		List<Map<String, Object>> list = tpmsTargetService.getUpdateTempList(map);
		if(list.size()>0) list = tpmsCmmService.dataReplace(list);
		mav.addObject("res", list);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/deleteFromTemp.do", method=RequestMethod.POST)
	public ModelAndView deleteFromTemp(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsTargetService.deleteFromTemp(map));		
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/afterDelete.do", method=RequestMethod.POST)
	public ModelAndView afterDelete(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		List<Map<String, Object>> list = tpmsTargetService.afterDelete(map);
		if(list.size()>0) list = tpmsCmmService.dataReplace(list);
		mav.addObject("res", list);
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/saveTargetList.do", method=RequestMethod.POST)
	public ModelAndView saveTargetList(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		int year = Integer.parseInt(String.valueOf(map.get("year")));
		int cnt = Integer.parseInt(String.valueOf(map.get("cnt")));
		int del_result = tpmsTargetService.deleteAllTarget(year);
		
	
		
		if(del_result==cnt) {
			String valueStr = null;
			ObjectMapper mapper = new ObjectMapper();
			try {
				valueStr = java.net.URLDecoder.decode((String)map.get("list"), "UTF-8");
				List<Map<String, Object>> targetList = mapper.readValue(valueStr, new TypeReference<ArrayList<HashMap<String, Object>>>() {});
				List<Map<String, Object>> restList = new ArrayList<Map<String,Object>>();
				for(int i=0; i<targetList.size();++i) {
					if(targetList.get(i).containsKey("target_id")) {
						restList.add(targetList.get(i));
					}
				}
				targetList = tpmsCmmService.getTargetId("T", targetList);	// 타겟ID가 없는 리스트만 생성해서 리턴해줌
				for(int i=0; i<restList.size(); ++i) {
					targetList.add(restList.get(i));
				}
				System.out.println("target_size: "+ targetList.size());
				int save_result = tpmsTargetService.saveUpdatedTarget(targetList);
				if(save_result == targetList.size()) {
					mav.addObject("res", "success");
				}else {
					mav.addObject("res", "fail");
				}			
			
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
//		String success = "false";
//		
//		List<Map<String, Object>> allTempTargetList = tpmsTargetService.getAllTempTarget();
//		int result = tpmsTargetService.deleteFromTarget(allTempTargetList);
//		
//		if(result==1) {
//			allTempTargetList = tpmsCmmService.getTargetId("T", allTempTargetList);			
//			int result2 = tpmsTargetService.saveUpdatedTarget(allTempTargetList);
//			if(result2==1) success="true";
//		}
		
		
//		for(int i=0; i<allTempTargetList.size(); i++) {
//			System.out.println(i+"번째 Target_ID: "+ allTempTargetList.get(i));
//		}
		
//		mav.addObject("res", success);
		
		return mav;
	}
	
}
