package egovframework.com.tpms.node.web;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import egovframework.com.tpms.node.service.TpmsNodeService;
import egovframework.com.tpms.node.service.TpmsNodeVO;

@RestController
public class TpmsNodeController {
	
	@Resource(name="tpmsNodeService")
	private TpmsNodeService tpmsNodeService;
	
	@RequestMapping(value="/tpms/selectNodeList.do", method=RequestMethod.POST)
	public ModelAndView selectNodeList() {
		ArrayList<TpmsNodeVO> list = tpmsNodeService.selectNodeList();
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", list);
		
		return mav;
	}
}
