package egovframework.com.tpms.node.service.impl;

import java.util.ArrayList;

import egovframework.com.tpms.node.service.TpmsNodeVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsNodeMapper")
public interface TpmsNodeMapper {
	//	<select id="selectNodeList" resultType="tpmsNodeVO">
	public ArrayList<TpmsNodeVO> selectNodeList();
}
