package egovframework.com.tpms.node.service;

import java.util.ArrayList;

public interface TpmsNodeService {
	//	<select id="selectNodeList" resultType="tpmsNodeVO">
	public ArrayList<TpmsNodeVO> selectNodeList();
}
