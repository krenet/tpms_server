package egovframework.com.tpms.node.service.impl;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import egovframework.com.tpms.node.service.TpmsNodeService;
import egovframework.com.tpms.node.service.TpmsNodeVO;

@Service("tpmsNodeService")
@Transactional
public class TpmsNodeImpl implements TpmsNodeService{

	@Resource(name="tpmsNodeMapper")
	private TpmsNodeMapper tpmsNodeMapper;

	@Override
	public ArrayList<TpmsNodeVO> selectNodeList() {
		// TODO Auto-generated method stub
		return tpmsNodeMapper.selectNodeList();
	}
}
