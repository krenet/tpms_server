package egovframework.com.tpms.repair.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import egovframework.com.tpms.repair.service.TpmsRepairRecordService;

@Service("tpmsRepairRecordService")
@Transactional
public class TpmsRepairRecordImpl implements TpmsRepairRecordService {
	
	@Resource(name="tpmsRepairRecordMapper")
	private TpmsRepairRecordMapper tpmsRepairRecordMapper;

	@Override
	public List<Map<String, Object>> getAllRepairRecordList() {
		
		List<Map<String, Object>> list = tpmsRepairRecordMapper.getAllRepairRecordList();
		if(list.size()>0) list = replacDate(list);
		
		return list;
	}

	@Override
	public List<Map<String, Object>> getSearchMaster(Map<String, Object> map) {
		String str = (String) map.get("no");
		List<String> noList = new ArrayList<String>(Arrays.asList(str.split(",")));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("noList", noList);
		
		return tpmsRepairRecordMapper.getSearchMaster(param);
	}
	
	public List<Map<String, Object>> replacDate(List<Map<String, Object>> list){
		
		for(int i=0; i<list.size();++i) {
			int dir = (int) list.get(i).get("dir");
			if(dir==0) {
				list.get(i).put("roadDir", "상행");
			}else if(dir==1) {
				list.get(i).put("roadDir", "하행");
			}
			double total_ext = (double) list.get(i).get("total_ext");
			list.get(i).put("totalExt", String.format("%.2f", total_ext/1000));
			String name = (String) list.get(i).get("name");
			if(name.equals("")) list.get(i).replace("name", "-");
			String date = (String) list.get(i).get("date");
			list.get(i).put("year", date.split("-")[0]);
		}
		
		return list;
	}

}
