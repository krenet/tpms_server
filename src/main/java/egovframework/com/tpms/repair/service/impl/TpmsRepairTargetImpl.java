package egovframework.com.tpms.repair.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import egovframework.com.tpms.repair.service.TpmsEquip2VO;
import egovframework.com.tpms.repair.service.TpmsRepairTargetService;
import egovframework.com.tpms.repair.service.TpmsRepairTargetVO;

@Service("tpmsRepairTargetService")
@Transactional
public class TpmsRepairTargetImpl implements TpmsRepairTargetService {
	
	@Resource(name="tpmsRepairTargetMapper")
	private TpmsRepairTargetMapper tpmsRepairTargetMapper; 
	


//	@Override
//	public List<Map<String, Object>> getEquip2(String equip2Type) {
//
//		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
//		String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
////		ObjectMapper mapper = new ObjectMapper();
////		TpmsEquip2VO vo = mapper.convertValue(map, TpmsEquip2VO.class);
//		if(equip2Type.equals("crack")) {
//			list = tpmsRepairTargetMapper.getCrack(year);
//			if(list.size()>0) {
//				for(int i=0; i<list.size(); ++i) {
//					double cr = (double) list.get(i).get("cr");
//					list.get(i).put("crack", String.format("%.2f", cr));
//				}
//			}
//		}else if(equip2Type.equals("gpr")) {
//			
//		}else if(equip2Type.equals("hwd")) {
//			
//		}
//		
//		
//		return list;
//	}


	@Override
	public List<Map<String, Object>> getCurrYearMaint(int year) {

		List<Map<String, Object>> list = tpmsRepairTargetMapper.getCurrYearMaint(year);
		if(list.size()>0) {
			System.out.println("현재 list: "+list);
			list = replacDate(list);
		}
		
		return list;
	}


	@Override
	public List<Map<String, Object>> getAllMaint() {
		
		List<Map<String, Object>> list = tpmsRepairTargetMapper.getAllMaint();
		if(list.size()>0) {
			list = replacDate(list);
		}
		
		return list;
	}
	
	@Override
	public List<Map<String, Object>> getCurrMaintTs_Id(Map<String, Object> param) {
		
		return tpmsRepairTargetMapper.getCurrMaintTs_Id(param);
	}



	@Override
	public int saveNewRepair(Map<String, Object> map) {
		
		int result = 0;
		String valueStr = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			valueStr = java.net.URLDecoder.decode((String)map.get("list"), "UTF-8");
			ArrayList<HashMap<String, Object>> repairData = mapper.readValue(valueStr, new TypeReference<ArrayList<HashMap<String, Object>>>() {});
			System.out.println("reapirData: "+repairData);
			map.put("repair_target_id", repairData.get(0).get("repair_target_id"));
			map.put("s_t10m_id", repairData.get(0).get("s_t10m_id"));
			map.put("e_t10m_id", repairData.get(0).get("e_t10m_id"));
			map.put("s_ts_id", repairData.get(0).get("s_ts_id"));
			map.put("e_ts_id", repairData.get(0).get("e_ts_id"));
			map.put("year", repairData.get(0).get("year"));
			map.put("in_code", repairData.get(0).get("in_code"));
			map.put("dir", repairData.get(0).get("dir"));
//			map.put("lane", repairData.get(0).get("lane"));
			map.put("lane", 1);  // 차선 정보 예시
			map.put("tsm_id", repairData.get(0).get("tsm_id"));
			map.put("total_ext", repairData.get(0).get("total_ext"));
			map.put("s_node_name", repairData.get(0).get("s_node_name"));
			map.put("e_node_name", repairData.get(0).get("e_node_name"));
			map.put("s_ts_node_id", repairData.get(0).get("s_ts_node_id"));
			map.put("e_ts_node_id", repairData.get(0).get("e_ts_node_id"));
			map.put("tri_id", repairData.get(0).get("tri_id"));
			map.put("spos", repairData.get(0).get("spos"));
			map.put("epos", repairData.get(0).get("epos"));
			map.remove("list");
			
			ObjectMapper voMapper = new ObjectMapper();
			TpmsRepairTargetVO vo = voMapper.convertValue(map, TpmsRepairTargetVO.class);
			result = tpmsRepairTargetMapper.saveNewRepair(vo);
//			System.out.println("추가 후 map: "+map);
		
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	@Override
	public List<Map<String, Object>> getSelectEquip1Data(Map<String, Object> map) {
		String str = (String) map.get("target_id");
		List<String> targetIdList = new ArrayList<String>(Arrays.asList(str.split(",")));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("targetIdList", targetIdList);
 		return tpmsRepairTargetMapper.getSelectEquip1Data(param);
	}
	
	@Override
	public List<Map<String, Object>> getSelectCrackData(Map<String, Object> map, Object formula) {
		
		String str = (String) map.get("ts_id");
		List<String> tsList = new ArrayList<String>(Arrays.asList(str.split(",")));
		List<Long> tsIdList = new ArrayList<Long>();
		for(String ts : tsList) {
			tsIdList.add(Long.parseLong(ts));
		}
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tsIdList", tsIdList);
		System.out.println("crack_param: "+param);

		List<Map<String, Object>> crackList = tpmsRepairTargetMapper.getSelectCrackData(param);
		
		Map<String, Object> formulahMap = (Map<String, Object>) formula;
		String pav_idx = (String)formulahMap.get("formula");
				
		int num=1;
		for(int i=0; i<crackList.size()-1; ++i) {
			String pre_id= (String) crackList.get(i).get("detail_target_id");
			String next_id = (String) crackList.get(i+1).get("detail_target_id");
			if(i==0) crackList.get(0).replace("extension", 10);
			if(next_id.equals(pre_id)) {
				num +=1;
				crackList.get(i+1).replace("extension", (num)*10);
			}else{
				num = 1;
				crackList.get(i+1).replace("extension", (num)*10);
			}
		}
		
		for(int i=0; i<crackList.size(); ++i) {
			crackList.get(i).put("crack", String.format("%.2f", crackList.get(i).get("cr")));
			crackList.get(i).put("pav_idx", String.format("%.2f", crackList.get(i).get(pav_idx)));
			crackList.get(i).put("pav_idx_origin", crackList.get(i).get(pav_idx));
		}

		return crackList;
		
	}
	

	@Override
	public List<Map<String, Object>> getSelectGprData(Map<String, Object> map) {
		String str = (String) map.get("ts_id");
		List<String> tsList = new ArrayList<String>(Arrays.asList(str.split(",")));
		List<Long> tsIdList = new ArrayList<Long>();
		for(String ts : tsList) {
			tsIdList.add(Long.parseLong(ts));
		}
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tsIdList", tsIdList);
		
		return tpmsRepairTargetMapper.getSelectGprData(param);
	}
	
	
	@Override
	public List<Map<String, Object>> getSelectHwdData(Map<String, Object> map) {
		String str = (String) map.get("ts_id");
		List<String> tsList = new ArrayList<String>(Arrays.asList(str.split(",")));
		List<Long> tsIdList = new ArrayList<Long>();
		for(String ts : tsList) {
			tsIdList.add(Long.parseLong(ts));
		}
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tsIdList", tsIdList);
		
		return tpmsRepairTargetMapper.getSelectHwdData(param);
	}
	
	
	@Override
	public int deleteRepairData(Map<String, Object> map) {
		String str = (String) map.get("repair_target_id");
		List<String> repairIdList = new ArrayList<String>(Arrays.asList(str.split(",")));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("repairIdList", repairIdList);
		
		int result = tpmsRepairTargetMapper.deleteRepairData(param);
		
		if(result == repairIdList.size()) {
			return 1;
		}else {
			return 0;
		}
		
	}

	
	
	public List<Map<String, Object>> replacDate(List<Map<String, Object>> list){
		
		for(int i=0; i<list.size();++i) {
			int dir = (int) list.get(i).get("dir");
			if(dir==0) {
				list.get(i).put("roadDir", "상행");
			}else if(dir==1) {
				list.get(i).put("roadDir", "하행");
			}
			double total_ext = (double) list.get(i).get("total_ext");
			list.get(i).put("totalExt", String.format("%.2f", total_ext/1000));
			String name = (String) list.get(i).get("name");
			if(name.equals("")) list.get(i).replace("name", "-");
		}
		
		return list;
	}

}
