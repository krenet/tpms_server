package egovframework.com.tpms.repair.service.impl;

import java.util.List;
import java.util.Map;

import egovframework.com.tpms.repair.service.TpmsEquip2VO;
import egovframework.com.tpms.repair.service.TpmsRepairTargetVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsRepairTargetMapper")
public interface TpmsRepairTargetMapper {
	
//	public List<Map<String, Object>> getCrack(String year);
	public List<Map<String, Object>> getCurrYearMaint(int year);
	public List<Map<String, Object>> getAllMaint();
	public List<Map<String, Object>> getCurrMaintTs_Id(Map<String, Object> param);
	public int saveNewRepair(TpmsRepairTargetVO vo);
	public List<Map<String, Object>> getSelectEquip1Data(Map<String, Object> param);
	public List<Map<String, Object>> getSelectCrackData(Map<String, Object> param);
	public List<Map<String, Object>> getSelectGprData(Map<String, Object> param);
	public List<Map<String, Object>> getSelectHwdData(Map<String, Object> param);
	public int deleteRepairData(Map<String, Object> param);
}
