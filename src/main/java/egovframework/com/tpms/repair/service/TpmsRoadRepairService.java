package egovframework.com.tpms.repair.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public interface TpmsRoadRepairService {
	public ArrayList<String> selectRepairTargetIdList();
	public Map<String, Object> selectTargetInfoByTargetId(HashMap<String, Object> param);
	public int insertRepairData(HashMap<String, Object> param);
	public ArrayList<HashMap<String, Object>> getRoadRepairPriorityList(String formula);
}
