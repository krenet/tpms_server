package egovframework.com.tpms.repair.service;

import java.util.List;
import java.util.Map;

public interface TpmsRepairTargetService {
	
//	public List<Map<String, Object>> getEquip2(String equip2Type);
	public List<Map<String, Object>> getCurrYearMaint(int year);
	public List<Map<String, Object>> getAllMaint();
	public List<Map<String, Object>> getCurrMaintTs_Id(Map<String, Object> param);
	public int saveNewRepair(Map<String, Object> map);
	public List<Map<String, Object>> getSelectEquip1Data(Map<String, Object> map);
	public List<Map<String, Object>> getSelectCrackData(Map<String, Object> map, Object formula);
	public List<Map<String, Object>> getSelectGprData(Map<String, Object> map);
	public List<Map<String, Object>> getSelectHwdData(Map<String, Object> map);
	public int deleteRepairData(Map<String, Object> map);

}
