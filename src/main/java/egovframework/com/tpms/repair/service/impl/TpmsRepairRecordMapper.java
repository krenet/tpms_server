package egovframework.com.tpms.repair.service.impl;

import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper ("tpmsRepairRecordMapper")
public interface TpmsRepairRecordMapper {

	public List<Map<String, Object>> getAllRepairRecordList();
	public List<Map<String, Object>> getSearchMaster(Map<String, Object> param);
}
