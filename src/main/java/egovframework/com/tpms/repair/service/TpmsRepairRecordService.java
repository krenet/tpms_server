package egovframework.com.tpms.repair.service;

import java.util.List;
import java.util.Map;

public interface TpmsRepairRecordService {

	public List<Map<String, Object>> getAllRepairRecordList();
	public List<Map<String, Object>> getSearchMaster(Map<String, Object> map);

}
