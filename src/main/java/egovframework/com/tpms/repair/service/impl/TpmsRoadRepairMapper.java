package egovframework.com.tpms.repair.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import egovframework.com.tpms.repair.service.TpmsRoadRepairVO;
import egovframework.com.tpms.rsm.service.TpmsCrackViewVO;
import egovframework.com.tpms.rsm.service.TpmsPesVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("tpmsRoadRepairMapper")
public interface TpmsRoadRepairMapper {
	//selectRepairTargetIdList
	public ArrayList<String> selectRepairTargetIdList();
	//<select id="selectTargetInfoByTargetId" parameterType="String" resultType="map">
	public Map<String, Object> selectTargetInfoByTargetId(String target);
	//<select id="selectRepairHistoryVersion" parameterType="String" resultType="int">
	public int selectRepairHistoryVersion(String repair_target_id);
	//	<select id="getRepairHistoryVersionCheck" parameterType="String" resultType="int">
	public int getRepairHistoryVersionCheck(String status);
	//<insert id="insertRepairHistoryNewVersion" parameterType="String">
	public int insertRepairHistoryNewVersion(String status);
	//<insert id="insertRepairHistoryNewWithVersion" parameterType="int">
	public int insertRepairHistoryNewWithVersion(int version);
	//<insert id="insertRepairCompleteData" parameterType="tpmsRoadRepairVO">
	public int insertRepairCompleteData(TpmsRoadRepairVO vo);
	//<select id="selectLastIndexForRepair" resultType="tpmsCrackViewVO">
	public ArrayList<TpmsCrackViewVO> selectLastIndexForRepair();
	//<select id="getNodeDataForRepairReport" parameterType="tpmsCrackViewVO" resultType="map">
	public Map<String, Object> getNodeDataForRepairReport(TpmsCrackViewVO vo);
	//<select id="selectEquip1DataForReport" parameterType="map" resultType="tpmsPesVO">
	public ArrayList<TpmsPesVO> selectEquip1DataForReport(HashMap<String, Object> map);
}
