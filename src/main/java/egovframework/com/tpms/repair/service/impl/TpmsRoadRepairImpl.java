package egovframework.com.tpms.repair.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import egovframework.com.tpms.repair.service.TpmsRoadRepairService;
import egovframework.com.tpms.repair.service.TpmsRoadRepairVO;
import egovframework.com.tpms.roadinfo.service.TpmsRoadInfoVO;
import egovframework.com.tpms.roadinfo.service.impl.TpmsRoadInfoMapper;
import egovframework.com.tpms.rsm.service.TpmsCrackViewVO;
import egovframework.com.tpms.rsm.service.TpmsPesVO;

@Service("tpmsRoadRepairService")
@Transactional
public class TpmsRoadRepairImpl implements TpmsRoadRepairService {

	@Resource(name="tpmsRoadRepairMapper")
	private TpmsRoadRepairMapper tpmsRoadRepairMapper;

	@Resource(name="tpmsRoadInfoMapper")
	private TpmsRoadInfoMapper tpmsRoadInfoMapper;
	
	@Override
	public ArrayList<String> selectRepairTargetIdList() {
		// TODO Auto-generated method stub
		return tpmsRoadRepairMapper.selectRepairTargetIdList();
	}

	@Override
	public Map<String, Object> selectTargetInfoByTargetId(HashMap<String, Object> param) {
		// TODO Auto-generated method stub
		return tpmsRoadRepairMapper.selectTargetInfoByTargetId((String)param.get("target"));
	}

	@Override
	public int insertRepairData(HashMap<String, Object> param) {
		// TODO Auto-generated method stub
		
		ObjectMapper mapper = new ObjectMapper();
		TpmsRoadRepairVO vo = mapper.convertValue(param, TpmsRoadRepairVO.class);
		
		if(vo.getPave_meterial().equals("undefined")) vo.setPave_meterial(null);
		
		int version = tpmsRoadRepairMapper.selectRepairHistoryVersion(vo.getRepair_target_id());
		String status = "";
		if(version==0) status = "0302"; //신규
		else status = "0303"; //수정
		
		version = tpmsRoadRepairMapper.getRepairHistoryVersionCheck(status);
		if(version==0) tpmsRoadRepairMapper.insertRepairHistoryNewVersion(status);
		else tpmsRoadRepairMapper.insertRepairHistoryNewWithVersion(version+1);
		version = tpmsRoadRepairMapper.getRepairHistoryVersionCheck(status);
		vo.setVersion(version);

		return tpmsRoadRepairMapper.insertRepairCompleteData(vo);
	}

	@Override
	public ArrayList<HashMap<String, Object>> getRoadRepairPriorityList(String formula) {
		// TODO Auto-generated method stub
		ArrayList<TpmsCrackViewVO> viewVO = tpmsRoadRepairMapper.selectLastIndexForRepair();
		// grouping list
		HashMap<String, ArrayList<TpmsCrackViewVO>> groupList = new HashMap<>();
		
		ArrayList<TpmsCrackViewVO> temp = null;
		for (TpmsCrackViewVO tpmsCrackViewVO : viewVO) {
			String key = ""+tpmsCrackViewVO.getTri_id()+tpmsCrackViewVO.getMain_num()+tpmsCrackViewVO.getSub_num()+tpmsCrackViewVO.getDir();
			temp = new ArrayList<>();
			if(groupList.containsKey(key)) {
				temp = groupList.get(key);
			}
			temp.add(tpmsCrackViewVO);
			groupList.put(key, temp);
		}
		
		Set set = groupList.keySet(); 
		Iterator iterator = set.iterator();
		
		double standard = 0.0;
		switch (formula) {
		case "mpci":
			standard = 4.0;
			break;
		case "spi":
			standard = 7.0;
			break;
		case "nhpci":
			standard = 5.0;
			break;
		}
		
		// 10m 단위 보수 대상 추천 구간
		ArrayList<ArrayList<TpmsCrackViewVO>> repairList = new ArrayList<>();
		ArrayList<TpmsCrackViewVO> tempRepairList = null;
		while(iterator.hasNext()){
			
			String key = (String)iterator.next();
			tempRepairList = new ArrayList<>();
			temp = groupList.get(key);
			int criticalCnt = 0;
			double sumInx = 0.0;
			for(int i=0;i<temp.size();++i) {
				
				tempRepairList.add(temp.get(i));
				if(tempRepairList.size()>9) {
					criticalCnt = 0;
					sumInx = 0.0;
					for (int j=tempRepairList.size()-1; j>=tempRepairList.size()-10; --j) {
						switch (formula) {
						case "mpci":
							sumInx += tempRepairList.get(j).getMpci();
							if(tempRepairList.get(j).getMpci()<=standard) ++criticalCnt;
							break;
						case "spi":
							sumInx += tempRepairList.get(j).getSpi();
							if(tempRepairList.get(j).getSpi()<=standard) ++criticalCnt;
							break;
						case "nhpci":
							sumInx += tempRepairList.get(j).getNhpci();
							if(tempRepairList.get(j).getNhpci()<=standard) ++criticalCnt;
							break;
						}
						
					}
					double avgInx = sumInx / 10.0; 
					if(avgInx<=standard && criticalCnt>=7) {
						
					}else {
						if(tempRepairList.size()>=11) {
							tempRepairList.remove(tempRepairList.size()-1);
							tempRepairList.trimToSize();
							repairList.add(tempRepairList);
							tempRepairList = new ArrayList<>();
						}else {
							tempRepairList.remove(0);
							tempRepairList.trimToSize();
						}
					}
				}
			}
			if(tempRepairList.size()>=10) repairList.add(tempRepairList);
		}

		// 순위 계산
		// avg index 계산
		HashMap<Integer, Double> indexMap = new HashMap<>();
		
		double sumIndex = 0.0;
		double avgIndex = 0.0;
		for(int i=0; i<repairList.size(); ++i) {
			temp = repairList.get(i);
			sumIndex = 0.0;
			for (TpmsCrackViewVO tpmsCrackViewVO : temp) {
				switch (formula) {
				case "mpci":
					sumIndex += tpmsCrackViewVO.getMpci();
					break;
				case "spi":
					sumIndex += tpmsCrackViewVO.getSpi();
					break;
				case "nhpci":
					sumIndex += tpmsCrackViewVO.getNhpci();
					break;
				}
				
			}
			avgIndex = sumIndex / (double)temp.size();
			indexMap.put(i, avgIndex);
		}
		
		List<Entry<Integer, Double>> _list = new ArrayList<Entry<Integer, Double>>(indexMap.entrySet());
		Collections.sort(_list, new Comparator<Entry<Integer, Double>>(){
			@Override
			public int compare(Entry<Integer, Double> o1, Entry<Integer, Double> o2) {
				// TODO Auto-generated method stub
				return o1.getValue().compareTo(o2.getValue());
			}
			
		});
		
		// 순위 매겨진 10m 보수 대상 구간 그룹 리스트
		ArrayList<ArrayList<TpmsCrackViewVO>> orderRepairList = new ArrayList<>();
		
		for(int i=0;i<_list.size(); ++i) {
			orderRepairList.add(repairList.get(_list.get(i).getKey()));
		}
		
		
		ArrayList<HashMap<String, Object>> result = new ArrayList<>();
		int priority = 1;
		for (ArrayList<TpmsCrackViewVO> arrayList : orderRepairList) {
			result.add(getRepairReportData(arrayList, priority, formula));
			++priority;
		}
		
		return result;
		
	}
	
	public HashMap<String, Object> getRepairReportData(ArrayList<TpmsCrackViewVO> list, int priority, String formula){
		HashMap<String, Object> res = new HashMap<>();
		
		Map<String, Object> startNode = tpmsRoadRepairMapper.getNodeDataForRepairReport(list.get(0));
		Map<String, Object> endNode = tpmsRoadRepairMapper.getNodeDataForRepairReport(list.get(list.size()-1));
		HashMap<String, Object> map = new HashMap<>();
		int start_ext = list.get(0).getExtension();
		int end_ext = list.get(list.size()-1).getExtension();
		map.put("target_id", list.get(0).getTarget_id());
		map.put("start_ext", start_ext);
		map.put("end_ext", end_ext);
		ArrayList<TpmsPesVO> equip1List = tpmsRoadRepairMapper.selectEquip1DataForReport(map);
		TpmsRoadInfoVO infoVO =  tpmsRoadInfoMapper.selectRoadInfoDataById(list.get(0).getTri_id());
		int startMeter = list.get(0).getT10m_order() * 10;
		int endMeter = list.get(list.size()-1).getT10m_order() * 10;
		
		// 시점 구간
		String start = startNode.get("node_name").toString() + "+" + startMeter + "m";
		// 종점 구간
		String end = endNode.get("node_name").toString() + "+" + endMeter + "m";
		int total_ext = list.size() * 10; // meter 기준
		double avg_index = 0.0;
		double avg_cr = 0.0;
		double avg_rutting = 0.0;
		double avg_iri = 0.0;
		
		double t_inx = 0.0; double t_cr = 0.0; double t_rutt=0.0; double t_iri=0.0;
		ArrayList<Long> tsList = new ArrayList<>();
		for (TpmsCrackViewVO tpmsCrackViewVO : list) {
			if(!tsList.contains(tpmsCrackViewVO.getTs_id())) tsList.add(tpmsCrackViewVO.getTs_id());
			switch (formula) {
			case "mpci":
				t_inx += tpmsCrackViewVO.getMpci();
				break;
			case "spi":
				t_inx += tpmsCrackViewVO.getSpi();
				break;
			case "nhpci":
				t_inx += tpmsCrackViewVO.getNhpci();
				break;
			}
			
			t_cr += tpmsCrackViewVO.getCr();
		}
		avg_index = t_inx / (double) list.size();
		avg_cr = t_cr / (double) list.size();
		
		for(TpmsPesVO equip1VO : equip1List) {
			t_rutt += equip1VO.getRutting();
			t_iri += equip1VO.getAvg_iri();
		}
		avg_rutting = t_rutt / (double) equip1List.size();
		avg_iri = t_iri / (double) equip1List.size();
		
		String rank = null;
		switch (infoVO.getRank()) {
		case "101":
			rank = "고속국도";
			break;
		case "102":
			rank = "도시고속국도";		
			break;
		case "103":
			rank = "일반국도";		
			break;
		case "104":
			rank = "특별·광역시도";
			break;
		case "105":
			rank = "국가지원 지방도";
			break;
		case "106":
			rank = "지방도";
			break;
		case "107":
			rank = "시·군도";
			break;
		case "108":
			rank = "기타";
			break;
		}
		
		
		res.put("priority", priority);
		res.put("rank", rank);
		res.put("no", infoVO.getNo());
		res.put("name", infoVO.getName());
		res.put("dir", (list.get(0).getDir()==0)?"상행":"하행");
		res.put("lane", 1);
		res.put("s_pos", start);
		res.put("e_pos", end);
		res.put("total_ext", total_ext);
		res.put("avg_index", Math.round(avg_index*100)/100.0);
		res.put("avg_cr",  Math.round(avg_cr*100)/100.0);
		res.put("avg_rutting", Math.round(avg_rutting*100)/100.0);
		res.put("avg_iri", Math.round(avg_iri*100)/100.0);
		res.put("tsList", tsList);
		
		
		return res;
	}
	
	
	
	
	
}
