package egovframework.com.tpms.repair.service;

public class TpmsEquip2VO {
	
	private String equip2Type;
	private String year;
	
	
	public String getEquip2Type() {
		return equip2Type;
	}
	public void setEquip2Type(String equip2Type) {
		this.equip2Type = equip2Type;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}

}
