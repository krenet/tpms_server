package egovframework.com.tpms.repair.service;

public class TpmsRoadRepairVO {
	private long id;
	private String repair_target_id;
	private String pave_type;
	private String method_type;
	private String method;
	private String pave_meterial;
	private double split_thin;
	private double layer_reinforce;
	private double surface_thin;
	private String comment;
	private long price;
	private String date;
	private int version;
	private String sigongsa;
	private String sigong_pm;
	private String gamrisa;
	private String gamrisa_pm;
	private String gamriwon;
	private String gamriwon_pm;
	private double total_ext;
	private long tsm_id;
	private String e_node_name;
	private long e_t10m_id;
	private long s_ts_id;
	private long s_ts_node_id;
	private int dir;
	private String in_code;
	private long e_ts_id;
	private int year;
	private long e_ts_node_id;
	private String s_node_name;
	private long s_t10m_id;
	private int lane;
	private String pave_material;
	
	private long tri_id;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getRepair_target_id() {
		return repair_target_id;
	}
	public void setRepair_target_id(String repair_target_id) {
		this.repair_target_id = repair_target_id;
	}
	public String getPave_type() {
		return pave_type;
	}
	public void setPave_type(String pave_type) {
		this.pave_type = pave_type;
	}
	public String getMethod_type() {
		return method_type;
	}
	public void setMethod_type(String method_type) {
		this.method_type = method_type;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getPave_meterial() {
		return pave_meterial;
	}
	public void setPave_meterial(String pave_meterial) {
		this.pave_meterial = pave_meterial;
	}
	public double getSplit_thin() {
		return split_thin;
	}
	public void setSplit_thin(double split_thin) {
		this.split_thin = split_thin;
	}
	public double getLayer_reinforce() {
		return layer_reinforce;
	}
	public void setLayer_reinforce(double layer_reinforce) {
		this.layer_reinforce = layer_reinforce;
	}
	public double getSurface_thin() {
		return surface_thin;
	}
	public void setSurface_thin(double surface_thin) {
		this.surface_thin = surface_thin;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getSigongsa() {
		return sigongsa;
	}
	public void setSigongsa(String sigongsa) {
		this.sigongsa = sigongsa;
	}
	public String getSigong_pm() {
		return sigong_pm;
	}
	public void setSigong_pm(String sigong_pm) {
		this.sigong_pm = sigong_pm;
	}
	public String getGamrisa() {
		return gamrisa;
	}
	public void setGamrisa(String gamrisa) {
		this.gamrisa = gamrisa;
	}
	public String getGamrisa_pm() {
		return gamrisa_pm;
	}
	public void setGamrisa_pm(String gamrisa_pm) {
		this.gamrisa_pm = gamrisa_pm;
	}
	public String getGamriwon() {
		return gamriwon;
	}
	public void setGamriwon(String gamriwon) {
		this.gamriwon = gamriwon;
	}
	public String getGamriwon_pm() {
		return gamriwon_pm;
	}
	public void setGamriwon_pm(String gamriwon_pm) {
		this.gamriwon_pm = gamriwon_pm;
	}
	public double getTotal_ext() {
		return total_ext;
	}
	public void setTotal_ext(double total_ext) {
		this.total_ext = total_ext;
	}
	public long getTsm_id() {
		return tsm_id;
	}
	public void setTsm_id(long tsm_id) {
		this.tsm_id = tsm_id;
	}
	public String getE_node_name() {
		return e_node_name;
	}
	public void setE_node_name(String e_node_name) {
		this.e_node_name = e_node_name;
	}
	public long getE_t10m_id() {
		return e_t10m_id;
	}
	public void setE_t10m_id(long e_t10m_id) {
		this.e_t10m_id = e_t10m_id;
	}
	public long getS_ts_id() {
		return s_ts_id;
	}
	public void setS_ts_id(long s_ts_id) {
		this.s_ts_id = s_ts_id;
	}
	public long getS_ts_node_id() {
		return s_ts_node_id;
	}
	public void setS_ts_node_id(long s_ts_node_id) {
		this.s_ts_node_id = s_ts_node_id;
	}
	public int getDir() {
		return dir;
	}
	public void setDir(int dir) {
		this.dir = dir;
	}
	public String getIn_code() {
		return in_code;
	}
	public void setIn_code(String in_code) {
		this.in_code = in_code;
	}
	public long getE_ts_id() {
		return e_ts_id;
	}
	public void setE_ts_id(long e_ts_id) {
		this.e_ts_id = e_ts_id;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public long getE_ts_node_id() {
		return e_ts_node_id;
	}
	public void setE_ts_node_id(long e_ts_node_id) {
		this.e_ts_node_id = e_ts_node_id;
	}
	public String getS_node_name() {
		return s_node_name;
	}
	public void setS_node_name(String s_node_name) {
		this.s_node_name = s_node_name;
	}
	public long getS_t10m_id() {
		return s_t10m_id;
	}
	public void setS_t10m_id(long s_t10m_id) {
		this.s_t10m_id = s_t10m_id;
	}
	public int getLane() {
		return lane;
	}
	public void setLane(int lane) {
		this.lane = lane;
	}
	public String getPave_material() {
		return pave_material;
	}
	public void setPave_material(String pave_material) {
		this.pave_material = pave_material;
	}
	public long getTri_id() {
		return tri_id;
	}
	public void setTri_id(long tri_id) {
		this.tri_id = tri_id;
	}
	
	
	
	
}
