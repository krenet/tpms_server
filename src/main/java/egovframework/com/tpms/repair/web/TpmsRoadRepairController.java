package egovframework.com.tpms.repair.web;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.cmm.service.TpmsCmmService;
import egovframework.com.tpms.repair.service.TpmsRoadRepairService;

@Controller
public class TpmsRoadRepairController {

	@Resource(name="tpmsRoadRepairService")
	private TpmsRoadRepairService tpmsRoadRepairService; 
	
	@Resource(name="tpmsCmmService")
	private TpmsCmmService tpmsCmmService;
	
	@RequestMapping(value="/tpms/getIncompleteRepairTargetId.do", method=RequestMethod.POST)
	public ModelAndView getIncompleteRepairTargetId(){
	
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("res", tpmsRoadRepairService.selectRepairTargetIdList());
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/selectTargetInfoByTargetId.do", method=RequestMethod.POST)
	public ModelAndView selectTargetInfoByTargetId(@RequestParam HashMap<String, Object> param){
	
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("res", tpmsRoadRepairService.selectTargetInfoByTargetId(param));
		
		return mav;
	}
	//insertRepairData
	@RequestMapping(value="/tpms/insertRepairData.do", method=RequestMethod.POST)
	public ModelAndView insertRepairData(@RequestParam HashMap<String, Object> param){
	
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("res", tpmsRoadRepairService.insertRepairData(param));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getRoadRepairPriorityList.do", method=RequestMethod.POST)
	public ModelAndView getRoadRepairPriorityList(){
		String formula = tpmsCmmService.getIndexFormula();
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		ArrayList<HashMap<String, Object>> res = tpmsRoadRepairService.getRoadRepairPriorityList(formula);
		mav.addObject("res", res);
		
		return mav;
	}
	
}
