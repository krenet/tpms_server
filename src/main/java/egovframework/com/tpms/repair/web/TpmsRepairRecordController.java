package egovframework.com.tpms.repair.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.repair.service.TpmsRepairRecordService;
import egovframework.com.tpms.rsm.service.TpmsDetailTargetService;


@Controller
public class TpmsRepairRecordController {

	@Resource(name="tpmsRepairRecordService")
	private TpmsRepairRecordService tpmsRepairRecordService;
	
	@Resource(name="tpmsDetailTargetService")
	private TpmsDetailTargetService tpmsDetailTargetService;
	
	@RequestMapping(value="/tpms/getAllRepairRecordList.do", method=RequestMethod.POST)
	public ModelAndView getAllRepairRecordList(){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		List<Map<String, Object>> list = tpmsRepairRecordService.getAllRepairRecordList();
		if(list.size()>0) {
			Map<String, Long> extParam = new HashMap<String, Long>();
			for(int i=0; i<list.size(); ++i) {
				extParam.put("s_ts_id", ((Number)list.get(i).get("s_ts_id")).longValue());
				extParam.put("s_ts_node_id",((Number)list.get(i).get("s_ts_node_id")).longValue());
				extParam.put("e_ts_id", ((Number)list.get(i).get("e_ts_id")).longValue());
				extParam.put("e_ts_node_id",((Number)list.get(i).get("e_ts_node_id")).longValue());
				String s_ext = tpmsDetailTargetService.getStartExt(extParam);
				String e_ext = tpmsDetailTargetService.getEndExt(extParam);
				if(s_ext==null) s_ext="0";
				if(e_ext==null) e_ext="0";
				list.get(i).put("ts_id", list.get(i).get("s_ts_id"));
				list.get(i).put("s_ext", s_ext); // 시점 노드부터 몇 미터 떨어진 지점인지
				list.get(i).put("e_ext", e_ext); 
				list.get(i).put("spos", list.get(i).get("s_node_name")+"+"+String.format("%.2f", (Double.parseDouble(s_ext)/1000))+"km");
				list.get(i).put("epos", list.get(i).get("e_node_name")+"+"+String.format("%.2f", (Double.parseDouble(e_ext)/1000))+"km");
			}
			mav.addObject("res", list);
		}
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getSearchMaster.do", method=RequestMethod.POST)
	public ModelAndView getSearchMaster(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		mav.addObject("res", tpmsRepairRecordService.getSearchMaster(map));
		
		return mav;
	}
}
