package egovframework.com.tpms.repair.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.tpms.cmm.service.TpmsCmmService;
import egovframework.com.tpms.repair.service.TpmsRepairTargetService;
import egovframework.com.tpms.rsm.service.TpmsDetailTargetService;

@Controller
public class TpmsRepairTargetController {

	@Resource(name="tpmsRepairTargetService")
	private TpmsRepairTargetService tpmsRepairTargetService;
	
	@Resource(name="tpmsDetailTargetService")
	private TpmsDetailTargetService tpmsDetailTargetService;
	
	@Resource(name="tpmsCmmService")
	private TpmsCmmService tpmsCmmService;
	
//	
//	@RequestMapping(value="/tpms/getEquip2.do", method=RequestMethod.POST)
//	public ModelAndView getEquip2(@RequestParam String equip2Type){
//	
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("jsonView");
//		
//		mav.addObject("res", tpmsRepairTargetService.getEquip2(equip2Type));
//		
//		return mav;
//	}
	
	@RequestMapping(value="/tpms/getCurrYearMaint.do", method=RequestMethod.POST)
	public ModelAndView getCurrYearMaint(@RequestParam int year){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		List<Map<String, Object>> list = tpmsRepairTargetService.getCurrYearMaint(year);
		if(list.size()>0) {
			Map<String, Long> extParam = new HashMap<String, Long>();
			for(int i=0; i<list.size(); ++i) {
				extParam.put("s_ts_id", ((Number)list.get(i).get("s_ts_id")).longValue());
				extParam.put("s_ts_node_id",((Number)list.get(i).get("s_ts_node_id")).longValue());
				extParam.put("e_ts_id", ((Number)list.get(i).get("e_ts_id")).longValue());
				extParam.put("e_ts_node_id",((Number)list.get(i).get("e_ts_node_id")).longValue());
				String s_ext = tpmsDetailTargetService.getStartExt(extParam);
				String e_ext = tpmsDetailTargetService.getEndExt(extParam);
				if(s_ext==null) s_ext="0";
				if(e_ext==null) e_ext="0";
				list.get(i).put("ts_id", list.get(i).get("s_ts_id"));
				list.get(i).put("s_ext", s_ext); // 시점 노드부터 몇 미터 떨어진 지점인지
				list.get(i).put("e_ext", e_ext); 
				list.get(i).put("spos", list.get(i).get("s_node_name")+"+"+String.format("%.2f", (Double.parseDouble(s_ext)/1000))+"km");
				list.get(i).put("epos", list.get(i).get("e_node_name")+"+"+String.format("%.2f", (Double.parseDouble(e_ext)/1000))+"km");
			}
			mav.addObject("res", list);
		}
		
//		mav.addObject("res", tpmsRepairTargetService.getCurrYearMaint(year));	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getAllMaint.do", method=RequestMethod.POST)
	public ModelAndView getAllMaint(){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
	
		List<Map<String, Object>> list = tpmsRepairTargetService.getAllMaint();
		if(list.size()>0) {
			Map<String, Long> extParam = new HashMap<String, Long>();
			for(int i=0; i<list.size(); ++i) {
				extParam.put("s_ts_id", ((Number)list.get(i).get("s_ts_id")).longValue());
				extParam.put("s_ts_node_id",((Number)list.get(i).get("s_ts_node_id")).longValue());
				extParam.put("e_ts_id", ((Number)list.get(i).get("e_ts_id")).longValue());
				extParam.put("e_ts_node_id",((Number)list.get(i).get("e_ts_node_id")).longValue());
				String s_ext = tpmsDetailTargetService.getStartExt(extParam);
				String e_ext = tpmsDetailTargetService.getEndExt(extParam);
				if(s_ext==null) s_ext="0";
				if(e_ext==null) e_ext="0";
				list.get(i).put("ts_id", list.get(i).get("s_ts_id"));
				list.get(i).put("s_ext", s_ext); // 시점 노드부터 몇 미터 떨어진 지점인지
				list.get(i).put("e_ext", e_ext); 
				list.get(i).put("spos", list.get(i).get("s_node_name")+"+"+String.format("%.2f", (Double.parseDouble(s_ext)/1000))+"km");
				list.get(i).put("epos", list.get(i).get("e_node_name")+"+"+String.format("%.2f", (Double.parseDouble(e_ext)/1000))+"km");
			}
			mav.addObject("res", list);
		}
		
		return mav;
	}


	@RequestMapping(value="/tpms/getCurrMaintTs_Id.do", method=RequestMethod.POST)
	public ModelAndView getCurrMaintTs_Id() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		int year = Calendar.getInstance().get(Calendar.YEAR);
		List<Map<String, Object>> list = tpmsRepairTargetService.getCurrYearMaint(year);
		List<String> repairIdList = new ArrayList<String>();
		for(int i=0; i<list.size(); ++i) {
			repairIdList.add((String)list.get(i).get("repair_target_id"));
		}
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("repairIdList", repairIdList);
		
		mav.addObject("res", tpmsRepairTargetService.getCurrMaintTs_Id(param));	
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getRepairId.do", method=RequestMethod.POST)
	public ModelAndView getRepairId(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<Map<String, Object>> repairList = new ArrayList<Map<String,Object>>();
		try {
			String valueStr = java.net.URLDecoder.decode((String)map.get("list"), "UTF-8");
			ArrayList<HashMap<String, Object>> list = mapper.readValue(valueStr, new TypeReference<ArrayList<HashMap<String, Object>>>() {});
			System.out.println("첫번째: "+list);
			result.put("group_num", list.get(0).get("tsm_id"));
			result.put("year", year);
			result.put("rank", list.get(0).get("rank"));
			result.put("dir", list.get(0).get("dir"));
			result.put("lane", list.get(0).get("lane"));
			result.put("tsm_id", list.get(0).get("tsm_id"));
			result.put("tri_id", list.get(0).get("tri_id"));
			result.put("s_ts_id", list.get(0).get("ts_id"));
			result.put("s_t10m_id", tpmsDetailTargetService.getS_T10m((Integer)list.get(0).get("ts_id")));			
			HashMap<String, Integer> paramMap = new HashMap<String, Integer>();
			paramMap.put("tsm_id", (Integer)list.get(0).get("tsm_id"));
			paramMap.put("order", (Integer)list.get(0).get("order"));
			Map<String, Long> extParam = new HashMap<String, Long>();
			extParam.put("s_ts_id", ((Number)list.get(0).get("ts_id")).longValue());
			// 시점
			Map<String, Object> s_node = new HashMap<String, Object>();
			Long s_ts_node_id=null;
			if(paramMap.get("order")!=1) {
				s_node = tpmsDetailTargetService.getNode_OrderNEQ1(paramMap);
				s_ts_node_id = (Long)s_node.get("id");
				if(s_node.size()==0) {
					s_node = tpmsDetailTargetService.getNode_OrderEQ1(paramMap);
					s_ts_node_id = (Long)s_node.get("ts_id");
				}
			}else {
				s_node = tpmsDetailTargetService.getNode_OrderEQ1(paramMap);	
				s_ts_node_id = (Long)s_node.get("ts_id");
			}
			extParam.put("s_ts_node_id", s_ts_node_id);
			String s_ext = tpmsDetailTargetService.getStartExt(extParam);
			if(s_ext==null) s_ext="0";
			result.put("s_ts_node_id", s_ts_node_id);
			result.put("s_ext", s_ext);  // 시점 노드부터 몇 미터 떨어진 지점인지
			result.put("s_node_name", s_node.get("node_name"));  // 시점 노드 이름
			result.put("s_node_type", s_node.get("node_type"));  // 시점 노드 타입
			result.put("spos", s_node.get("node_name")+"+"+String.format("%.2f", (Double.parseDouble(s_ext)/1000))+"km");
			
			
			// 종점
			Map<String, Object> e_node = new HashMap<String, Object>();
			paramMap.replace("tsm_id", (Integer)list.get(list.size()-1).get("tsm_id"));
			paramMap.replace("order", (Integer)list.get(list.size()-1).get("order"));
			result.put("e_ts_id", list.get(list.size()-1).get("ts_id"));
			extParam.put("e_ts_id", ((Number)list.get(list.size()-1).get("ts_id")).longValue());
			result.put("e_t10m_id", tpmsDetailTargetService.getE_T10m((Integer)list.get(list.size()-1).get("ts_id")));
			
			Long e_ts_node_id=null;
			if(paramMap.get("order")!=1) {
				e_node = tpmsDetailTargetService.getNode_OrderNEQ1(paramMap);
				e_ts_node_id = (Long)e_node.get("id");
				if(e_node.size()==0) {
					e_node = tpmsDetailTargetService.getNode_OrderEQ1(paramMap);
					e_ts_node_id = (Long)e_node.get("ts_id");
				}
			}else {
				e_node = tpmsDetailTargetService.getNode_OrderEQ1(paramMap);
				e_ts_node_id = (Long)e_node.get("ts_id");
			}
			extParam.put("e_ts_node_id", e_ts_node_id);
			String e_ext = tpmsDetailTargetService.getEndExt(extParam);
			if(e_ext==null) e_ext="0";
			result.put("e_ts_node_id", e_ts_node_id);
			result.put("e_ext", e_ext);  // 종점 노드부터 몇 미터 떨어진 지점인지
			result.put("e_node_name", e_node.get("node_name"));  // 종점 노드 이름
			result.put("e_node_type", e_node.get("node_type"));  // 종점 노드 타입
			result.put("epos", e_node.get("node_name")+"+"+String.format("%.2f", (Double.parseDouble(e_ext)/1000))+"km");
			
			// 총연장 길이
			double total_ext = 0;
			for(int k=0; k<list.size(); ++k) {
				total_ext +=(double)list.get(k).get("sm_ext");
			}
			result.put("total_ext", total_ext);
			
			repairList.add(result);
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(repairList.size()>0) {
			repairList = tpmsCmmService.getTargetId("R", repairList);
			mav.addObject("res", repairList);
		}

		return mav;
	}
	
	
	@RequestMapping(value="/tpms/saveNewRepair.do", method=RequestMethod.POST)
	public ModelAndView saveNewRepair(@RequestParam HashMap<String, Object> map){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		
		int result = tpmsRepairTargetService.saveNewRepair(map);
//		System.out.println("result: "+ result);
//		System.out.println("보수대상구간 저장 map: "+map);
		mav.addObject("res", result);
		return mav;
	}
	
	
	@RequestMapping(value="/tpms/getSelectEquip1Data.do", method=RequestMethod.POST)
	public ModelAndView getSelectEquip1Data(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
		mav.addObject("res", tpmsRepairTargetService.getSelectEquip1Data(map));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/getSelectEquip2Data.do", method=RequestMethod.POST)
	public ModelAndView getSelectEquip2Data(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");

		Map<String, Object> param = new HashMap<>();
		param.put("section", "setting");
		
		mav.addObject("res_crack", tpmsRepairTargetService.getSelectCrackData(map, tpmsCmmService.readIniFile(param)));
		mav.addObject("res_gpr", tpmsRepairTargetService.getSelectGprData(map));
		mav.addObject("res_hwd", tpmsRepairTargetService.getSelectHwdData(map));
		
		return mav;
	}
	
	@RequestMapping(value="/tpms/deleteRepairData.do", method=RequestMethod.POST)
	public ModelAndView deleteRepairData(@RequestParam HashMap<String, Object> map){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsonView");
	
		int result = tpmsRepairTargetService.deleteRepairData(map);
		if(result==1) {
			mav.addObject("res", "success");
		}else {
			mav.addObject("res", "fail");
		}
		
		return mav;
	}
	
	
}
